#pragma once
#include <Scene.h>
#include <ObjExaminer.h>
#include <Material.h>

class WaterScene :
	public Scene
{
public:
	WaterScene();
	~WaterScene();


void run();
void loadTextures();
void load(Game* newGame);

private:

	GameObject * player;

	GLuint asphalt = 0;
	GLuint asphaltSpec = 0;
	GLuint asphaltNormal = 0;
	GLuint metal = 0;

	GLuint brickWall = 0;
	GLuint brickWallNormal = 0;

	GLuint shipSpec = 0;

	GLuint waterDudv = 0;

	GLuint waterNormal = 0;

	Material* cubeMaterial;

	void loadInit();

	void loadCubeCircle();

	void loadCubeGrid(int);

	void loadPlayer();

	void loadCamera();

	void loadWaterPlane();

	void loadLighting();

	void loadIndexedCube();

	void loadTree001();

	void loadWorld();

	void setupPostProcessing();
};