#pragma once
#include "Scene.h"
class PhysicsTestScene :
	public Scene
{
public:
	PhysicsTestScene();
	~PhysicsTestScene();

	void run();

	void load(Game *);


private:
	GameObject * player = nullptr;

	void loadBase();

	void loadInit();

	void loadCamera();

	void loadPlayer();

	void loadCubes();

	void loadGUI();
};

