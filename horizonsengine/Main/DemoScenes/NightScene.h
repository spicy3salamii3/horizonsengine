#pragma once
#include "Scene.h"
class NightScene :
	public Scene
{
public:
	NightScene();
	~NightScene();

	void run();
	void loadTextures();
	void load(Game* newGame);

private:

	void loadLara();

	GameObject * player;

	GLuint asphalt = 0;
	GLuint asphaltSpec = 0;
	GLuint asphaltNormal = 0;
	GLuint metal = 0;

	GLuint brickWall = 0;
	GLuint brickWallNormal = 0;

	GLuint shipSpec = 0;

	void loadInit();

	void loadStreet();

	void loadCubeCircle();

	void loadPlayer();

	void loadWorld();

	void loadCamera();

	void loadLighting();

	void loadSponza();
};

