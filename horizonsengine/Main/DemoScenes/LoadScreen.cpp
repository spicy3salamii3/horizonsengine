#include "LoadScreen.h"
#include <ImageRect.h>
#include <MenuController.h>
#include <Text.h>
#include <texture_loader.h>
#include <smallSpotLight.h>
#include <Scenery.h>
#include <SimpleCamera.h>
#include <Waterplane.h>
#include <Skybox.h>
#include <FPSCounter.h>
#include <MoonLight.h>
#include <BrightnessPP.h>
#include <Rotator.h>
#include <Button.h>
#include <ContrastPP.h>
#include <GLRenderer.h>
#include <HorizonsUI.h>


LoadScreen::LoadScreen()
{
	name = "Menu Choice";

}


LoadScreen::~LoadScreen()
{
	delete menuController;
}

void LoadScreen::run()
{
	Fade * f = new Fade(FADE_FROM_BLACK);
	addPublicBehaviour(f);
	f->startFade(1000);
	gameRefr->setCaptureMouse(false);
}

void LoadScreen::load(Game * thisGame)
{
	gameRefr = thisGame;

	gameRefr->setRenderEngine(GLRenderer::singleton());
	
	loadInit();

	examiner = ObjExaminer::getInstance();

	examiner->setDirectoryObject("Objects/");

	loadGUI();

	loadSphere();

	loadCamera();

	debugSettings->lightSphere = false;

	smallSpotLight * spot = new smallSpotLight(glm::vec3(0,0, 3.5), glm::vec3(0, 2, 10));
	smallSpotLight * spot2 = new smallSpotLight(glm::vec3(0,0, 3.5), glm::vec3(0, 2, -10));
	smallSpotLight * spot3 = new smallSpotLight(glm::vec3(0,0, 3.5), glm::vec3(10, 2, -10));

	spot->coneDirection = glm::vec3(0, -0.2, -1.0f);
	spot2->coneDirection = glm::vec3(0, -0.2, 1.0f);
	spot3->coneDirection = glm::vec3(0.3, -0.2, 1.0f);

	currLighting->addLight(spot);
	currLighting->addLight(spot2);
	currLighting->addLight(spot3);


	Moonlight * moonLight = new Moonlight(glm::vec3(0, 0.8f, 0.8f));
	currLighting->addLight(moonLight);

	Skybox * sky = new Skybox(1);

	sky->name = "Skybox";

	sky->setPos(glm::vec3(0,-200, 0));

	addGameObject(sky);

	this;


	gameRefr->setConfigAll();
}


void LoadScreen::loadGUI()
{

	Text* message = new Text(Game::singleton()->getConfig().codeFontCharacters);

	message->centred = true;

	message->setText("Welcome to Horizons Engine Demo");

	message->setPos(glm::vec3(0, 250, 0));

	message->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	addGUIObject(message);

	message->setScale(glm::vec3(0.5f));

	menuController = new MenuController(gameRefr);

	addPublicBehaviour(menuController);

	TextureExaminer* texExam = TextureExaminer::singleton();

	texExam->setDirectory("");

	Texture *t = texExam->getTexture("Textures\\Button.png");



	SceneOnButtonAction* waterAction = new SceneOnButtonAction("Water");
	SceneOnButtonAction* terrainAction = new SceneOnButtonAction("Terrain");
	SceneOnButtonAction* nightAction = new SceneOnButtonAction("Night");
	SceneOnButtonAction* physicsAction = new SceneOnButtonAction("Physics");

	Button* WaterScene = new Button(t, glm::vec3(0.2f), waterAction, "WaterButton");
	Text* waterText = new Text(gameRefr->getConfig().codeFontCharacters);
	

	Button* TerrainScene = new Button(t, glm::vec3(0.2f), terrainAction, "TerrainButton");
	Text* terrainText = new Text(gameRefr->getConfig().codeFontCharacters);

	Button* NightScene = new Button(t, glm::vec3(0.2f), nightAction, "NightButton");
	Text* nightText = new Text(gameRefr->getConfig().codeFontCharacters);

	Button* PhysicsScene = new Button(t, glm::vec3(0.2f), physicsAction, "Physics Button");
	Text* physicsText = new Text(gameRefr->getConfig().codeFontCharacters);

	addGUIObject(WaterScene);
	addGUIObject(waterText);

	addGUIObject(TerrainScene);
	addGUIObject(terrainText);

	addGUIObject(NightScene);
	addGUIObject(nightText);

	addGUIObject(PhysicsScene);
	addGUIObject(physicsText);
	

	addInputListener(WaterScene);
	addInputListener(TerrainScene);
	addInputListener(NightScene);
	addInputListener(PhysicsScene);

	WaterScene->setPos(glm::vec3(0, 80, 0));
	waterText->setPos(glm::vec3(0, 0, 0));

	TerrainScene->setPos(glm::vec3(0, 0, 0));
	terrainText->setPos(glm::vec3(0));

	NightScene->setPos(glm::vec3(0, -80, 0));
	nightText->setPos(glm::vec3(0));

	PhysicsScene->setPos(glm::vec3(0, -160, 0));
	physicsText->setPos(glm::vec3(0));

	WaterScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	waterText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	TerrainScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	terrainText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	NightScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	nightText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	PhysicsScene->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	physicsText->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	waterText->setParent(WaterScene);
	waterText->centred = true;
	waterText->setColour(glm::vec3(0));
	waterText->setText("Water Scene");

	nightText->setParent(NightScene);
	nightText->centred = true;
	nightText->setColour(glm::vec3(0));
	nightText->setText("NightScene");

	terrainText->setParent(TerrainScene);
	terrainText->centred = true;
	terrainText->setColour(glm::vec3(0));
	terrainText->setText("TerrainScene");

	physicsText->setParent(PhysicsScene);
	physicsText->centred = true;
	physicsText->setColour(glm::vec3(0));
	physicsText->setText("Physics Demo Scene");
}

void LoadScreen::loadSphere()
{
	examiner->setFile("HiResSphere.obj");

	Model * sphere = new Model();

	examiner->indexShapes(sphere);

	Custom* sphereGO = new Custom(sphere, glm::vec3(0, 2, 0), glm::vec3(0.05f), glm::vec3(0, -1.57f ,0));

	sphereGO->name = "That Sphere";

	sphereGO->setup();

	sphereGO->getComponent<RenderConditions*>()->setMaterial(examiner->getMaterial());

	addGameObject(sphereGO);

	sphereGO->addPrivateBehaviour(new yRotator(sphereGO, 0.0002f));

	GLuint waterDudv = fiLoadTextureNormal("Textures/waterDUDV.png");
	GLuint waterNormal = fiLoadTextureNormal("Textures/waterNormalMap.png");

	WaterPlane * water = new WaterPlane(glm::vec2(40,80));

	water->name = "Water";

	water->setPos(glm::vec3(0, 0, 0));

	addReflectiveGameObject(water);

	water->setDudv(waterDudv);

	water->setNormal(waterNormal);

}

void LoadScreen::loadCamera()
{
	RotatingCamera * camera = new RotatingCamera();

	gameRefr->setCamera(camera);

	//BrightnessPP * bright = new BrightnessPP(1.5f);
	//gameRefr->addPostProccessor(bright);
}

