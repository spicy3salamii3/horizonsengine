#include "NightScene.h"
#include <Game.h>
#include <VAOData.h>
#include <Scenery.h>
#include <MoonLight.h>
#include <FirstPersonCamera.h>
#include <Skybox.h>
#include <smallSpotLight.h>
#include <PlayerController.h>
#include <Rotator.h>
#include <PointLight.h>
#include <Volume.h>
#include <WaterPlane.h>
#include <ImageRect.h>
#include <BrightnessPP.h>

NightScene::NightScene()
{
	name = "Night";
}


NightScene::~NightScene()
{
	delete currLighting;
}

void NightScene::run()
{
	gameRefr->captureMouse = true;
	gameRefr->setPlayer(player);
	gameRefr->setPlaying(true);
}

void NightScene::loadTextures()
{
	asphalt = fiLoadTexture("Textures/Asphalt.png");
	asphaltSpec = fiLoadTexture("Textures/AsphaltSpec.png");
	asphaltNormal = fiLoadTextureNormal("Textures/AsphaltNormal.png");
	metal = fiLoadTexture("Textures/metal.jpg");
	brickWall = fiLoadTexture("Textures/brickwall.jpg");
	brickWallNormal = fiLoadTextureNormal("Textures/brickwall_normal.jpg");
	shipSpec = fiLoadTexture("Textures/shipSpec.png");
}

void NightScene::loadInit()
{
	Scene::loadInit();


	loadTextures();
	GameObject * skyBox = new Skybox(1);
	addGameObject(skyBox);

	examiner = ObjExaminer::getInstance();
}

void NightScene::loadStreet()
{
	examiner->setFile("HouseRow.obj");

	
	Model* roadData = new Model();

	examiner->indexShapes(roadData);

	Custom* road = new Custom(roadData, glm::vec3(0), glm::vec3(1), glm::vec3(0));

	addGameObject(road);
}

void NightScene::loadCubeCircle()
{
	//Set the file
	examiner->setFile("Cube.obj");

	//Create a VAO data from this
	Model *cube = new Model();

	examiner->indexShapes(cube);

	//Get the VAO
	GLuint cubeVAO = examiner->getVAO();

	//get the cube size 
	int cubeSize = examiner->getTotalVerts();

	static const int cubeCount = 34;

	float interval = (2.0f * M_PI) / cubeCount;

	float radius = 150.0f;
	float x, z;
	float height = 5.0f;
	int count = 0;

	for (float angle = 0; angle < 2.0f* M_PI; angle += interval)
	{
		x = radius * cos(angle);
		z = radius * sin(angle);

		Custom  * cubeInCircle = new Custom(cube, glm::vec3(x, height, z), glm::vec3(0.5f), glm::vec3(0));
		addGameObject(cubeInCircle);

		yRotator * rotator = new yRotator(cubeInCircle, 0.0003f);
		cubeInCircle->addPrivateBehaviour(rotator);
	}
}

void NightScene::loadPlayer()
{
	//Add a Cube to the scene
	//Set the file
	examiner->setFile("Cube.obj");
	
	Model *cube = new Model();

	examiner->indexShapes(cube);

	//Get the VAO
	GLuint cubeVAO = examiner->getVAO();

	//get the cube size 
	int cubeSize = examiner->getTotalVerts();

	Custom *cube1 = new Custom(cube, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.05f), glm::vec3(M_PI, 0.0f, 0.0f));

	addGameObject(cube1);

	player = cube1;

	//Make a player controller
	PlayerController * controller = new PlayerController(gameRefr);

	controller->setTarget(player);

	controller->setSpeed(0.1f);

	player->addPrivateBehaviour(controller);

}

void NightScene::loadCamera()
{
	//Setup the game camera
	Camera * newCamera = new FirstPersonCamera(player);

	gameRefr->setCamera(newCamera);
}

void NightScene::loadLighting()
{
	//////
	//LIGHTING
	//////

	//Add a basic moonlight to the scene
	currLighting->addLight(new Moonlight(glm::vec3(0.1f, 1.0f, 0.4f)));

	currLighting->lights[0]->castShadows = true;

	currLighting->addLight(new smallSpotLight(glm::vec3(4, 0, 2.3f), glm::vec3(-15, 12, 5)));
	currLighting->addLight(new smallSpotLight(glm::vec3(0.3, 0, 4.3f), glm::vec3(0, 12, 0)));
	currLighting->addLight(new smallSpotLight(glm::vec3(0, 4, 2.3f), glm::vec3(15, 12, -5)));
}

void NightScene::loadSponza()
{
	examiner->setDirectoryTexture("Textures\\Sponza\\");

	examiner->setFile("sponza.obj");

	SceneConfig sc;

	sc.scale = 0.05f;

	examiner->indexScene(this, sc);
}

void NightScene::loadLara()
{
	examiner->setDirectoryTexture("");
	examiner->setDirectoryObject("Objects\\");
	examiner->setFile("Dangerous Croft.obj");

	Model * laraData = new Model();

	examiner->indexShapes(laraData);

	Custom * lara = new Custom(laraData, glm::vec3(0, 60, 0), glm::vec3(1), glm::vec3(M_PI, 0, 0));

	addGameObject(lara);
}

void NightScene::load(Game* thisGame)
{
	gameRefr = thisGame;

	loadInit();

	examiner->setDirectoryObject("Objects\\");

	loadPlayer();

	//loadStreet();

	//loadWorld();

	loadSponza();

	//loadLara();

	loadLighting();

	loadCamera();

	//gameRefr->addPostProccessor(new BrightnessPP(1.9f));

	gameRefr->setConfigAll();
}

void NightScene::loadWorld()
{
	examiner->setFile("world2.obj");

	Model * worldData = new Model();

	examiner->indexShapes(worldData);

	Custom * world = new Custom(worldData, glm::vec3(0), glm::vec3(1), glm::vec3(0));

	addGameObject(world);



}