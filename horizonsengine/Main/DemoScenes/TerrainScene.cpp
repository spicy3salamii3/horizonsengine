#include "TerrainScene.h"
#include <Game.h>
#include <VAOData.h>
#include <Scenery.h>
#include <MoonLight.h>
#include <FirstPersonCamera.h>
#include <ThirdPersonCamera.h>
#include <Skybox.h>
#include <smallSpotLight.h>
#include <PlayerController.h>
#include <Rotator.h>
#include <PointLight.h>
#include <Volume.h>
#include <Triangle.h>
#include <WaterPlane.h>
#include <ImageRect.h>
#include <ObjectFactory.h>
#include <FPSCounter.h>
#include <PrintPositions.h>
#include <Terrain.h>
#include <InstancedObject3D.h>
#include <GenerateCube.h>
#include <GrassyTerrainTextureData.h>
#include <PhysicsComponent.h>
#include <Path.h>
#include <CheckPlayerHeight.h>

TerrainScene::TerrainScene()
{
	name = "Terrain";
}

TerrainScene::~TerrainScene()
{
	delete currLighting;

}

void TerrainScene::run()
{
	gameRefr->captureMouse = true;
	gameRefr->setPlayer(player);
	gameRefr->setPlaying(true);
}

void TerrainScene::loadTextures()
{
	shipSpec = fiLoadTexture("Textures/shipSpec.png");

	waterDudv = fiLoadTextureNormal("Textures/waterDUDV.png");
	waterNormal = fiLoadTextureNormal("Textures/waterNormalMap.png");
}

void TerrainScene::loadInit()
{
	Scene::loadInit();

	loadTextures();

	examiner = ObjExaminer::getInstance();
}

void TerrainScene::loadCubeGrid(int max)
{
	//Set the file
	examiner->setFile("cube.obj");

	float height = 80.0f;

	//Create a VAO data from this
	Model *cube = new Model();

	examiner->indexShapes(cube);

	int distance = 25;

	for (int count = -max; count < max; count++)
	{
		for (int count2 = -max; count2 < max; count2++)
		{
			Custom  * cubeInGrid = new Custom(cube, glm::vec3(count *distance, height, count2 * distance), glm::vec3(0.5f), glm::vec3(0));
			addGameObject(cubeInGrid);

			yRotator * rotator = new yRotator(cubeInGrid, 0.0003f);
			cubeInGrid->addPrivateBehaviour(rotator);
		}
	}
}

void TerrainScene::loadCubeCircle()
{
	for (float height = 140; height < 260; height += 20)
	{
		//Set the file
		examiner->setFile("cube.obj");


		//Create a VAO data from this
		Model *cube = new Model();

		examiner->indexShapes(cube);

		static const int cubeCount = 20;

		float interval = (2.0f * M_PI) / cubeCount;

		float radius = 50.0f;
		float x, z;
		int count = 0;
		glm::vec2 centre = glm::vec2(-310, 77);

		for (float angle = 0; angle < 2.0f* M_PI; angle += interval)
		{
			x = radius * cos(angle) + centre.x;
			z = radius * sin(angle) + centre.y;

			Custom  * cubeInCircle = new Custom(cube, glm::vec3(x, height, z), glm::vec3(0.1f), glm::vec3(0));
			addGameObject(cubeInCircle);
			cubeInCircle->e = cube->getExtents(0);
			//cubeInCircle->setVolume(new PointVolume(cubeInCircle, cubeInCircle->e));

			cubeInCircle->addComponent(new PhysicsComponent(cubeInCircle, 50.0f));
		}
	}
}

void TerrainScene::loadPlayer()
{
	//Add a Cube to the scene
	//Set the file
	examiner->setFile("Cube.obj");

	Model *cube = new Model();

	examiner->indexShapes(cube);

	Custom *cube1 = new Custom(cube, glm::vec3(-310.0f, 100.0f, 77.0f), glm::vec3(0.1f), glm::vec3(0.0f, 0.0f, 0.0f));

	addGameObject(cube1);

	player = cube1;

	//Make a player controller
	PlayerController * controller = new PlayerController(gameRefr);

	controller->setSpeed(0.03f);

	player->addPrivateBehaviour(controller);

	GenerateCube * cubeOnClick = new GenerateCube(cube, player, this);

	player->addPrivateBehaviour(cubeOnClick);

	addInputListener(cubeOnClick);
}

void TerrainScene::loadCamera()
{
	//Setup the game camera
	Camera * newCamera = new FirstPersonCamera(player);
	gameRefr->setCamera(newCamera);
}

void TerrainScene::loadWaterPlane()
{
	WaterPlane * water = new WaterPlane(glm::vec2(20));

	water->setPos(glm::vec3(0, 6, 0));

	water->setScale(glm::vec3(300));

	addReflectiveGameObject(water);

	water->setDudv(waterDudv);

	water->setNormal(waterNormal);
}

void TerrainScene::loadLighting()
{
	//////
	//LIGHTING
	//////

	//Add a basic moonlight to the scene
	Daylight * sun = new Daylight(glm::vec3(-0.5f, 1.0f, -0.7f));
	sun->castShadows = true;
	currLighting->addLight(sun);

}

void TerrainScene::load(Game* thisGame)
{
	gameRefr = thisGame;

	loadInit();

	examiner->setDirectoryObject("Objects\\");

	loadCubeCircle();

	loadWorld();

	loadPlayer();

	loadLighting();

	loadCamera();

	GameObject * skyBox = new Skybox(0);

	addGameObject(skyBox);

	gameRefr->setConfigAll();

	Path* path = new Path();

	path->generateFromSceneWithKeyword(this, "RacingLine");
}

void TerrainScene::loadIndexedCube()
{
	examiner->setFile("Objects/cubeSimple.obj");

	Model * cubeData = new Model();

	examiner->indexShapes(cubeData);

	Custom* cubeIndexed = new Custom(cubeData, glm::vec3(0, 70, 0), glm::vec3(1), glm::vec3(0));

	addGameObject(cubeIndexed);
}

void TerrainScene::loadWorld()
{
	examiner->setFile("World.obj");
	SceneConfig  sc;

	sc.addInstancible("BarrierSection_A");

	sc.tilingFactor = 95;

	sc.terrainDivisor = 3;

	sc.texData = new GrassyTerrainTextureData();

	examiner->indexScene(this, sc);

	GLuint waterDudv = fiLoadTexture("Textures/waterDUDV.png");

	GLuint waterNormal = fiLoadTexture("Textures/waterNormalMap.png");

	WaterPlane* river0 = new WaterPlane(glm::vec2(6, 11));

	river0->setPos(glm::vec3(-150.0f, 0.0f, 81.0f));

	addReflectiveGameObject(river0);

	river0->setDudv(waterDudv);

	river0->setNormal(waterNormal);

}
