#pragma once
#include <Scene.h>
#include <ObjExaminer.h>
#include <MenuController.h>
#include <Fade.h>
#include <ImageRect.h>

class Game;

class LoadScreen :
	public Scene
{
public:
	LoadScreen();
	~LoadScreen();

	void run();
	void load(Game* gameRefr);
	void loadTextures() {}
protected:
	void loadSphere();

	void loadGUI();

	void loadCamera();
	
	MenuController * menuController;

	Fade * fader;
};

