#version 330 core
layout (triangles) in;
layout (line_strip, max_vertices = 12) out;


uniform sampler2D normalMap;

in VS_OUT {

    vec3 tangent;
	vec3 normal;
	vec3 bitangent;
	vec2 texCoords;
} gs_in[];

const float MAGNITUDE = 0.8;

vec2 averageTexCoord;
vec4 averagePos;

void GenerateLineAtVertex(int index)
{

	//gl_Position = gl_in[index].gl_Position;

 //   EmitVertex();

 //   gl_Position =
	//	 gl_in[index].gl_Position 
	//	+  vec4(readIn, 0.0) * MAGNITUDE;


 //   EmitVertex();
	//


	//gl_Position = gl_in[index].gl_Position;

	//EmitVertex();

	//gl_Position =
	//	gl_in[index].gl_Position
	//	+ vec4(gs_in[index].tangent, 0.0) *( MAGNITUDE+1.0);


	//EmitVertex();

    EndPrimitive();
}


void main()
{
	//Whats the average position of the 3 vertexes?
	averagePos = (gl_in[0].gl_Position + gl_in[1].gl_Position + gl_in[2].gl_Position)/3;

	//What's the texCoord of that point?
	averageTexCoord = (gs_in[0].texCoords + gs_in[1].texCoords + gs_in[2].texCoords)/3;

	mat3 TBN = mat3(gs_in[0].tangent, gs_in[0].bitangent, gs_in[0].normal);

	vec3 readIn;
	vec3 inputN= (texture2D(normalMap, averageTexCoord)).xyz;

	readIn.x =( 2.0* inputN.r)-1.0;
	readIn.y =( 2.0* inputN.g)-1.0;
	readIn.z =( 2.0* inputN.b)-1.0;

	readIn = TBN * readIn;


	//Draw the actual line
	gl_Position = averagePos;

	EmitVertex();

	gl_Position =
		averagePos
		+ vec4(readIn, 0.0) *( MAGNITUDE+1.0);


	EmitVertex();

	EndPrimitive();

}  



