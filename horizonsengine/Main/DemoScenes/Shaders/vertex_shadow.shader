#version 330

layout(location = 0) in vec3 position;

uniform mat4 proj;
uniform mat4 light;
uniform mat4 T;
uniform mat4 R;
uniform mat4 S;

void main(void) {

	gl_Position = proj * light * T * R * S * vec4(position, 1.0);

}