#version 330

// Input vertex packet
layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texCoords;


uniform mat4 scale;
uniform mat4 proj;
uniform mat4 trans;

// Output vertex packet
out packet{
	vec2 textureCoord;
} outputVertex;


void main(void) {

	outputVertex.textureCoord = texCoords;


	// Setup local variable pos in case we want to modify it (since position is constant)
	vec4 pos = proj*trans * scale*  vec4(position.x , position.y, 0.0, 1.0);

	// Apply transformation to pos and store result in gl_Position
	gl_Position = pos;
}