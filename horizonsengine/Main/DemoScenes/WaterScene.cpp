#include "WaterScene.h"
#include <Game.h>
#include <VAOData.h>
#include <Scenery.h>
#include <MoonLight.h>
#include <FirstPersonCamera.h>
#include <ThirdPersonCamera.h>
#include <Skybox.h>
#include <smallSpotLight.h>
#include <PlayerController.h>
#include <Rotator.h>
#include <PointLight.h>
#include <Volume.h>
#include <WaterPlane.h>
#include <ImageRect.h>
#include <FPSCounter.h>
#include <GrassyTerrainTextureData.h>
#include <Terrain.h>
#include <ContrastPP.h>
#include <PrintPositions.h>

WaterScene::WaterScene()
{
	name = "Water";
}

WaterScene::~WaterScene()
{
	delete currLighting;
}

void WaterScene::run()
{
	gameRefr->captureMouse = true;
	gameRefr->setPlayer(player);
	gameRefr->setPlaying(true);
}

void WaterScene::loadTextures()
{
	shipSpec = fiLoadTexture("Textures/shipSpec.png");

	waterDudv = fiLoadTextureNormal("Textures/waterDUDV.png");
	waterNormal = fiLoadTextureNormal("Textures/waterNormalMap.png");
}

void WaterScene::loadInit()
{
	Scene::loadInit();

	loadTextures();

	GameObject * skyBox = new Skybox(0);

	addGameObject(skyBox);

	examiner = ObjExaminer::getInstance();


}

void WaterScene::loadCubeGrid(int max)
{
	//Set the file
	examiner->setFile("cube.obj");

	float height = 20.0f;

	//Create a VAO data from this
	Model *cube = new Model();

	examiner->indexShapes(cube);

	cubeMaterial = examiner->getMaterial();


	examiner->setFile("HiResSphere.obj");

	Model* sphere = new Model();

	examiner->indexShapes(sphere);

	Material* sphereMat = examiner->getMaterial();
	
	int distance = 25;

	for (int count = -max; count < max; count++)
	{
		for (int count2 = -max; count2 < max; count2++)
		{
			if (count2 % 2 == 0)
			{
				Custom* cubeInGrid = new Custom(cube, glm::vec3(count * distance, height, count2 * distance), glm::vec3(0.5f), glm::vec3(0));
				cubeInGrid->setup();

				if (count % 3 == 0)
				{
					cubeInGrid->getComponent<RenderConditions*>()->setMaterial(sphereMat);
				}
				else
				{
					cubeInGrid->getComponent<RenderConditions*>()->setMaterial(cubeMaterial);
				}

				addGameObject(cubeInGrid);

				yRotator* rotator = new yRotator(cubeInGrid, 0.0003f);
				cubeInGrid->addPrivateBehaviour(rotator);
			}
			else
			{
				Custom* sphereInGrid = new Custom(sphere, glm::vec3(count * distance, height, count2 * distance), glm::vec3(0.2f), glm::vec3(0));

				if (count % 3 == 0)
				{
					sphereInGrid->getComponent<RenderConditions*>()->setMaterial(cubeMaterial);
				}
				else
				{
					sphereInGrid->getComponent<RenderConditions*>()->setMaterial(sphereMat);
				}

				addGameObject(sphereInGrid);

				yRotator* rotator = new yRotator(sphereInGrid, 0.0003f);
				sphereInGrid->addPrivateBehaviour(rotator);
			}

		}
	}
}

void WaterScene::loadCubeCircle()
{	
	//Set the file
	examiner->setFile("cube.obj");


	//Create a VAO data from this
	Model *cube = new Model();

	examiner->indexShapes(cube);

	cubeMaterial = examiner->getMaterial();

	static const int cubeCount = 34;

	float interval = (2.0f * M_PI) / cubeCount;

	float radius = 150.0f;
	float x, z;
	float height = 5.0f;
	int count = 0;

	for (float angle = 0; angle < 2.0f* M_PI; angle += interval)
	{
		x = radius * cos(angle);
		z = radius * sin(angle);

		Custom  * cubeInCircle = new Custom(cube, glm::vec3(x, height, z), glm::vec3(0.5f), glm::vec3(0));
		cubeInCircle->setup();

		cubeInCircle->getComponent<RenderConditions*>()->setMaterial(cubeMaterial);
		addGameObject(cubeInCircle);

		yRotator * rotator = new yRotator(cubeInCircle, 0.0003f);
		cubeInCircle->addPrivateBehaviour(rotator);



	}
}

void WaterScene::loadPlayer()
{
	//Add a Cube to the scene
	//Set the file
	examiner->setFile("cube.obj");

	Model *cube = new Model();

	examiner->indexShapes(cube);

	Custom *cube1 = new Custom(cube, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.1f), glm::vec3(0.0f, 0.0f, 0.0f));

	addGameObject(cube1);

	
	player = cube1;


	//Make a player controller
	PlayerController * controller = new PlayerController(gameRefr);

	player->addPrivateBehaviour(controller);
}

void WaterScene::loadCamera()
{
	//Setup the game camera
	Camera * newCamera = new FirstPersonCamera(player);
	newCamera->setTarget(player);
	gameRefr->setCamera(newCamera);
}

void WaterScene::loadWaterPlane()
{
	WaterPlane * water = new WaterPlane(glm::vec2(12));

	water->setPos(glm::vec3(0, -6, 0));
	
	addReflectiveGameObject(water);

	water->setDudv(waterDudv);

	water->setNormal(waterNormal);
}

void WaterScene::loadTree001()
{
	Model * tree001data = new Model();

	examiner->setFile("tree01b.obj");

	examiner->indexShapes(tree001data);

	Custom * tree001 = new Custom(tree001data, glm::vec3(0, 30, 20), glm::vec3(1), glm::vec3(0));
	addGameObject(tree001);
}

void WaterScene::loadLighting()
{
	//////
	//LIGHTING
	//////
	
	//Add a basic moonlight to the scene
	currLighting->addLight(new Daylight(glm::vec3(-0.5f, 1.0f, -0.7f)));

	debugSettings->lightSphere = true;

	static const int lightCount = 8;
	float height = 30.0f;
	float radius = 150.0f;
	float x, z;

	float interval = (2.0f * M_PI) / lightCount;

	for (float angle = 0; angle < 2.0f* M_PI; angle += interval)
	{
		x = radius * cos(angle);
		z = radius * sin(angle);

		smallSpotLight * spotLight = new smallSpotLight(glm::vec3(0, 0, 4), glm::vec3(x, height + 10, z));
		currLighting->addLight(spotLight);
	}

	smallSpotLight * spotLight = new smallSpotLight(glm::vec3(0, 0, 4), glm::vec3(0, 7, 0));
	currLighting->addLight(spotLight);

}

void WaterScene::load(Game* thisGame)
{
	gameRefr = thisGame;

	loadInit();

	examiner->setDirectoryObject("Objects\\");

	//loadCubeCircle();

	loadCubeGrid(10);

	loadPlayer();

	loadWaterPlane();

	loadLighting();

	loadCamera();

	loadWorld();
	
	setupPostProcessing();

	gameRefr->setConfigAll();
}

void WaterScene::loadIndexedCube()
{
	examiner->setFile("Objects/cubeSimple.obj");

	Model * cubeData = new Model();

	examiner->indexShapes(cubeData);

	Custom* cubeIndexed = new Custom(cubeData, glm::vec3(0,70,0), glm::vec3(1), glm::vec3(0));

	addGameObject(cubeIndexed);
}

void WaterScene::loadWorld()
{
	examiner->setFile("WaterWorld.obj");

	Model *  terrain = new Model();

	examiner->indexShapes(terrain);

	Terrain * terrainObject = new Terrain(terrain , new GrassyTerrainTextureData());
	terrainObject->setScale(glm::vec3(0.035f));
	terrainObject->setTilingFactor(1);
	addGameObject(terrainObject);
}

void WaterScene::setupPostProcessing()
{
	gameRefr->addPostProccessor(new ContrastPP(0.12f));
}