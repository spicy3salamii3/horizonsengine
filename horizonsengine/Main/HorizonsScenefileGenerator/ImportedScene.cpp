#include "ImportedScene.h"
#include <FirstPersonCamera.h>
#include <Game.h>
#include <Scenery.h>
#include <glm-0.9.9.2/glm/glm.hpp>

ImportedScene::ImportedScene()
{
}


ImportedScene::~ImportedScene()
{
}

void ImportedScene::run()
{

}
void ImportedScene::load(Game * refr)
{
	gameRefr = refr;
	sceneInit();

	loadPlayer();

	loadCamera();

	loadGUI();
}

void ImportedScene::loadGUI()
{

}
void ImportedScene::sceneInit()
{
	Scene::loadInit();
}

void ImportedScene::loadCamera()
{
	FirstPersonCamera * cam = new FirstPersonCamera(player);

	gameCamera = cam;

	gameRefr->setCamera(cam);
}

void ImportedScene::loadPlayer()
{
	player = new Custom(new Model(), glm::vec3(0), glm::vec3(1), glm::vec3(0));

	addGameObject(player);
}