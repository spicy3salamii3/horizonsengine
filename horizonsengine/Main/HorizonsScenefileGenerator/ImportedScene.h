#pragma once

#include <Scene.h>
class ImportedScene : public Scene
{
public:
	ImportedScene();
	~ImportedScene();

	void load(Game * refr);

	void run();

private:

	void loadGUI();

	void sceneInit();

	void loadCamera();

	void loadPlayer();

	Game * gameRefr;

	Camera * gameCamera;

	GameObject * player;
};

