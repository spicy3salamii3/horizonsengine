#include <EngineWrapper.h>
#include "ImportedScene.h"
int main(int argc, char* argv[]) 
{
	EngineWrapper * horizonsInstance = new EngineWrapper();


	horizonsInstance->initGL(argc, argv, "Horizons Development Scenes");

	horizonsInstance->sortConfig();

	Scene * levelOne = new ImportedScene();

	horizonsInstance->initializeGame(levelOne);


	horizonsInstance->startGame();

	return 0;
}