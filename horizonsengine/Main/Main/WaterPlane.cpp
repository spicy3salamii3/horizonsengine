#include "WaterPlane.h"
#include "WaterFBO.h"
#include "LightingState.h"
#include "Config.h"
#include "Light.h"

WaterPlane::WaterPlane(glm::vec2 d)
{
	setup();

	dimensions = d * 20.0f;
	intializeFBOGroup();
	applyScaling();
	sortVAO();



	ref = waterFBO->getReflectionTexture();
	refra = waterFBO->getRefractionTexture();
	depthMap = waterFBO->getDepthBuffer();
}


WaterPlane::~WaterPlane()
{
}

void WaterPlane::update(double f)
{
	Object3D::update(f);

	offset += waterSpeed * deltaTime;
	if (offset > 1.0f)
	{
		offset -= 1.0f;
	}

	wrc->offset = offset;

	Object3D::lateUpdate(f);

	thisFBO->update();
}


void WaterPlane::setup()
{
	wrc = new WaterRenderingComponent(this);

	wrc->dimensions = dimensions;
}

void WaterPlane::applyScaling()
{
	for (int i = 0; i< 6; i++)
	{
		vertices[i*3] *= dimensions.x;
		vertices[2+(i*3)] *= dimensions.y;
	}
}

void WaterPlane::draw(RenderSettings*rs)
{

	//Set the program, and enable alpha blending
	glUseProgram(thisConfig.waterShaders->getProgramID());
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Set the texturess
	thisConfig.waterShaders->setTextures();
	//Srt the offset
	thisConfig.waterShaders->setOffset(offset);

	//Set the mats
	thisConfig.waterShaders->setMats(T, R, S, rs->cameraMat);

	//Set the lighting uniforms
	thisConfig.waterShaders->setLightUniforms(thisConfig.currLighting);

	//Set tiling factor
	thisConfig.waterShaders->setTiling(dimensions);
	//Bind the correct textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ref);
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, refra);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, dudv);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, normal);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, depthMap);

	glBindVertexArray(thisVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisable(GL_BLEND);
}

void WaterPlane::sortVAO()
{
	glGenVertexArrays(1, &thisVAO);
	glBindVertexArray(thisVAO);

	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)* 3 * 6, vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	


	glBindVertexArray(0);

}

void WaterPlane::intializeFBOGroup()
{
	waterFBO = new WaterFBO(this);
	thisFBO = waterFBO;
}

