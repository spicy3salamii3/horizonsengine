#pragma once
#include "PublicBehaviour.h"
#include "GameObject.h"
#include <string>
#include "Text.h"

class Game;

class PrintPositions :public PublicBehaviour
{
public:
	PrintPositions(Game *);
	~PrintPositions();

	void addObject(GameObject *, std::string);

	void update(double);

private:

	Game * refr;

	int readingOut = 0;

	const static int MAX_TRACKED_OBJECTS = 4;
	GameObject * tracked[MAX_TRACKED_OBJECTS] = { nullptr };
	Text * readOuts[MAX_TRACKED_OBJECTS * 4] = { nullptr };

	float xoffset = 170.0f;
};