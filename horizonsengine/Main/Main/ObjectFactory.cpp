#include "ObjectFactory.h"
#include "InstancedObject3D.h"
#include "Triangle.h"
#include "Scenery.h"
#include "ComponentReference.h"
#include "Helpers.h"
#include "ObjExaminer.h"
#include "TerrainCollider.h"
#include "Terrain.h"
#include <bullet/btBulletCollisionCommon.h>
#include "Game.h"

ObjectFactory* ObjectFactory::thisPointer = nullptr;

ObjectFactory::ObjectFactory()
{

}

ObjectFactory::~ObjectFactory()
{
}

ObjectFactory* ObjectFactory::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new ObjectFactory();
	}
	return thisPointer;
}

Terrain * ObjectFactory::makeTerrain(Model * v, TerrainTextureData * t, float tile, float scale)
{
	Terrain* terrain = new Terrain(v, t);

	terrain->setTilingFactor(tile);

	terrain->setScale(glm::vec3(scale));

	return  terrain;
}

GameObject * ObjectFactory::makeInstancedObject(InstancedVAOData *iData)
{
	InstancedObject3D * instance;
	
	instance = new InstancedObject3D(iData);

	return instance;
}

GameObject * ObjectFactory::makeCustomObject(Model *iData, glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	Custom * instance;

	instance = new Custom(iData, a, b,c);

	return instance;
}

GameObject * ObjectFactory::makeCustomObject(Model *iData, float scale)
{
	Custom * instance;

	instance = new Custom(iData, glm::vec3(0),glm::vec3(scale), glm::vec3(0));

	return instance;
}

void ObjectFactory::addToTerrain(Model* data)
{
	//start making triangles from the raw data in the meshdata class


	if (triangles == nullptr)
	{
		triangles = new btTriangleMesh();
	}
	Triangle x = Triangle();


	for (int meshCount = 0; meshCount < data->getMeshCount(); meshCount++)
	{
		int totalTris = data->getMeshByIndex(meshCount)->getTrianglesCount();

		for (int triCount = 0; triCount < totalTris; triCount++)
		{
			x.p1 = data->getMeshByIndex(meshCount)->getVertexByIndex(triCount * 3);
			x.p2 = data->getMeshByIndex(meshCount)->getVertexByIndex((triCount * 3) + 1);
			x.p3 = data->getMeshByIndex(meshCount)->getVertexByIndex((triCount * 3) + 2);

			x.centre = getAverageOf3PointsXZ(x.p1, x.p2, x.p3);

			triangles->addTriangle(convert(x.p1), convert(x.p2), convert(x.p3));
			
		}
	}

}

btCollisionShape* ObjectFactory::createTerrainCollider()
{
	if (triangles == nullptr) return nullptr;
	btBvhTriangleMeshShape * collider = new btBvhTriangleMeshShape(triangles, true, true);
	return collider;
}

btCollisionShape * ObjectFactory::makeBoxCollider(GameObject * obj)
{

	Extents scaledExtents = obj->getScaledExtents();
	float x = scaledExtents.maxExtent.x - scaledExtents.minExtent.x;
	float y = scaledExtents.maxExtent.y - scaledExtents.minExtent.y;
	float z = scaledExtents.maxExtent.z - scaledExtents.minExtent.z;

	btVector3 halfBox = btVector3(x / 2.0f, y / 2.0f, z / 2.0f);

	return new btBoxShape(halfBox);
}

btCollisionShape* ObjectFactory::makeBoxCollider(Extents e)
{
	float x = e.maxExtent.x - e.minExtent.x;
	float y = e.maxExtent.y - e.minExtent.y;
	float z = e.maxExtent.z - e.minExtent.z;

	btVector3 halfBox = btVector3(x / 2.0f, y / 2.0f, z / 2.0f);

	return new btBoxShape(halfBox);
}

btCollisionShape * ObjectFactory::makeCylinderCollider( GameObject *host )
{
	Extents scaledExtents = host->getScaledExtents();

	float x = scaledExtents.maxExtent.x - scaledExtents.minExtent.x;
	float y = scaledExtents.maxExtent.y - scaledExtents.minExtent.y;
	float z = scaledExtents.maxExtent.z - scaledExtents.minExtent.z;

	btVector3 halfCylinder = btVector3(x / 2.0f, y / 2.0f, z / 2.0f);

	return new btCylinderShape(halfCylinder);
}

btCollisionShape* ObjectFactory::makeCylinderCollider(Extents e)
{
	Extents scaledExtents = e;

	float x = scaledExtents.maxExtent.x - scaledExtents.minExtent.x;
	float y = scaledExtents.maxExtent.y - scaledExtents.minExtent.y;
	float z = scaledExtents.maxExtent.z - scaledExtents.minExtent.z;

	btVector3 halfCylinder = btVector3(x / 2.0f, y / 2.0f, z / 2.0f);

	return new btCylinderShape(halfCylinder);
}




btCollisionShape* ObjectFactory::makeComplexCollider(Model* data)
{
	//start making triangles from the raw data in the meshdata class

	btConvexTriangleMeshShape* collider;
	Triangle x = Triangle();
	btTriangleMesh* triangles = new btTriangleMesh();


	for (int meshCount = 0; meshCount < data->getMeshCount(); meshCount++)
	{
		int totalTris = data->getMeshByIndex(meshCount)->getTrianglesCount();

		for (int triCount = 0; triCount < totalTris; triCount++)
		{
			x.p1 = data->getMeshByIndex(meshCount)->getVertexByIndex(triCount * 3);
			x.p2 = data->getMeshByIndex(meshCount)->getVertexByIndex((triCount * 3) + 1);
			x.p3 = data->getMeshByIndex(meshCount)->getVertexByIndex((triCount * 3) + 2);

		
			triangles->addTriangle(convert(x.p1), convert(x.p2), convert(x.p3));

		}
	}

	collider = new btConvexTriangleMeshShape(triangles);
	return collider;
}

btCollisionShape* ObjectFactory::makeComplexCollider(MeshData* data)
{
	//start making triangles from the raw data in the meshdata class

	btConvexTriangleMeshShape* collider;
	Triangle x = Triangle();
	btTriangleMesh* triangles = new btTriangleMesh();



	int totalTris = data->getTrianglesCount();

	std::cout << "Making Complex Colllider with " << totalTris << " Triangles\n";

	for (int triCount = 0; triCount < totalTris; triCount++)
	{
		x.p1 = data->getVertexByIndex(triCount * 3);
		x.p2 = data->getVertexByIndex((triCount * 3) + 1);
		x.p3 = data->getVertexByIndex((triCount * 3) + 2);


		triangles->addTriangle(convert(x.p1), convert(x.p2), convert(x.p3));
	}
	

	collider = new btConvexTriangleMeshShape(triangles);
	return collider;
}
