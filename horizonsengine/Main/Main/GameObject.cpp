#include "GameObject.h"

GameObject::GameObject()
{
	setup();
	setPos(glm::vec3(0));
	setScale(glm::vec3(1));
	setRot(glm::vec3(0));
}

void GameObject::update(double)
{


}

Extents GameObject::getWorldExtents()
{
	return e*(getTransMat() *getScaleMat());
}

Extents GameObject::getScaledExtents()
{
	Extents f = e * getScaleMat();
	return f;
}

glm::vec3 GameObject::getPos()
{
	return glm::vec3(T[3]);
}

glm::vec3 GameObject::getRot()
{
	return glm::vec3(rotX, rotY, rotZ);
}

glm::vec3 GameObject::getScale()
{
	glm::vec3 scale;
	glm::decompose(S, scale, glm::quat(), glm::vec3(), glm::vec3(), glm::vec4());
	return scale;
}

glm::mat4  GameObject::getRotMat()
{
	return R;
}

glm::mat4  GameObject::getTransMat()
{
	return T;
}

glm::mat4 GameObject::getScaleMat()
{
	return S;
}

glm::mat4  GameObject::getModelMat()
{
	if (modelMatChanged)
	{
		modelMat = T * R * S;
		modelMatChanged = false;
	}
	return modelMat;
}

void GameObject::setPos(glm::vec3 pos)
{
	T = glm::translate(glm::mat4(1), pos);

	modelMatChanged = true;
}

void GameObject::setRot(glm::vec3 pos)
{

	R = glm::toMat4(
		glm::quat(glm::vec3(0.0f, pos.y, 0.0f))* 
		glm::quat(glm::vec3(pos.x, 0.0f, 0.0f))* 
		glm::quat(glm::vec3(0.0f, 0.0f, pos.z))
	);

	rotX = pos.x;
	rotY = pos.y;
	rotZ = pos.z;

	modelMatChanged = true;
}

void GameObject::setRot(glm::quat q)
{
	R = glm::toMat4(q);
	glm::vec3 qV = glm::eulerAngles(q);
	rotX = qV.x;
	rotY = qV.y;
	rotZ = qV.z;

	modelMatChanged = true;

}

void GameObject::setScale(glm::vec3 pos)
{
	S = glm::scale(glm::mat4(1), pos);

	modelMatChanged = true;
}