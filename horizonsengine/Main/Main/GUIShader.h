#pragma once
#include "Shader.h"
class GUIShader :
	public Shader
{
public:
	static GUIShader * getInstance();
	~GUIShader() {}

	void setProj(glm::mat4);

	void setup();

	void setScreenPos(glm::mat4);

	void setScaleMat(glm::mat4);


	void prepareToRender(GameObject*);

	void prepareToRender(RenderSettings*);
private:
	GUIShader();

	std::string vertexFilename = "Shaders\\vertex_2d.shader";
	std::string fragFilename = "Shaders\\fragment_2d.shader";

	GLuint locScale = 0;
	GLuint locProj = 0;
	GLuint locPos = 0;
	static GUIShader* thisPointer;
};

