#include "AnimationComponent.h"
#include "GameObject.h"
AnimationComponent::AnimationComponent(GameObject * x) : Component(x)
{
	x->addPrivateBehaviour(new AnimationBehaviour(this));
}

int AnimationComponent::addAnimation(Animation2D * a)
{
	animations.push_back(a);
	return animations.size() - 1;
}


void AnimationComponent::setAnimation(int i)
{
	currentAnimation = animations[i];
	currentAnimation->reset();
}

void AnimationComponent::stepAnimation(double delta)
{
	if (currentAnimation == nullptr) return;

	currentAnimation->stepAnimation(delta);

	if (currentAnimation->isDone())
	{
		if (!loop)
		currentAnimation = nullptr;
	}
}