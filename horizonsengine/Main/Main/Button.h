#pragma once
#include "ImageRect.h"
#include "InputListener.h"
#include "Texture.h"
#include "Animation2D.h"
#include "AnimationComponent.h"

class ButtonAction
{
public:
	virtual void onClick(int, float x, float y) {}
};


class DebugCentre;
class Button :
	public virtual ImageRect, public InputListener
{
public:;
	Button(Texture* t, glm::vec3 scale,  ButtonAction * buttonA = nullptr, std::string label = "Button Generic");
	~Button();

	void update(double delta);

	void OnClick(int, float x, float y);

	void mouseMovePos(float, float);


private:
	void setup();

	int locMousePosX;
	int locPos;
	DebugCentre * dc;

	bool mouseOver = false;

	AnimationComponent * animator;

	int locAnimation;
	int locRevAnimation;
	Animation2D* zoomAnimation;
	Animation2D* revAnimation;

	ButtonAction * buttonAction;
};

