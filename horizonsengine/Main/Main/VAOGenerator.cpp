#include "VAOGenerator.h"

VAOGenerator * VAOGenerator::thisPointer = nullptr;

VAOGenerator::VAOGenerator()
{
}


VAOGenerator::~VAOGenerator()
{
}


VAOGenerator* VAOGenerator::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new VAOGenerator();
	}
	return thisPointer;
}

GLuint VAOGenerator::makeVAO(MeshData * thisMesh)
{
	GLuint VAO = 0;

	float * points = thisMesh->getPoints();
	float * norms = thisMesh->getNormals();
	float * texts = thisMesh->getTexts();
	float * tangents = thisMesh->getTangents();
	float * bitangents = thisMesh->getBitangents();
	GLuint * indices = thisMesh->getIndices();





	return GLuint();
}

GLuint VAOGenerator::makeVAOIndexed(MeshData *)
{
	return GLuint();
}

GLuint VAOGenerator::makeVAOInstanced(MeshData *)
{
	return GLuint();
}
