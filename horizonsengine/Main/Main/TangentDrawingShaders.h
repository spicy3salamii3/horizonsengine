#pragma once
#include "Shader.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
class TangentDrawingShaders :
	public Shader
{
public:

	~TangentDrawingShaders();
	static TangentDrawingShaders* getInstance();

	void setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S, glm::mat4 C);
	void setProj(glm::mat4);
private:
	TangentDrawingShaders();

	std::string vertFilename = "Shaders\\vertex_shader_tangent.shader";
	std::string geometryFilename = "Shaders\\geometry_shader_tangent.shader";
	std::string fragFilename = "Shaders\\fragment_shader_tangent.shader";

	void setup();

	static TangentDrawingShaders* thisPointer;

	GLuint locTt;

	GLuint locRt;

	GLuint locSt;

	GLuint locCt;

	GLuint tLocProj;
};

