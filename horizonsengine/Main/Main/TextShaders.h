#pragma once
#include "Shader.h"
#include <glm-0.9.9.2/glm/glm.hpp>
class TextShaders :
	public Shader
{
public:
	static TextShaders* getInstance();
	~TextShaders();

	void setProj(glm::mat4);
	void setColour(glm::vec3);

private:
	static TextShaders * thisPointer;

	TextShaders();

	GLuint locTextColour;
	GLuint locProj;

	void setup();

	std::string vertexFilename  = "Shaders/vertex_text.shader";
	std::string fragFilename = "Shaders/fragment_text.shader";
};

