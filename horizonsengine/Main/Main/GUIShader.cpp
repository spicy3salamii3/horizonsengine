#include "GUIShader.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Object2D.h"
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "AnimationComponent.h"

GUIShader * GUIShader::thisPointer = nullptr;

GUIShader::GUIShader()
{
	programID = setupShaders(vertexFilename, fragFilename);
	setup();
}

void GUIShader::setScreenPos(glm::mat4 mat)
{
	glUniformMatrix4fv(locPos, 1,GL_FALSE, glm::value_ptr(mat));
}


void GUIShader::setScaleMat(glm::mat4 x)
{
	glUniformMatrix4fv(locScale, 1, GL_FALSE, glm::value_ptr(x));
}

void GUIShader::prepareToRender(GameObject* go)
{
	GUITransform* guit = go->getComponent<GUITransform*>();

	setScreenPos(guit->getPosMat());

	setScaleMat(guit->getScaleMat());

	setProj(guit->getProj());
}

void GUIShader::prepareToRender(RenderSettings*)
{
}

GUIShader* GUIShader::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new GUIShader();
	}
	return thisPointer;
}

void GUIShader::setProj(glm::mat4 x)
{
	glUniformMatrix4fv(locProj, 1, GL_FALSE, (GLfloat*)glm::value_ptr(x));
}

void GUIShader::setup()
{
	locProj = glGetUniformLocation(programID, "proj");
	locPos = glGetUniformLocation(programID, "trans");

	locScale = glGetUniformLocation(programID, "scale");
}