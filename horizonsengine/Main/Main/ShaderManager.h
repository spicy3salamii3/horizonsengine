#pragma once
#include "Shader.h"
#include <vector>

class ShaderManager
{public:

	void addShader(Shader * x)
	{
		shaders.push_back(x);
	}

	void beginFrame()
	{
		for (int i =0; i < shaders.size(); i++)
		{
			shaders[i]->newFrame();
		}
	}

	template<typename T>
	inline T getShader()
	{
		for (Shader* currShader : shaders)
		{
			T currentEntry = dynamic_cast<T>(currShader);
			if (currentEntry != nullptr)
			{
				return currentEntry;
			}
		}
	}

	static ShaderManager* singleton()
	{
		if (thisPointer == nullptr)
		{
			thisPointer = new ShaderManager();
		}
		return thisPointer;
	}
	
private:
	std::vector<Shader* > shaders;

	static ShaderManager* thisPointer;

	ShaderManager() {}
};