#include "SkyboxShaders.h"
#include <CoreStructures/CoreStructures.h>
#include "GameObject.h"

SkyboxShaders* SkyboxShaders::thisPointer = nullptr;

SkyboxShaders::SkyboxShaders()
{
	programID = setupShaders(vertexFilename, fragmentFilename);
	setup();
}


SkyboxShaders::~SkyboxShaders()
{
}

SkyboxShaders * SkyboxShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new SkyboxShaders();
	}
	return thisPointer;
}

void SkyboxShaders::setup()
{
	locCs = glGetUniformLocation(programID, "view");

	locCP = glGetUniformLocation(programID, "clipPlane");
	locCPbool= glGetUniformLocation(programID, "usePlane");
	locScale = glGetUniformLocation(programID, "scale");
	locTransform = glGetUniformLocation(programID, "finalTransform");
}

void SkyboxShaders::prepareToRender(GameObject* go)
{
	setMats(go->getModelMat());

	setScale(go->getScale().x);
}

void SkyboxShaders::prepareToRender(RenderSettings* rs)
{
	setCameraMat(rs->cameraMat);

	setClipPlane(rs->clipPlane, rs->useClipPlane);
}


void SkyboxShaders::setMats(glm::mat4 TRS)
{
	glUniformMatrix4fv(locTransform, 1, GL_FALSE, (GLfloat*)& TRS);
}
void SkyboxShaders::setCameraMat(glm::mat4 C)
{
	glUniformMatrix4fv(locCs, 1, GL_FALSE, (GLfloat*)& C);
}


void SkyboxShaders::setMats(glm::mat4 finalTrans, glm::mat4 C, glm::mat4 P)
{
	setCameraMat(C);
	setMats(finalTrans);
}

void SkyboxShaders::setScale(float x)
{
	glUniform1f(locScale, x);
}