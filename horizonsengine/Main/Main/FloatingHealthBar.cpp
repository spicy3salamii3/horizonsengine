#include "FloatingHealthBar.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include "Camera.h"
FloatingHealthBar::FloatingHealthBar(GLuint bTex, GLuint pTex, glm::vec2 res, float pX, float pY, bool facingCamera ) 
{
	barRes = res;
	barTex = bTex;
	progressTex = pTex;

	progressRes.x = barRes.x * pX;
	progressRes.y = barRes.y * pY;

	progWidth = progressRes.x/WorldGUI::unitsPerPixel;
	faceCamera = facingCamera;
}


FloatingHealthBar::~FloatingHealthBar()
{
	delete barGUI;
	delete progressGUI;
}

void FloatingHealthBar::setup(float m)
{
	maxProgress = m;
	Texture* t = new Texture(barTex, barRes, "");
	barGUI = new WorldGUI(t);
	Texture* text = new Texture(progressTex, progressRes, "");
	progressGUI = new WorldGUI(text);
	//text = new Text();

	barGUI->setConfig(thisConfig);
	progressGUI->setConfig(thisConfig);
	//text->setConfig(thisConfig);
}

void FloatingHealthBar::setProgress(float x)
{
	progress = x;

	//text->setText(std::to_string(progress) + "/" + std::to_string(maxProgress));
}

void FloatingHealthBar::update(double x)
{
	Object3D::update(x);
	float yRot = 0.0f;
	if (faceCamera)
	{
		if (thisConfig.currLighting->currCamera != nullptr)
		{
			yRot = thisConfig.currLighting->currCamera->getCameraAngle().y;

		}
	}

	glm::vec3 rot = getRot();
	rot.y = yRot + M_PI;
	setRot(rot);

	barGUI->setPos(getPos());
	barGUI->setScale(getScale());
	barGUI->setRot(getRot());


	float progressDistance = -progWidth *(1-(progress/maxProgress));
	//bring the progess bar slightly forward so that it doesn't glitch into the bar
	glm::vec3 gg =getRotMat()*getScaleMat() * glm::vec4(progressDistance, 0, 0.03f, 1);
	progressGUI->setPos(getPos() + gg );

	glm::vec3 progressScale = glm::vec3((progress / maxProgress),1,1);
	progressGUI->setScale(getScale()* progressScale );
	progressGUI->setRot(getRot());


	//text->setPos(getPos()+ gg*2.0f);
	//text->setScale(getScale());
	//text->setRot(getRot());

	barGUI->update(x);
	progressGUI->update(x);
	//text->update(x);

	Object3D::lateUpdate(x);
}

void FloatingHealthBar::draw(RenderSettings * rs)
{
	progressGUI->draw(rs);
	barGUI->draw(rs);
	//text->draw(rs);
}