#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>

class Model;
class Camera;
class Light;
class ShadowBox;

class LightingState
{
public:

	LightingState();

	glm::vec3 cameraPos;
	int numberOfLights = 0;

	const static int MAX_LIGHTS = 12;
	Light* lights[MAX_LIGHTS] = { nullptr };

	void addLight(Light* newLight);

	glm::mat4 cameraWithOutPersp;
	glm::mat4 persp;

	Model* sphereData;
	Camera* currCamera;
};