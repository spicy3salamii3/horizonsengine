#include "Text.h"
#include <glm-0.9.9.2/glm/ext.hpp>
#include "Game.h"


Text::Text(std::map<GLchar, Character> f)
{
	initTransform();
	sortVAO();
	font = f;
	transform->anchorType = GUI_TRANSFORM_ANCHOR_TL;
}


Text::~Text()
{
}

void Text::update(double)
{

}


void Text::draw(RenderSettings *rs)
{
	float minY = 0.0f;

	DisplayDetails * dd = Game::singleton()->getDisplayDetails();

	float x = transform->getPos().x;



	if ((transform->anchorType == GUI_TRANSFORM_ANCHOR_CEN )&& centred)
	{
		x -= ( abs(transform->res.x)) / 2.0f;

	}

	float startX = x;

	float y = transform->getPos().y -(yOffset*transform->getRelativeScale().y);

	glUseProgram(thisConfig.textShaders->getProgramID());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	thisConfig.textShaders->setProj(transform->getProj());

	thisConfig.textShaders->setColour(colour);

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);

	// Iterate through all characters
	std::string::const_iterator c;

	for (c = text.begin(); c != text.end(); c++)
	{
		Character ch = font[*c];

		GLfloat xpos = x + ch.Bearing.x * transform->getRelativeScale().x;
		GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * transform->getRelativeScale().y;

		GLfloat w = ch.Size.x * transform->getRelativeScale().x;
		GLfloat h = ch.Size.y * transform->getRelativeScale().y;
		// Update VBO for each character
		GLfloat vertices[6][4] = {
		{ xpos,     ypos + h,   0.0, 0.0 },
		{ xpos,     ypos,       0.0, 1.0 },
		{ xpos + w, ypos,       1.0, 1.0 },

		{ xpos,     ypos + h,   0.0, 0.0 },
		{ xpos + w, ypos,       1.0, 1.0 },
		{ xpos + w, ypos + h,   1.0, 0.0 }
		};
		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.textureID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * transform->getRelativeScale().x; // Bitshift by 6 to get value in pixels (2^6 = 64)

		minY = glm::min(ypos, minY);
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisable(GL_BLEND);

	if(centred)
	transform->res = glm::vec2(abs(x- startX), abs(y- minY));
}

void Text::sortVAO()
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}