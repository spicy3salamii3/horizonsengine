#pragma once
#include "FBO.h"
class ShadowFBO :
	public FBOGroup
{
public:
	ShadowFBO(int width, int height);
	~ShadowFBO();

	void update();

	void bind();

	void unBind();

	GLuint getDepthTexture() { return texture; }
private:
	void setupFBO();

	GLuint frameBuffer = 0;

	GLuint texture = 0;
	int w;
	int h;
	int initialWidth;
	int initialHeight;


};

