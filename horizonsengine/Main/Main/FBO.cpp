#include "FBO.h"
#include "Game.h"


FBOGroup::FBOGroup()
{
	flagsWantedByPass |= PassFlagTypes::AllObjects;
}


FBOGroup::~FBOGroup()
{
}

ScreenBuffer::ScreenBuffer()
{
	//FBO id etcs are all left as 0 as just rendering to frame buffer
	dd = Game::singleton()->getDisplayDetails();
	bufferCount = 1;
}