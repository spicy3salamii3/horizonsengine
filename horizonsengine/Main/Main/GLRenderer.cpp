#include "GLRenderer.h"
#include "Game.h"
#include "ShaderManager.h"

GLRenderer* GLRenderer::thisPointer = nullptr;

GLRenderer::GLRenderer()
{

}

GLRenderer::~GLRenderer()
{

}

GLRenderer* GLRenderer::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new GLRenderer();
	}
	return thisPointer;
}

void GLRenderer::initialize()
{
	DisplayDetails* dd = Game::singleton()->getDisplayDetails();
	
	glViewport(0,0, dd->width, dd->height);

}

void GLRenderer::update(double)
{
	screenBuffer->setCameraAngle(0, gameCamera->getCameraMat());
}

void GLRenderer::setScene(Scene* scene)
{
	//For now - delete all content from old scene
	renderPasses.contents.clear();

	currScene = scene;

	renderPasses.commonPart = currScene;

	PassData* pd = new PassData();

	screenBuffer = new ScreenBuffer();

	pd->commonPart = screenBuffer;

	renderPasses.contents.push_back(pd);

	//To do - 
	//Add 3 passes - 
	//Main
	//Transparent (does this need to be later in the chain? not sure)
	//GUI


	//Then loop through and add in any existing game objects as currently this will only work on empty scenes

}

//Scene is in for multi-scene support in future
void GLRenderer::onAddFBO(FBOGroup* fbo, Scene* scene)
{
	//For now - add FBO to every pass

	PassData* pd = new PassData();

	pd->commonPart = fbo;

	renderPasses.contents.push_back(pd);

	sortPasses();
}

void GLRenderer::sortPasses()
{

}

void GLRenderer::addPostProcessor(PostProcessingShader*)
{

}

void GLRenderer::removePostProcessor(PostProcessingShader*)
{

}

void GLRenderer::onAddGameObject(GameObject* go)
{
	RenderConditions* rc = go->getComponent<RenderConditions*>();

	if (rc == nullptr) 
	{
		std::cout << "No render component\n";
		return;
	}

	//For now, every item added to every FBOgroup. Can look in future about limiting to relevant passes
	for each (PassData * pd in renderPasses.contents)
	{
		addToPassData(pd, go, rc);
	}
}

void GLRenderer::addToPassData(PassData* pd, GameObject* go, RenderConditions * rc )
{
	//For every shader type already in the pass data, check if this is the shader in use. If not already featured, add it to the passdata's shader list
	ShaderData* correctShader = nullptr;
	for each (ShaderData * sd in pd->contents)
	{
		//
		if (rc->shaderInUse == sd->commonPart)
		{
			//This is the right shader!
			correctShader = sd;
			break;
		}
	}

	if (correctShader != nullptr)
	{
		//The shader is featured this pass! Nothing to be done :)
	}
	else
	{
		//The shader isn't featured in this pass yet!
		correctShader = new ShaderData();
		correctShader->commonPart = rc->shaderInUse;
		pd->contents.push_back(correctShader);
	}

	addToShaderData(correctShader, go, rc);
}

void GLRenderer::addToShaderData(ShaderData* sd, GameObject* go, RenderConditions* rc)
{
	//For every material featured in the shader data, check if this is the correct material, and add to it. If the materiala is not found, add a new material data
	if (rc->model == nullptr)
	{
		std::cout << "Model was null, object name " << go->name << std::endl;
		return;
	}


	for (int meshCount = 0; meshCount < rc->model->getMeshCount(); meshCount++)
	{
		if (rc->getMaterial(meshCount) == nullptr)
		{
			std::cout << "Material was null, object name " << go->name << " mesh index " << meshCount << std::endl;
			return;
		}
		MaterialData* correctMaterial = nullptr;

		for each (MaterialData * md in sd->contents)
		{
			//
			if (rc->getMaterial(meshCount) == md->commonPart)
			{
				//This is the right Material!
				correctMaterial = md;
				break;
			}
		}

		if (correctMaterial != nullptr)
		{
			//The Material is featured this pass! Nothing to be done :)
		}
		else
		{
			//The shader isn't featured in this pass yet!
			correctMaterial = new MaterialData();
			correctMaterial->commonPart = rc->getMaterial(meshCount);
			sd->contents.push_back(correctMaterial);
		}

		addToMaterialData(correctMaterial, go, rc);
	}


}

void GLRenderer::addToMaterialData(MaterialData* md, GameObject* go, RenderConditions* rc)
{
	//For every model featured in the shader data, check if this is the correct model, and add to it. If the model is not found, add a new model data
	ModelData* correctModel = nullptr;

	for each (ModelData * md in md->contents)
	{
		//
		if (rc->model == md->commonPart)
		{
			//This is the right model!
			correctModel = md;
			break;
		}
	}

	if (correctModel != nullptr)
	{
		//The model is featured this pass! Nothing to be done :)
	}
	else
	{
		//The model isn't featured in this pass yet!
		correctModel = new ModelData();
		correctModel->commonPart = rc->model;
		md->contents.push_back(correctModel);
	}

	addToModelData(correctModel, go);
}

void GLRenderer::addToModelData(ModelData* md, GameObject* go)
{
	//No validation here - every game object is assumed to be unique
	md->contents.push_back(go);

	std::cout << "Added " << go->name << " to GLRenderer\n";
}

void GLRenderer::render()
{
	//This Method will run every frame and draw all models to screen
	ShaderManager::singleton()->beginFrame();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_BLEND);

	for each (PassData * pd in renderPasses.contents)
	{
		pd->commonPart->update();
		for (int count = 0; count < pd->commonPart->getTotalPasses(); count++)
		{
			renderToFrameBuffer(pd, count);
		}
	}
}

void GLRenderer::renderToFrameBuffer(PassData *pd, int i )
{
	pd->commonPart->bindFBOByIndex(i);

	rs = new RenderSettings(pd->commonPart->getCameraAngleRequiredByIndex(i), pd->commonPart->getClipPlaneUseBoolByIndex(i), pd->commonPart->getClipPlaneByIndex(i));

	for each (ShaderData * sd in pd->contents)
	{
		renderShaderData(sd);
	}

	pd->commonPart->unBind();

	delete rs;
}

void GLRenderer::renderShaderData(ShaderData* sd)
{

	glUseProgram(sd->commonPart->getProgramID());

	sd->commonPart->setTextures();

	//Camera needs to be set please!

	currentShader = sd->commonPart;



	currentShader->prepareToRender(rs);
	

	for each (MaterialData * md in sd->contents)
	{
		renderMaterialData(md);
	}
}

void GLRenderer::renderMaterialData(MaterialData* Md)
{
	currentShader->setNormalBool(false);

	Md->commonPart->Bind();

	currentShader->setLighting(Md->commonPart->shininess, currScene->currLighting);

	for each (ModelData * md in Md->contents)
	{
		renderModelData(md);
	}
}

void GLRenderer::renderModelData(ModelData* md)
{
	for (int meshIndex = 0; meshIndex < md->commonPart->getMeshCount(); meshIndex++)
	{
		int indicesLength = md->commonPart->getElementsCountByIndex(meshIndex);


		glBindVertexArray(md->commonPart->getVAOByIndex(meshIndex));

		for each (GameObject * go in md->contents)
		{
			currentShader->prepareToRender(go);

			glDrawElements(GL_TRIANGLES, indicesLength, GL_UNSIGNED_INT, (void*)0);
		}
	}
}
