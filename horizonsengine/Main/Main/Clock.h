#pragma once

#define CLOCK_PHYSICS_START 0
#define CLOCK_PHYSICS_END 1
#define CLOCK_RENDER_START 2
#define CLOCK_RENDER_END 3

class Clock
{
public:
	Clock() {}
	~Clock() {}
	void tick();
	float getDelta() { return delta; }
	float getAvgDelta() { return avgDelta; }
	void end();
	int getFPS() { return avgFPS; }

	void writeTimeForStep(int i);

private:
	double ticks = 0.0f;
	double lastTime = 0.0f;
	float delta = 0.0f;
	float avgDelta = 0.0f;
	float avgDeltaCounter = 0.0f;
	double frames = 0.0f;

	int frameTicker = 0;

	void delay();

	float ticksCounter =0.0f;
	float frameCounter = 0;
	const float target = 1.5f;

	float avgFPS = 0;

	float avgFPS10 = 0;

	double physicsTimer = 0.0;
	double renderTimer = 0.0;
};