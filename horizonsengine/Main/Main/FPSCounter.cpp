#include "FPSCounter.h"
#include <iostream>
#include "Game.h"

FPSCounter::FPSCounter(Game * newRefr) :refr(newRefr)
{
	text = new Text(newRefr->getConfig().codeFontCharacters);
	text->setPos(glm::vec3(10.0f, 10.0f, 0.0f));
	text->setConfig(refr->getConfig());
}


FPSCounter::~FPSCounter()
{
}


void FPSCounter::update(double delta)
{
	int fps = 1000.0f / delta;

	text->setText("FPS: "+ std::to_string(fps));

	text->update(delta);
}

void FPSCounter::draw(RenderSettings * rs)
{
	text->draw(rs);
}