#pragma once
#include <stdio.h>
using namespace std;
#include "ObjExaminer.h"
#include <iostream>
#include <math.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include "VAOData.h"
#include "Scene.h"
#include <ft2build.h>
#include "Bounds.h"
#include FT_FREETYPE_H
#include <string>
#include "GameObject.h"
#include "PhysicsComponent.h"
#include "Extents.h"
#include "Game.h"
#include "PhysicsHandler.h"
#include "MaterialHandler.h"

#define OE_DIFF 0
#define OE_NORM 1
#define OE_SPEC 2

using namespace tinyobj;

ObjExaminer* ObjExaminer::thisPointer = nullptr;

ObjExaminer* ObjExaminer::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new ObjExaminer();
	}
	return thisPointer;
}

ObjExaminer::ObjExaminer()
{
	textExam = TextureExaminer::singleton();

	objectMaker = ObjectFactory::getInstance();
}

ObjExaminer::~ObjExaminer()
{

}

std::string * ObjExaminer::getShapeNames()
{
	return nullptr;
}

void ObjExaminer::importPoints()
{

	//a container for any error that might happen
	std::string err;

	//Load the object into the right containers
	bool result = tinyobj::LoadObj(&attrib, &shapes, &materials, &err,( objectDirectory + currFilename).c_str(), objectDirectory.c_str());

	if (!err.empty())
	{
		std::cout << err << "\n";
	}
}

Extents ObjExaminer::getExtents()
{
	return e;
}

//This method is called once per shape in an object
void ObjExaminer::sortPoints(bool zero, Bounds2DXZ restrictions)
{
	minExtent = glm::vec3(1000000);
	maxExtent = glm::vec3(-1000000);
	//Since this class is a singleton, reset all variables used
	totalVerts = 0;

	averageTot = glm::vec3(0, 0, 0);

	int count = 0;

	//Variables used for the tangent calculations
	glm::vec3 v0;
	glm::vec3 v1;
	glm::vec3 v2;

	glm::vec2 w0;
	glm::vec2 w1;
	glm::vec2 w2;

	glm::vec3 deltaPos1;
	glm::vec3 deltaPos2;

	glm::vec2 deltaUV1;
	glm::vec2 deltaUV2;

	glm::vec3 tangent;
	glm::vec3 bitangent;

	glm::vec3 minPoint = glm::vec3(1000000);
	glm::vec3 maxPoint = glm::vec3(-1000000);

	glm::vec3 testers[3];

	real_t nx, ny, nz, vx, vy, vz, tx, ty;

	int triCount = 0;

	//Looping through shapes, faces, then vertexes

	for (size_t s = 0; s < shapes.size(); s++) {
		// Loop over faces(polygon)
		index_t idx;
		size_t index_offset = 0;
		bool tanCalcs = true;
		//If the shape name is master or fits the current shape name, proceed to import the points
		if ( currShapeName == shapes[s].name)
		{
			int assignCount = shapes[s].mesh.indices.size();
			basicAllocated = true;
			points = new float[assignCount*3];
			norms = new float[assignCount * 3];
			texts = new float[assignCount * 2];
			tangents = new float[assignCount * 4];
			bitangents = new float[assignCount * 4];

			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
			{
				//For each Face
				bool triangleIncluded = false;
				int fv = shapes[s].mesh.num_face_vertices[f];

				// Loop over triangles in the face

				for (size_t v = 0; v < fv; v++) {

					//this section of code is PER VERTEX

					//Add each vertex, normal and tex coord to the temp containers
					idx = shapes[s].mesh.indices[index_offset + v];

					//vertexes
					vx = attrib.vertices[3 * idx.vertex_index + 0];
					vy = attrib.vertices[3 * idx.vertex_index + 1];
					vz = attrib.vertices[3 * idx.vertex_index + 2];
					//fill in the testing vector
					testers[v] = glm::vec3(vx, vy, vz);

					//fill in the points variable. if this triangle isn't included, this will be overriding data
					points[count * 3] = vx;
					points[(count * 3) + 1] = vy;
					points[(count * 3) + 2] = vz;

					if (idx.normal_index >= 0.0f)
					{
						//Normals
						nx = attrib.normals[3 * idx.normal_index + 0];
						ny = attrib.normals[3 * idx.normal_index + 1];
						nz = attrib.normals[3 * idx.normal_index + 2];

						norms[count * 3] = nx;
						norms[(count * 3) + 1] = ny;
						norms[(count * 3) + 2] = nz;
					}
					else
					{
						tanCalcs = false;
					}

					if (idx.texcoord_index >= 0.0f) {
						//Texture Co-ords
						tx = attrib.texcoords[2 * idx.texcoord_index + 0];
						ty = attrib.texcoords[2 * idx.texcoord_index + 1];


						texts[count * 2] = tx;
						texts[(count * 2) + 1] = ty;
					}
					else
					{
						tanCalcs = false;
						std::cout << "No Texture Coords in model\n";
					}

					//increase the counter!
					count++;
					//Add to the average pos 

					averageTot += testers[v];

					//Extents calculations
					if (vx > maxExtent.x) maxExtent.x = vx;
					if (vx < minExtent.x) minExtent.x = vx;
					if (vy > maxExtent.y) maxExtent.y = vy;
					if (vy < minExtent.y) minExtent.y = vy;
					if (vz > maxExtent.z) maxExtent.z = vz;
					if (vz < minExtent.z) minExtent.z = vz;

					if (testers[v].x > maxPoint.x&& testers[v].y > maxPoint.y && testers[v].z > maxPoint.z) maxPoint = testers[v];

					if (testers[v].x < minPoint.x&& testers[v].y < minPoint.y && testers[v].z < minPoint.z) minPoint = testers[v];

					//Get the vertex and texture coord vectors for tangent calculations, aka positions and uvs for the three points

					if (tanCalcs) {
						if (v == 0)
						{
							v0 = glm::vec3(vx, vy, vz);
							w0 = glm::vec2(tx, ty);
						}
						else if (v == 1)
						{
							v1 = glm::vec3(vx, vy, vz);
							w1 = glm::vec2(tx, ty);
						}
						else
						{
							v2 = glm::vec3(vx, vy, vz);
							w2 = glm::vec2(tx, ty);
						}
					}
				}

				triangleIncluded = isWithinBounds((testers[0] + testers[1] + testers[2]) / 3.0f, restrictions);

				//This section of code is PER TRIANGLE. Count is at the first vertex of the next triangle.
				//if triangle fits in the bounds
				if (triangleIncluded)
				{

					if (tanCalcs)//turn off tangent calculations
					{
						deltaPos1 = v1 - v0;
						deltaPos2 = v2 - v0;

						deltaUV1 = w1 - w0;
						deltaUV2 = w2 - w0;

						float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
						tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y)*r;
						bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x)*r;


						tangents[(triCount * 12)] = tangent.x;
						tangents[(triCount * 12) + 1] = tangent.y;
						tangents[(triCount * 12) + 2] = tangent.z;
						tangents[(triCount * 12) + 3] = 0.0f;

						tangents[(triCount * 12) + 4] = tangent.x;
						tangents[(triCount * 12) + 5] = tangent.y;
						tangents[(triCount * 12) + 6] = tangent.z;
						tangents[(triCount * 12) + 7] = 0.0f;

						tangents[(triCount * 12) + 8] = tangent.x;
						tangents[(triCount * 12) + 9] = tangent.y;
						tangents[(triCount * 12) + 10] = tangent.z;
						tangents[(triCount * 12) + 11] = 0.0f;


						bitangents[(triCount * 12)] = bitangent.x;
						bitangents[(triCount * 12) + 1] = bitangent.y;
						bitangents[(triCount * 12) + 2] = bitangent.z;
						bitangents[(triCount * 12) + 3] = 0.0f;

						bitangents[(triCount * 12) + 4] = bitangent.x;
						bitangents[(triCount * 12) + 5] = bitangent.y;
						bitangents[(triCount * 12) + 6] = bitangent.z;
						bitangents[(triCount * 12) + 7] = 0.0f;

						bitangents[(triCount * 12) + 8] = bitangent.x;
						bitangents[(triCount * 12) + 9] = bitangent.y;
						bitangents[(triCount * 12) + 10] = bitangent.z;
						bitangents[(triCount * 12) + 11] = 0.0f;
					}
					triCount++;
				}
				else
				{
					//Reduce the counter! this triangle isn't important and can be written over/ignored!
					count -= 3;
				}


				index_offset += fv;

				// per-face material
				shapes[s].mesh.material_ids[f];

			}
		}

	}

	//work out the average point aka centre
	averageTot /= count;
	
	totalVerts = count;

	e.minExtent = minExtent;
	e.maxExtent = maxExtent;
	e.minPoint = minPoint;
	e.maxPoint = maxPoint;

	float medx = minExtent.x + ((maxExtent.x - minExtent.x) / 2);
	float medy = minExtent.y + ((maxExtent.y - minExtent.y) / 2);
	float medz = minExtent.z + ((maxExtent.z - maxExtent.z) / 2);

	glm::vec3 avg = glm::vec3(medx, medy, medz);
	
	e.centre = averageTot;

	if (zero) moveToOrigin(false);

}

void ObjExaminer::moveToOrigin(bool useMinY)
{
	
	for (int point = 0; point < totalVerts; point++)
	{
		//x
		points[point*3] -= averageTot.x;

		//y
		if (useMinY)
		{
			//used so the base is at 0 y rather than the centre
			points[(point * 3) + 1] -= minExtent.y;
		}
		else
		{
			points[(point * 3) + 1] -= averageTot.y;
		}

		//z
		points[(point * 3) + 2] -= averageTot.z;
	}
}

Texture* ObjExaminer::getTexture(int type, int shapeNo)
{
	int matID;

	textExam->setDirectory(textureDirectory);

	matID = shapes[shapeNo].mesh.material_ids[0];

	if (matID < 0) return  0;

	Texture* texture = 0;
	switch (type)
	{
	case 0:
		texture = textExam->getTexture(materials[matID].diffuse_texname);
		break;
	case 1:
		texture = textExam->getNormalTexture(materials[matID].bump_texname);
		if (texture == 0) texture = textExam->getNormalTexture(materials[matID].displacement_texname);
		break;
	case 2:
		texture = textExam->getNormalTexture(materials[matID].specular_texname);
		break;
	}

	return texture;
}

std::string ObjExaminer::removeNumber(std::string x)
{
	char b;
	int indexOfFirstNumber = x.length();
	for (int count = 0; count < x.length(); count++)
	{
		b = x[count];
		if (isdigit(b))
		{
			//it is a number
			indexOfFirstNumber = count;
			break;
		}
	}
	return x.substr(0, indexOfFirstNumber);
}

void ObjExaminer::indexShapes(Model * currData, std::string shapeName, bool zero)
{
	//Since this class is a singleton, reset all variables used
	//totalVerts = 0;
	int meshCount = shapes.size();

	for (int shapeCount = 0; shapeCount < meshCount; shapeCount++)
	{
#pragma region For Each Shape In File

		if (!(shapeName == "" || shapeName == "Master"))
		{
			if (shapes[shapeCount].name != shapeName)
			{
				continue;
			}
		}

		//Set the current shape name
		currShapeName = shapes[shapeCount].name;

		Texture *  diff = getTexture(0, shapeCount);

		Texture *  norm = getTexture(1, shapeCount);

		Texture * spec = getTexture(2, shapeCount);

		recentMaterial = MaterialHandler::singleton()->makeMaterial(diff, spec, norm);

		//use the normal sortPoints method to sort all the vertexes into buffers
		sortPoints(zero);

		MeshData * currMesh = indexMesh(false);

		deleteBasic();

		currMesh->setExtents(e);

		printf("Object imported, total vertexes = %d. Indexing now...\n", totalVerts);


		currData->addMesh(currMesh);
	
#pragma endregion 
	}

}

MeshData* ObjExaminer::indexMesh(int instances)
{

	float Va1, Va2, Va3, Vb1, Vb2, Vb3, Na1, Na2, Na3, Nb1, Nb2, Nb3, Ta1, Ta2, Tb1, Tb2;

	MeshData * currMesh;

	int count2;
	bool foundSimilarIndex;
	//Loop through the vertex, normal and texcoord buffers and find similar combinations
	int indexesDone = 0;
	int vertexCount = 0;
	int indexInBuffer = 0;
	int vertCount3 = 0;
	int vertCount2 = 0;
	int indexInBuffer3 = 0;
	int indexInBuffer2 = 0;

	//Allocate some memory for the vertex buffers. To be deleted.
	indexingBufferV = new float[totalVerts*3];
	indexingBufferN = new float[totalVerts*3];
	indexingBufferTex = new float[totalVerts*2];
	indexingBufferTan = new float[totalVerts*4];
	indexingBufferBitan = new float[totalVerts*4];
	indexBuffer = new int[totalVerts];

	//For each vertex in shape
	for (vertexCount; vertexCount < totalVerts; vertexCount++)
	{
		
		vertCount3 = vertexCount * 3;
		vertCount2 = vertexCount * 2;

		Va1 = points[vertCount3];
		Va2 = points[(vertCount3)+1];
		Va3 = points[(vertCount3)+2];

		Na1 = norms[vertCount3];
		Na2 = norms[(vertCount3)+1];
		Na3 = norms[(vertCount3)+2];

		Ta1 = texts[vertCount2];
		Ta2 = texts[(vertCount2)+1];

		foundSimilarIndex = false;

		//Loop through previous entries to the index buffer and see if there is a similar one.

		count2 = 0;
		//after the indexing accuracy figure is passed, only check that many vertexes for index pairings. 
		if (indexesDone > 5 + indexingAccuracy) count2 = indexesDone - indexingAccuracy;

		for (count2; count2 < indexesDone; count2++)
		{
			if (!foundSimilarIndex)
			{
				indexInBuffer = indexBuffer[count2];
				indexInBuffer3 = indexInBuffer * 3;
				indexInBuffer2 = indexInBuffer * 2;

				Vb1 = indexingBufferV[indexInBuffer3];
				Vb2 = indexingBufferV[(indexInBuffer3)+1];
				Vb3 = indexingBufferV[(indexInBuffer3)+2];

				Nb1 = indexingBufferN[indexInBuffer3];
				Nb2 = indexingBufferN[(indexInBuffer3)+1];
				Nb3 = indexingBufferN[(indexInBuffer3)+2];

				Tb1 = indexingBufferTex[indexInBuffer2];
				Tb2 = indexingBufferTex[(indexInBuffer2)+1];

				//NEsted Ifs to save processor time
				if (Vb1 == Va1)
				{
					if ((Vb2 == Va2) && (Nb1 == Na1) && (Ta1 == Ta1))
					{
						if ((Vb3 == Va3) && (Nb2 == Na2) && (Ta2 == Tb2))
						{
							if (Nb3 == Na3)
							{

								//Found a similar vertex on all counts! mark this index as the current indexInBuffer, and don't add a new index
								indexBuffer[vertexCount] = indexInBuffer;

								indexingBufferTan[indexInBuffer * 4] += tangents[vertexCount * 4];
								indexingBufferTan[(indexInBuffer * 4) + 1] += tangents[(vertexCount * 4) + 1];
								indexingBufferTan[(indexInBuffer * 4) + 2] += tangents[(vertexCount * 4) + 2];
								indexingBufferTan[(indexInBuffer * 4) + 3] += tangents[(vertexCount * 4) + 3];

								indexingBufferBitan[indexInBuffer * 4] = bitangents[vertexCount * 4];
								indexingBufferBitan[(indexInBuffer * 4) + 1] += bitangents[(vertexCount * 4) + 1];
								indexingBufferBitan[(indexInBuffer * 4) + 2] += bitangents[(vertexCount * 4) + 2];
								indexingBufferBitan[(indexInBuffer * 4) + 3] += bitangents[(vertexCount * 4) + 3];

								foundSimilarIndex = true;
								break;
							}
						}
					}
				}
			}//End of if foundSimialr = false
		}

		if (foundSimilarIndex)
		{
			//do not modify indexes done, there are the same number of indexes.
		}

		else
		{
			//create new index entry
			indexBuffer[vertexCount] = indexesDone;
			indexingBufferV[indexesDone * 3] = points[vertexCount * 3];
			indexingBufferV[(indexesDone * 3) + 1] = points[(vertexCount * 3) + 1];
			indexingBufferV[(indexesDone * 3) + 2] = points[(vertexCount * 3) + 2];

			indexingBufferN[indexesDone * 3] = norms[vertexCount * 3];
			indexingBufferN[(indexesDone * 3) + 1] = norms[(vertexCount * 3) + 1];
			indexingBufferN[(indexesDone * 3) + 2] = norms[(vertexCount * 3) + 2];

			indexingBufferTex[indexesDone * 2] = texts[vertexCount * 2];
			indexingBufferTex[(indexesDone * 2) + 1] = texts[(vertexCount * 2) + 1];

			indexingBufferTan[indexesDone * 4] = tangents[vertexCount * 4];
			indexingBufferTan[(indexesDone * 4) + 1] = tangents[(vertexCount * 4) + 1];
			indexingBufferTan[(indexesDone * 4) + 2] = tangents[(vertexCount * 4) + 2];
			indexingBufferTan[(indexesDone * 4) + 3] = tangents[(vertexCount * 4) + 3];

			indexingBufferBitan[indexesDone * 4] = bitangents[vertexCount * 4];
			indexingBufferBitan[(indexesDone * 4) + 1] = bitangents[(vertexCount * 4) + 1];
			indexingBufferBitan[(indexesDone * 4) + 2] = bitangents[(vertexCount * 4) + 2];
			indexingBufferBitan[(indexesDone * 4) + 3] = bitangents[(vertexCount * 4) + 3];

			indexesDone++;
		}

	}//End of all the vertexes

	deleteBasic();

	indexedPoints = new float[indexesDone * 3];
	indexedNorms = new float[indexesDone * 3];
	indexedTexts = new float[indexesDone * 2];
	indexedTangents = new float[indexesDone * 4];
	indexedBitangents = new float[indexesDone * 4];
	indices = new GLuint[vertexCount];
	

	for (int count3 = 0; count3 < (indexesDone * 3); count3++)
	{
		indexedPoints[count3] = indexingBufferV[count3];
		indexedNorms[count3] = indexingBufferN[count3];
	}

	for (int count3 = 0; count3 < (indexesDone * 2); count3++)
	{
		indexedTexts[count3] = indexingBufferTex[count3];
	}

	for (int count3 = 0; count3 < (indexesDone * 4); count3++)
	{
		indexedTangents[count3] = indexingBufferTan[count3];
		indexedBitangents[count3] = indexingBufferBitan[count3];
	}

	for (int count3 = 0; count3 < vertexCount; count3++)
	{
		indices[count3] = indexBuffer[count3];
	}

	deleteIndexBuffers();

	indicesLength = vertexCount;

	uniqueVertexes = indexesDone;

	currMesh = new MeshData(0, indexedPoints,
		indexedNorms, indexedTangents, indexedBitangents, indexedTexts, indices, indicesLength, uniqueVertexes);

	

	if (instances == 0)
	{
		sortIndexedVAO();
	}
	else
	{
		sortInstancedVAO(instances);
	}
	currMesh->setVAO(shapeVAO);



	printf("Object index, saved %d vertexes\n\n", totalVerts - indexesDone);


	//Delete the old points/ norms etc

	return currMesh;
}

glm::mat4 ObjExaminer::makeInstancingMat(std::string shapeName, int instIndex, Scene * scene)
{
	int shapeID = 0;
	for (int count = 0; count < shapes.size(); count++)
	{
		if (shapes[count].name == shapeName)
		{
			shapeID = count;
			break;
		}
	}


	Extents e =calcShapeExtents(shapeID);


	return glm::translate(glm::mat4(1), e.centre) * glm::scale(glm::mat4(1), glm::vec3(scMain.scale));
}

//Note, does not calculate the max and min points, just the extents
Extents ObjExaminer::calcShapeExtents(int shapeID, bool moveToOrigin)
{
	Extents e;
	int count = 0;
	//Extents used for translate calculations
	glm::vec3 minExtent = glm::vec3(1000000);
	glm::vec3 maxExtent = glm::vec3(-1000000);
	glm::vec3 avgTot = glm::vec3(0);
	glm::vec3 tester;


	size_t index_offset = 0;

	for (size_t f = 0; f < shapes[shapeID].mesh.num_face_vertices.size(); f++)
	{
		int fv = shapes[shapeID].mesh.num_face_vertices[f];
		// Loop over triangles in the face
		for (size_t v = 0; v < fv; v++) {
			//Add each vertex, normal and tex coord to the temp containers

			index_t idx = shapes[shapeID].mesh.indices[index_offset + v];

			real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
			real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
			real_t vz = attrib.vertices[3 * idx.vertex_index + 2];

			if (vx > maxExtent.x) maxExtent.x = vx;
			if (vx < minExtent.x) minExtent.x = vx;
			if (vy > maxExtent.y) maxExtent.y = vy;
			if (vy < minExtent.y) minExtent.y = vy;
			if (vz > maxExtent.z) maxExtent.z = vz;
			if (vz < minExtent.z) minExtent.z = vz;
			tester.x = vx;
			tester.y = vy;
			tester.z = vz;
			avgTot += tester;
			count++;
		}
		index_offset += fv;
	}

	float medx = minExtent.x + ((maxExtent.x - minExtent.x) / 2);
	float medy = minExtent.y + ((maxExtent.y - minExtent.y) / 2);
	float medz = minExtent.z + ((maxExtent.z - maxExtent.z) / 2);

	glm::vec3 avg = glm::vec3(medx, medy, medz);

	avgTot /= count;

	averageTot = avgTot;

	e.centre = avgTot;
	e.minExtent = minExtent;
	e.maxExtent = maxExtent;

	if (moveToOrigin)
	{
		//Implement a - operator for extents
		e = e - avgTot;
	}


	return e;
}

std::string ObjExaminer::addNumber(std::string x, int c)
{
	std::string number = "";
	if (c > 99)
	{
		number = to_string(c);
	}
	else if (c > 9)
	{
		number = "0" + to_string(c);
	}
	else if (c >= 0)
	{
		number = "00" + to_string(c);
	}
	return x + number;
}

#pragma region Index Scene Pipeline

void ObjExaminer::indexScene(Scene * scene, SceneConfig x)
{
	int meshCount = shapes.size();
	scMain = x;

	textExam->setDirectory(textureDirectory);

	bool categorised = false;
	//Loop through shapes in the scene. Add any terrain or other keyword object. Tot up number of custom, and instanced objects needed
	for (int shapeCount = 0; shapeCount < meshCount; shapeCount++)
	{
		categorised = false;

		for (int count = 0; count < x.getInstancible(); count++)
		{
			std::string noNumberName = removeNumber(shapes[shapeCount].name);
			std::string in = x.instancible[count];

			if (noNumberName == in)
			{
				bool firstRead = true;
				//check if this is the first time this object has been noticed
				for (int count2 = 0; count2 < readShapesCount; count2++)
				{
					if (readShapes[count2] == noNumberName)
					{
						//already read and instanced. add to the count of instances
						repeatCounts[count2] += 1;
						firstRead = false;
						objectMaker->handleCustomPrefix(shapes[shapeCount]);
						categorised = true;
						break;
					}
				}

				if (firstRead)
				{

					readShapes[readShapesCount] = removeNumber(shapes[shapeCount].name);
					repeatCounts[readShapesCount] = 1;
					ogShapeID[readShapesCount] = shapeCount;
					readShapesCount++;

					std::cout << "New instancible object " << noNumberName << "\n\n";
					categorised = true;
					objectMaker->handleCustomPrefix(shapes[shapeCount]);
					continue;
				}
				//This object has been categorised
				
			}
		}

		if (categorised)
		{
			continue;
		}

		if (shapes[shapeCount].name.find(x.terrainKeyword) != string::npos && terrainCount <x.terrainCount)
		{
			printf("Found a terrain object\n");
			addTerrain(shapes[shapeCount].name, scene);
			//This object has been categorised
			continue;
		}

		
		if (shapes[shapeCount].name.substr(0, x.emptyParentKeyword.length()).find(x.emptyParentKeyword) != string::npos)
		{
			continue;
		}
	
		if (shapes[shapeCount].name.substr(0, x.waypointKeyword.length()).find(x.waypointKeyword) != string::npos)
		{
			printf("Found a waypoint\n");
			currShapeName = shapes[shapeCount].name;
			sortPoints(false);
			scene->addWaypoint(new Waypoint(glm::vec3(points[0], points[1], points[2]), shapes[shapeCount].name));
			deleteBasic();
			//This object has been categorised
			continue;
		}

		int keywordLen = x.collisionVolumeKeyword.length();

		//Looking for collision volumes
		if (shapes[shapeCount].name.substr(0, keywordLen).find(x.collisionVolumeKeyword) != string::npos)
		{
			//Found a collision volume!
			//Add it
			volumeShapeIDs.push_back(shapeCount);
			continue;
		}

		keywordLen = x.pathKeyword.length();

		if (shapes[shapeCount].name.substr(0, keywordLen).find(x.pathKeyword) != string::npos)
		{
			printf("Found a path\n");

			currShapeName = shapes[shapeCount].name;


			sortPoints(false);

			Path* newPath = new Path();

			glm::vec3 pointOnPath;
			for (int pointsCount = 0; pointsCount < totalVerts; pointsCount++)
			{
				pointOnPath.x = points[(pointsCount * 3)];
				pointOnPath.y = points[(pointsCount * 3)+1];
				pointOnPath.z = points[(pointsCount * 3)+2];
			}
			deleteBasic();
			//This object has been categorised
			continue;
		}

		//Doesn't fit anywhere else? ask object factory to handle a custom shape prefix... if not, just add it as a custom
		
		if (!objectMaker->handleCustomPrefix(shapes[shapeCount]))
			addCustom(shapeCount, scene);
	}

	//Finish off the instanced objects
	sortInstancedObjects(scene);

	//load in the static volumes 


	sortCollisionShapes(scene);

	//Turn all of the static objects into one huge terrain
	btCollisionShape* terr = objectMaker->createTerrainCollider();
	if ( terr!= nullptr)
	scene->addHostlessCollider(terr, glm::mat4(1));


	printf("Done\n");
}

void ObjExaminer::sortInstancedObjects(Scene * scene)
{
	//Loop through the objects that need to be instanced and add them
	InstancedVAOData* iData;

	for (int instCount = 0; instCount < readShapesCount; instCount++)
	{
		//Create a new Instanced Data
		iData = new InstancedVAOData();
		std::string shapeName;

		//Setup the textures for the instanced object
		Texture *  diff = getTexture(OE_DIFF, ogShapeID[instCount]);
		Texture* spec = getTexture(OE_SPEC, ogShapeID[instCount]);
		Texture* norm = getTexture(OE_NORM, ogShapeID[instCount]);

		//Get the base name for the instanced object
		currShapeName = readShapes[instCount];

		//create a list of matricies for the instances
		modelMatrices = new glm::mat4[repeatCounts[instCount]];

		//loop through all  the shapes that are instanced and generate a model matrix for them 
		for (int instancesCount = 1; instancesCount <= repeatCounts[instCount]; instancesCount++)
		{
			//get the shape name (base name plus 001, 002 etc
			shapeName = addNumber(currShapeName, instancesCount);
			//generate the matrix
			modelMatrices[instancesCount - 1] = makeInstancingMat(shapeName, instCount, scene);// 
		}

		//now, find the first and actually read in the shape data
		currShapeName = readShapes[instCount] + "001";

		//sort the points, and zero the object (move it to the origin)
		sortPoints(true);

		//create a mesh data
		MeshData * data = indexMesh(repeatCounts[instCount]);

		//clean up
		deleteBasic();

		//set the number of instances in the data
		iData->setInstances(repeatCounts[instCount]);

		//add the mesh data
		iData->addMesh(data);

		//create a gameobjecty
		GameObject * inst = objectMaker->makeInstancedObject(iData);


		inst->getComponent<RenderConditions*>()->setMaterial(MaterialHandler::singleton()->makeMaterial(diff, spec, norm));

		//add it to the scene
		scene->addGameObject(inst);

		inst->name = currShapeName;
	}
}

void ObjExaminer::sortCollisionShapes(Scene * scene)
{
	//Loop through volume shapeIDs and create them
	int boxKeyLen = scMain.boxVolumeKeyword.length();
	int keywordLen = scMain.collisionVolumeKeyword.length();
	int cylKeyLen = scMain.cylinderVolumeKeyword.length();
	int convKeyLen = scMain.convexTriKeyword.length();

	for (int count = 0; count < volumeShapeIDs.size(); count++)
	{


		Extents volumeExt = calcShapeExtents(volumeShapeIDs[count], true);

		std::string typeAndHostName = shapes[volumeShapeIDs[count]].name.substr(keywordLen);


		if (typeAndHostName.substr(0, boxKeyLen).find(scMain.boxVolumeKeyword) != string::npos)
		{
			//Box
			scene->addHostlessCollider(objectMaker->makeBoxCollider(volumeExt), glm::translate(glm::mat4(), averageTot));
		}

		if (typeAndHostName.substr(0, cylKeyLen).find(scMain.cylinderVolumeKeyword) != string::npos)
		{
			//Cylinder
			scene->addHostlessCollider(objectMaker->makeCylinderCollider(volumeExt), glm::translate(glm::mat4(), averageTot));
		}

		if (typeAndHostName.substr(0, convKeyLen).find(scMain.convexTriKeyword) != string::npos)
		{
			//Convex

			currShapeName = shapes[volumeShapeIDs[count]].name;

			sortPoints(false);

			MeshData* convData = indexMesh(false);

			deleteBasic();

			scene->addHostlessCollider(objectMaker->makeComplexCollider(convData), glm::translate(glm::mat4(), averageTot));

			delete convData;
		}
	}
}

void ObjExaminer::addCustom(int shapeIndex, Scene * scene)
{
	Model * currData = new Model();

	//Set the current shape name
	currShapeName = shapes[shapeIndex].name;

	Texture *  diff = getTexture(0, shapeIndex);

	Texture* norm = getTexture(1, shapeIndex);

	Texture* spec = getTexture(2, shapeIndex);

	//use the normal sortPoints method to sort all the vertexes into buffers. This will set the object to the origin, and also provide a position to set it at (averagepos)
	sortPoints(true);

	MeshData * currMesh = indexMesh(0);

	deleteBasic();

	printf("Object imported, total vertexes = %d. Indexing now...\n", totalVerts);

	currData->addMesh(currMesh);

	GameObject * cust = objectMaker->makeCustomObject(currData, scMain.scale);

	cust->getComponent<RenderConditions*>()->setMaterial(MaterialHandler::singleton()->makeMaterial(diff, spec, norm));

	cust->e = e- averageTot;

	cust->setPos(scMain.scale*averageTot);

	scene->addGameObject(cust);

	cust->name = currShapeName;

}

void ObjExaminer::addTerrain( std::string shapeName, Scene * scene)
{
	currShapeName = shapeName;
	//use the normal sortPoints method to sort all the vertexes into buffers

	sortPoints(false);
	deleteBasic();
	Bounds2DXZ totalBounds = Bounds2DXZ(e.minExtent, e.maxExtent);

	int totalGridSections = pow(scMain.terrainDivisor, 2);
	float sectionWidth = totalBounds.width / scMain.terrainDivisor;
	float sectionHeight = totalBounds.height / scMain.terrainDivisor;

	//create a vao data for all the terrain meshes to fit into
	Model * terrain = new Model();

	printf("Terrain imported, total vertexes = %d. Indexing and dividing in to %d sections now...\n", totalVerts, totalGridSections);

	int x =0;
	int y =0;

	//Divide the terrain into a grid and create one mesh data per grid object
	for (int gridCount = 0; gridCount < totalGridSections; gridCount++)
	{
		y = gridCount / scMain.terrainDivisor;
		x = gridCount - (y* scMain.terrainDivisor);

		glm::vec2 bl = glm::vec2( totalBounds.bottomLeft);
		glm::vec2 tr = glm::vec2(totalBounds.bottomLeft);

		bl.x += x * sectionWidth;
		bl.y += y * sectionHeight;
		
		tr.x += (x + 1) * sectionWidth;
		tr.y += (y + 1) * sectionHeight;

		Bounds2DXZ sectionBounds = Bounds2DXZ(bl, tr);

		sortPoints(false, sectionBounds);

		MeshData * currMesh = indexMesh(false);

		terrain->addMesh(currMesh);

		deleteBasic();
	}

	Terrain *  terr = objectMaker->makeTerrain(terrain, scMain.texData, scMain.tilingFactor, scMain.scale);

	objectMaker->addToTerrain(terrain);

	scene->addGameObject(terr);

	terr->name = currShapeName;
}

#pragma endregion

#pragma region VAO Production
void ObjExaminer::sortVAO()
{
	vertsVBO = 0;
	normsVBO = 0;
	textsVBO = 0;

	shapeVAO = 0;

	//Make the VAO
	glGenVertexArrays(1, &shapeVAO);
	glBindVertexArray(shapeVAO);


	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts*3, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	//The normal Buffer
	glGenBuffers(1, &normsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts*3, norms, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	//the tangent buffer
	glGenBuffers(1, &tangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, tangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts * 4, tangents, GL_STATIC_DRAW);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(3);

	glGenBuffers(1, &bitangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, bitangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts * 4, bitangents, GL_STATIC_DRAW);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(4);

	//and the texture co-ord buffer
	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts*2, texts, GL_STATIC_DRAW);
	glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(8);


	//Unbind VAO
	glBindVertexArray(0);

	deleteBasic();
}

void ObjExaminer::sortIndexedVAO()
{
	vertsVBO = 0;
	normsVBO = 0;
	textsVBO = 0;

	shapeVAO = 0;

	//Make the VAO
	glGenVertexArrays(1, &shapeVAO);
	glBindVertexArray(shapeVAO);


	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 3, indexedPoints, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);


	//The normal Buffer
	glGenBuffers(1, &normsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 3, indexedNorms, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);


	//and the texture co-ord buffer
	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 2, indexedTexts, GL_STATIC_DRAW);
	glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(8);


	glGenBuffers(1, &tangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, tangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 4, indexedTangents, GL_STATIC_DRAW);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(3);

	glGenBuffers(1, &bitangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, bitangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 4, indexedBitangents, GL_STATIC_DRAW);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(4);

	glGenBuffers(1, &indicesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesLength * sizeof(GLuint), indices, GL_STATIC_DRAW);


	//Unbind VAO
	glBindVertexArray(0);

	deleteIndexed();

	indices = nullptr;
}

void ObjExaminer::sortInstancedVAO(int instances)
{
	vertsVBO = 0;
	normsVBO = 0;
	textsVBO = 0;

	shapeVAO = 0;

	modelMatriciesVBO = 0;

	//Make the VAO
	glGenVertexArrays(1, &shapeVAO);
	glBindVertexArray(shapeVAO);


	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 3, indexedPoints, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);


	//The normal Buffer
	glGenBuffers(1, &normsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 3, indexedNorms, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);


	//and the texture co-ord buffer
	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 2, indexedTexts, GL_STATIC_DRAW);
	glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(8);


	glGenBuffers(1, &tangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, tangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 4, indexedTangents, GL_STATIC_DRAW);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(3);

	glGenBuffers(1, &bitangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, bitangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*uniqueVertexes * 4, indexedBitangents, GL_STATIC_DRAW);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(4);

	glGenBuffers(1, &indicesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesLength * sizeof(GLuint), indices, GL_STATIC_DRAW);

	deleteIndexed();

	indices = nullptr;

	glGenBuffers(1, &modelMatriciesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, modelMatriciesVBO);
	glBufferData(GL_ARRAY_BUFFER, instances * sizeof(glm::mat4), &modelMatrices[0], GL_STATIC_DRAW);

	// vertex Attributes
	GLsizei vec4Size = sizeof(glm::vec4);
	glEnableVertexAttribArray(9);
	glVertexAttribPointer(9, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
	glEnableVertexAttribArray(10);
	glVertexAttribPointer(10, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(vec4Size));
	glEnableVertexAttribArray(11);
	glVertexAttribPointer(11, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
	glEnableVertexAttribArray(12);
	glVertexAttribPointer(12, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

	glVertexAttribDivisor(9, 1);
	glVertexAttribDivisor(10, 1);
	glVertexAttribDivisor(11, 1);
	glVertexAttribDivisor(12, 1);

	//Unbind VAO
	glBindVertexArray(0);

	deleteInstanced();

}

#pragma endregion

#pragma region Cleanup


void ObjExaminer::deleteBasic()
{ 
	if (!basicAllocated) return;
	delete[] points;
	delete[] norms;
	delete[] texts;
	delete[] tangents;
	delete[] bitangents;

	points = nullptr;
	norms = nullptr;
	texts = nullptr;
	tangents = nullptr;
	bitangents = nullptr;

	basicAllocated = false;
}

void ObjExaminer::deleteIndexBuffers()
{
	delete indexingBufferV;
	delete indexingBufferN;
	delete indexingBufferTex;
	delete indexingBufferTan;
	delete indexingBufferBitan;
	delete indexBuffer;


	indexingBufferV = nullptr;
	indexingBufferN = nullptr;
	indexingBufferTex = nullptr;
	indexingBufferTan = nullptr;
	indexingBufferBitan = nullptr;
	indexBuffer = nullptr;
}

void ObjExaminer::deleteIndexed()
{
	delete[] indexedPoints;
	delete[] indexedNorms;
	delete[] indexedTexts;
	delete[] indexedTangents;
	delete[] indexedBitangents;
	delete[] indices;

}

void ObjExaminer::deleteInstanced()
{

	delete[] modelMatrices;
}
#pragma endregion

