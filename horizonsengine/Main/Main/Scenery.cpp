#include "Scenery.h"



Custom::Custom(Model* newModel, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot)
{
	setPos(pos);
	setScale(scale);
	setRot(rot);
	tempModelStorage = newModel;

	setup();


	shinyness = 15;
}


Custom::~Custom()
{

}

void Custom::setup()
{
	getComponent<RenderConditions*>()->model = tempModelStorage;
}
