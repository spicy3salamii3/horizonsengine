#pragma once
#include "Object3D.h"
#include "TerrainTextureData.h"
#include "TerrainShaders.h"
class Terrain :
	public Object3D
{
public:
	Terrain(Model * ,TerrainTextureData * t);
	~Terrain();

	void update(double);

	void draw(RenderSettings*);

	void setTilingFactor(int x) { tilingFactor = x; }
protected:

	int tilingFactor = 20;

	TerrainTextureData * texData =nullptr;

	TerrainShaders * defaultShadersAsTerr = nullptr;
};

