#include "Terrain.h"



Terrain::Terrain(Model*x, TerrainTextureData * t)
{
	RenderConditions* rc = new RenderConditions(this);

	rc->model = x;

	addComponent(rc);

	texData = t;

	tilingFactor = 80;

	shinyness = 750;
}


Terrain::~Terrain()
{
}

void Terrain::update(double x)
{
	Object3D::update(x);

	Object3D::lateUpdate(x);
	defaultShaders = thisConfig.terrShaders;
	defaultShadersAsTerr = thisConfig.terrShaders;
}

void Terrain::draw(RenderSettings * rs) ///////Normal map needs to be perma- on
{
	glUseProgram(defaultShaders->getProgramID());

	defaultShadersAsTerr->setTilingFactor(tilingFactor);
	defaultShadersAsTerr->setInvertY(false);


	defaultShaders->setMats(T, R, S, rs->cameraMat);
	defaultShaders->setClipPlane(rs->clipPlane, rs->useClipPlane);

	//Parent Relative Transform stuff
	if (parent == nullptr) { defaultShaders->setParentLoc(glm::vec4(0.0f)); }
	else { defaultShaders->setParentLoc(glm::vec4(0, 0, 0, 0.0f)); }

	//Set all of the lighting uniforms
	defaultShaders->setLighting(shinyness, thisConfig.currLighting);

	//Bind all of the textures
	for (int count2 = 0; count2 < 4; count2++)
	{
		glActiveTexture(GL_TEXTURE0 + count2);
		glBindTexture(GL_TEXTURE_2D, texData->bases[count2]);
	}
	for (int count2 = 0; count2 < 4; count2++)
	{
		glActiveTexture(GL_TEXTURE4 + count2);
		glBindTexture(GL_TEXTURE_2D, texData->normals[count2]);
	}
	for (int count2 = 0; count2 < 4; count2++)
	{
		glActiveTexture(GL_TEXTURE8 + count2);
		glBindTexture(GL_TEXTURE_2D, texData->specs[count2]);
	}

	glActiveTexture(GL_TEXTURE12);
	glBindTexture(GL_TEXTURE_2D, texData->splat);

	//for each mesh in object
	for (int count = 0; count < renderConditions->model->getMeshCount(); count++)
	{
		//Tell the shader if a normal map is being used or not
		defaultShaders->setNormalBool((renderConditions->model->getNormalMapByIndex(count) != 0));

		//grab a copy of the vao for this object
		GLuint VAO = renderConditions->model->getVAOByIndex(count);

		//bind the VAO
		glBindVertexArray(VAO);

		//work out the indices length of the mesh
		int indicesLength = renderConditions->model->getElementsCountByIndex(count);

		// Draw the triangles !
		glDrawElements(GL_TRIANGLES, indicesLength, GL_UNSIGNED_INT, (void*)0);
	}

	//unbind all 16 textures
	for (int count = 0; count < 13; count++)
	{
		glActiveTexture(GL_TEXTURE0+count);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	if (rs->renderTangents || thisConfig.debugSettings->drawTangents)
	{
		drawTangents(rs->cameraMat);
	}

	if (rs->renderNormals || thisConfig.debugSettings->drawNormals)
	{
		drawNormals(rs->cameraMat);
	}
}