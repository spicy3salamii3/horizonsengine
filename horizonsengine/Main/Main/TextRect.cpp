#include "TextRect.h"



TextRect::TextRect()
{
	scaleX = 1.0f;
	applyScaling();

}


TextRect::~TextRect()
{
}

void TextRect::applyScaling()
{
	for (int i = 0; i< totalPoints; i++)
	{
		points[i] *= scaleX;
	}
}

void TextRect::sortVAO()
{
	//Setup the VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	//Add the points to the VBO
	glGenBuffers(1, &pointsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, pointsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, texts, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);
}


void TextRect::draw(RenderSettings*)
{

	glUseProgram(thisConfig.guiShaders->getProgramID());

	glBindVertexArray(VAO);

	glBindTexture(GL_TEXTURE_2D, texture);

	glDrawArrays(GL_QUADS, 0, 4);

}

