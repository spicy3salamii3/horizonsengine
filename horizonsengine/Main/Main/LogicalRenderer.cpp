#include "LogicalRenderer.h"
#include "LightingState.h"
#include "Light.h"
#include "Camera.h"
#include "Config.h"
#include "ShadowShaders.h"
#include "ShadowBox.h"
#include  <iostream>
#include "ShaderManager.h"
#include "Game.h"
#include "RenderConditions.h"

LogicalRenderer::LogicalRenderer()
{

}

LogicalRenderer::~LogicalRenderer()
{
	delete finalRender;
	delete partialRender;
	delete ppFBO;
	delete msFBO;
	for each (PostProccessingFBO *x in auxPPFBOs)
	{
		delete x;
	}
}

void LogicalRenderer::initialize()
{
	ppFBO = new PostProccessingFBO(thisConfig, 0);

	msFBO = new PostProccessingFBO(thisConfig, thisConfig.displayDetails->aaAmount);

	GLuint ppTex = ppFBO->getTexture();

	finalRender = new PostProcessRect(ppTex);

	partialRender = new PostProcessRect(ppTex);

	partialRender->setConfig(thisConfig);

	finalRender->setConfig(thisConfig);

	finalRender->setShader(thisConfig.postShaders);
}

void LogicalRenderer::update(double x)
{
	
	if (gameCamera != nullptr)
	{
		cameraMatrix = gameCamera->getCameraMat();
	}


	deltaTime = x;
}

void LogicalRenderer::render(void)
{
	RenderSettings* rsGUI = new RenderSettings(glm::mat4(1), false, glm::vec4(0));

	if (thisConfig.debugSettings->statePrints) printf("Starting render\n");

	renderToEachFBOGroup();

	if (thisConfig.debugSettings->statePrints) printf("rendered to each fbo\n\n");

	msFBO->bind();


	renderScene(cameraMatrix);
	if (thisConfig.debugSettings->statePrints) printf("finished Render to ms fbo\n");


	msFBO->unBind();

	if (thisConfig.debugSettings->statePrints) printf("finished rendering the ms fbo\n");


	msFBO->blitTo(ppFBO);

	if (thisConfig.debugSettings->statePrints) printf("blitted to the PP fbo\n");


	doPostProcessing();

	if (thisConfig.debugSettings->statePrints) printf("post processing done and rendered\n");

	renderGUI(rsGUI);

	if (thisConfig.debugSettings->statePrints) printf("finished GUI render\n\n");

	delete rsGUI;
}

void LogicalRenderer::doPostProcessing()
{
	RenderSettings * rs = new RenderSettings(glm::mat4(1), false, glm::vec4(0));

	textureResults[0] = ppFBO->getTexture();

	for (int count = 0; count < auxPPFBOs.size(); count++)
	{
		auxPPFBOs[count]->bind();

		auxPPFBOs[count]->shader->update();

		partialRender->setShader(auxPPFBOs[count]->shader);
		partialRender->setTexture(textureResults[count]);
		partialRender->draw(rs);

		auxPPFBOs[count]->unBind();

		textureResults[count + 1] = auxPPFBOs[count]->getTexture();
	}


	finalRender->setTexture(textureResults[ppCount]);

	finalRender->update(deltaTime);


	finalRender->draw(rs);

	delete rs;
}

void LogicalRenderer::renderToEachFBOGroup()
{
	std::vector<ReflectiveObject*> refObjects = currScene->getReflectiveObjects();

	if (thisConfig.debugSettings->statePrints) printf("Got num of ref objects\n");

	//Add some code like "If visible in main camera render"

	for (int count = 0; count < refObjects.size(); count++)
	{
		renderToFBOGroup(refObjects[count]);
	}

	Light * currLight;


	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	for (int count = 0; count < thisConfig.currLighting->numberOfLights; count++)
	{
		if (thisConfig.currLighting->lights[count]->castShadows)
		{
			currLight = thisConfig.currLighting->lights[count];

			renderShadowMap(currLight);
		}
	}
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	if (thisConfig.debugSettings->statePrints) printf("Rendered to any FBOs\n");

}

void LogicalRenderer::renderShadowMap(Light * light)
{
	ShadowShaders * sShaders = ShadowShaders::getInstance();

	thisConfig.shaderManager->beginFrame();

	glUseProgram(sShaders->getProgramID());

	//ensure shadows are setup
	if (!light->shadowsSetup) light->setupShadows();

	//update the fbo group
	light->shadowFBOGroup->update();

	//Bind the shadow FBO
	light->shadowFBOGroup->bind();

	//Get the shadown box for this camera
	ShadowBox * sBox = gameCamera->getShadowBox();

	//update the shadow box's location and si
	sBox->update(light);

	//update the light view matrix
	light->updateLightViewMatrix(sBox->getCenter());
	//light->updateLightViewMatrix(gameCamera->getCameraPos());

	//get the projection matrix
	light->projMatrix = sBox->getOrthoProjectionMatrix();
	//light->projMatrix = glm::ortho(-400.0f, 400.0f, -400.0f, 400.0f, 0.1f, 800.0f);

	//Actually render the shit

	//replace this vector with a vector of objects that fit into the shadow box
	std::vector<GameObject*> shadowObjects = currScene->getObjects();

	for (int count = 0; count < shadowObjects.size(); count++)
	{
		GameObject* currObject = shadowObjects[count];
		Model* data = shadowObjects[count]->getComponent<RenderConditions*>()->model;
		
		if (data == nullptr) continue;

		sShaders->setMats(light->projMatrix,  light->getLightViewMatrix(), currObject->getTransMat(), currObject->getRotMat(), currObject->getScaleMat() );

		int max = data->getMeshCount();

		for (int meshCount = 0; meshCount < max; meshCount++)
		{
			MeshData * mData = data->getMeshByIndex(meshCount);

			glBindVertexArray(mData->getVAO());

			glDrawElements(GL_TRIANGLES, mData->getElementsCount(), GL_UNSIGNED_INT, (void*)0);
		}

	}
	//and unbind it
	light->shadowFBOGroup->unBind();

	//Tell the light what it's map is
	light->shadowMap = light->shadowFBOGroup->getDepthTexture();
}

void LogicalRenderer::renderToFBOGroup(ReflectiveObject * thisObject)
{
	glm::mat4 thisAngle;
	int max = thisObject->getFBOCount();

	thisConfig.shaderManager->beginFrame();

	for (int count = 0; count < max; count++)
	{
		thisAngle = thisObject->getCameraAngleRequiredByIndex(count);
		
		RenderSettings * rs = new RenderSettings(thisAngle, thisObject->getClipBoolByIndex(count), thisObject->getClipPlaneByIndex(count));
		
		thisObject->bindFBOByIndex(count);

		renderSceneFBO(rs, thisObject);
		
		thisObject->unBind();

		delete rs;
	}
}

void LogicalRenderer::renderToFBOGroup(FBOGroup * thisGroup)
{
	glm::mat4 thisAngle;
	int max = thisGroup->getTotalPasses();

	thisConfig.shaderManager->beginFrame();


	for (int count = 0; count < max; count++)
	{
		thisAngle = thisGroup->getCameraAngleRequiredByIndex(count);

		RenderSettings * rs = new RenderSettings(thisAngle,thisGroup->getClipPlaneUseBoolByIndex(count), thisGroup->getClipPlaneByIndex(count));

		thisGroup->bindFBOByIndex(count);

		renderSceneFBO(rs, thisGroup);

		thisGroup->unBind();

		delete rs;
	}
}

void LogicalRenderer::renderSceneFBO(RenderSettings*rs, ReflectiveObject* obj)
{
	renderGameObjects(rs);

	renderTransparentObjects(rs);

	renderReflectiveObjects(rs, obj);
}

void LogicalRenderer::renderSceneFBO(RenderSettings*rs, FBOGroup* obj)
{
	renderGameObjects(rs);

	renderTransparentObjects(rs);

	renderReflectiveObjects(rs,nullptr);
}

void LogicalRenderer::renderScene(glm::mat4 camera)
{
	RenderSettings * rs = new RenderSettings(camera, false, glm::vec4(0));
	thisConfig.shaderManager->beginFrame();


	if (thisConfig.debugSettings->statePrints) printf("Starting actual render\n");

	renderGameObjects(rs);

	if (thisConfig.debugSettings->statePrints) printf("Rendered gameobjects\n");

	renderTransparentObjects(rs);

	if (thisConfig.debugSettings->statePrints) printf("Rendered trans gameobjects\n");

	renderReflectiveObjects(rs, nullptr);

	if (thisConfig.debugSettings->statePrints) printf("Rendered refl gameobjects\n");

	renderLights(rs, nullptr);

	if (thisConfig.debugSettings->statePrints) printf("Rendered lights\n");

	delete rs;
}

void LogicalRenderer::renderGUI(RenderSettings *rs)
{
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	
	std::vector<GameObject*> objects = currScene->getGUIObjects();

	if (thisConfig.debugSettings->statePrints) printf("got total gui objects, number %d\n", objects.size());

	for (int count = 0; count < objects.size(); count++)
	{

		if (thisConfig.debugSettings->statePrints) printf("object wasn't null, rendering \n");
		objects[count]->draw(rs);
		if (thisConfig.debugSettings->statePrints) printf("rendered\n");
		
	}
	glDisable(GL_BLEND);
}

void LogicalRenderer::renderGameObjects(RenderSettings*rs)
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_BLEND);

	if (thisConfig.debugSettings->statePrints) printf("set all glu parameters\n");

	//Render all the renderable objects in the active scene
	std::vector<GameObject*> objects = currScene->getObjects();

	if (thisConfig.debugSettings->statePrints) printf("got total game objects, number %d\n", objects.size());

	for (int count = 0; count < objects.size(); count++)
	{
		if (thisConfig.debugSettings->statePrints) printf("retrieved gameobject number %d\n", count);

		objects[count]->draw(rs);
		if (thisConfig.debugSettings->statePrints) printf("rendered\n");
		
	}
}

void LogicalRenderer::renderTransparentObjects(RenderSettings*rs)
{
	//Render all the transparant objects!
	std::vector<GameObject*> objects = currScene->getTransObjects();
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (int count = 0; count < objects.size(); count++)
	{
		objects[count]->draw(rs);
	}

	glDisable(GL_BLEND);
}

void LogicalRenderer::renderReflectiveObjects(RenderSettings*rs, ReflectiveObject* obj)
{
	//Render all the renderable objects in the active scene
	std::vector<ReflectiveObject*> objects = currScene->getReflectiveObjects();

	for (int count = 0; count < objects.size(); count++)
	{
		if (objects[count] != obj)
		{
			objects[count]->draw(rs);
		}
	}
}

void LogicalRenderer::renderLights(RenderSettings*rs, Light *currLight)
{
	LightingState * currLighting = thisConfig.currLighting;

	for (int count = 0; count < currLighting->numberOfLights; count++)
	{
		if (currLighting->lights[count] != nullptr) {
			currLighting->lights[count]->update(deltaTime);
			currLighting->lights[count]->draw(rs);
		}
	}
}

void LogicalRenderer::addPostProcessor(PostProcessingShader * x)
{
	if (!(ppCount < MAX_PP_SHADERS))
	{
		printf("Error: max post processors already in use\n");
		return;
	}

	auxPPFBOs.push_back( new PostProccessingFBO(thisConfig, 0));

	auxPPFBOs[ppCount]->shader = x;

	Game::singleton()->debugCentre->addDebugTexture(auxPPFBOs[ppCount]->getTextureDetailsByIndex(0));

	ppCount++;
}

void LogicalRenderer::removePostProcessor(PostProcessingShader *x)
{
	for (int count = 0; count < ppCount; count++)
	{
		if (auxPPFBOs[count]->shader == x)
		{
			Game::singleton()->debugCentre->removeDebugTexture(auxPPFBOs[count]->getTexture());
			auxPPFBOs[count]->shader->destroy();
			delete auxPPFBOs[count];

			auxPPFBOs.erase(auxPPFBOs.begin() + count);
			ppCount--;
			return;
		}
	}

	std::cout << "Error: post processor not found\n";
}
