#pragma once
#include <vector>
#include <algorithm>

template <class ContentType, class CommonType>
class RenderGroup
{
public:
	int size = 0;

	std::vector<ContentType*> contents;

	CommonType* commonPart = nullptr;
};