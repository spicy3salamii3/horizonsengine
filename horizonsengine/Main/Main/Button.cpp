#include "Button.h"
#include "Game.h"
#include "GrowAnimation.h"
#include "AnimationComponent.h"

Button::Button(Texture* t, glm::vec3 scale,  ButtonAction * buttonA, std::string label): ImageRect(t)
{
	setScale(scale);

	setup();

	dc = Game::singleton()->debugCentre;

	locMousePosX = dc->addWatch(0.0f, "Mouse Pos X");
	locPos = dc->addWatch(glm::vec3(0), "ButtonPos");
	buttonAction = buttonA;

	name = label;
}


void Button::setup()
{
	//Put here as ImageRect's setup can be called in constructor. Rethinking this could be a good idea
	animator = new AnimationComponent(this);

	addComponent(animator);

	zoomAnimation = new GrowAnimation(100.0f, transform->getRelativeScale().x, 0.007f, this);
	revAnimation = new GrowAnimation(100.0f, transform->getRelativeScale().x + 0.007f, 0.007f, this, true);
	locAnimation = animator->addAnimation(zoomAnimation);
	locRevAnimation = animator->addAnimation(revAnimation);

	if (setupAlready) return;

	ImageRect::setup();

	setupAlready = true;
}

Button::~Button()
{
}

void Button::update(double delta)
{
	GUIObject::update(delta);
}


void Button::OnClick(int type,float x, float y)
{
	if (mouseOver&&buttonAction!= nullptr)
	{
		buttonAction->onClick(type, x, y);
	}
}

void Button::mouseMovePos(float x, float y)
{
	dc->updateWatch(locMousePosX, x);
	dc->updateWatch(locPos, transform->getPos());

	if (transform->isMouseOver(x, y) && !mouseOver)
	{
		mouseOver = true;
		animator->setAnimation(locAnimation);
	}
	if (mouseOver && !transform->isMouseOver(x, y))
	{
		mouseOver = false;
		animator->setAnimation(locRevAnimation);
	}
}