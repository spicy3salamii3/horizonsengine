#include "Fade.h"
#include "BrightnessPP.h"
#include "Game.h"

Fade::Fade(int t)
{
	type = t;
}

Fade::~Fade()
{
}

void Fade::update(double x)
{
	if (x > 20) x = 4;
	switch (type)
	{
	case FADE_SPLASH:
		fadeSplash(x);
		break;
	case FADE_FROM_BLACK:
		fadeIn(x);
		break;
	}

}

void Fade::fadeSplash(double x)
{
	if (active)
	{
		if (fadingIn)
		{
			timer += x;
			brightness->setBrightness((timer / maxTime)*maxBright);


			if (timer > maxTime)
			{
				fadingIn = false;
				paused = true;
			}
		}
		else if (paused)
		{
			timer += x;
			if (timer > maxTime*2.0f)
			{
				timer = maxTime;
				paused = false;
			}
		}
		else
		{
			timer -= x;
			brightness->setBrightness((timer / maxTime)*maxBright);


			if (timer < 0.0)
			{
				active = false;
				Game::singleton()->loadNextScene();
				Game::singleton()->removePostProcessor(brightness);
			}

		}
	}
	if (Game::singleton()->getKeys() & Keys::Space)
	{
		paused = false;
		fadingIn = false;
		timer = -1;
	}

}

void Fade::fadeIn(double x)
{
	if (active)
	{
		if (fadingIn)
		{
			timer += x;
			brightness->setBrightness((timer / maxTime)*maxBright);


			if (timer > maxTime)
			{
				active = false;
				brightness->setBrightness(1.0f);
				Game::singleton()->removePostProcessor(brightness);
			}
		}
	}
}

void Fade::startFade(double fadeTime)
{
	brightness = new BrightnessPP(0.0f);

	Game::singleton()->addPostProccessor(brightness);

	maxTime = fadeTime;

	timer = 0.0;

	fadingIn = true;
	active = true;
	paused = false;
}
