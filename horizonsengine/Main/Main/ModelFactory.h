#pragma once
#include "VAOData.h"


class ModelFactory
{
public:
	ModelFactory* singleton();

private:
	static ModelFactory* thisPointer;
};

