#pragma once
#include "DefaultShaders.h"
class TerrainShaders :
	public DefaultShaders
{
public:
	static TerrainShaders* getInstance();

	void setTilingFactor(int x);
protected:
	static TerrainShaders* thisPointer;
	TerrainShaders();
	~TerrainShaders();

	void setTextureUnits();

	int textureCount = 0;
	void setup();

	std::string vertFilename = "Shaders/vertex_shader_terr.shader";
	std::string fragFilename = "Shaders/fragment_shader_terr.shader";

	std::string  textureNames[13] =
	{
		"bases[0]",
		"bases[1]",
		"bases[2]",
		"bases[3]",
		"normals[0]",
		"normals[1]",
		"normals[2]",
		"normals[3]",
		"specs[0]",
		"specs[1]",
		"specs[2]",
		"specs[3]",
		"splat"
	};


	GLuint locTilingFactor;
};

