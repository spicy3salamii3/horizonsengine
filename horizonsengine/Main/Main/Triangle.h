#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>




struct Triangle
{
	Triangle() {}
	Triangle(glm::vec3 n1, glm::vec3 n2, glm::vec3 n3) { p1 = n1; p2 = n2; p3 = n3; }
	void workOutMaxXZ()
	{
		 float minX = 1000.0f;
		 float minZ = 1000.0f;
		 float maxX = -1000.0f;
		 float maxZ = -1000.0f;
		 if (p1.x < minX)minX = p1.x;
		 if (p1.x > maxX)maxX = p1.x;

		 if (p1.z < minZ)minZ = p1.z;
		 if (p1.z > maxZ)maxZ = p1.z;

		 if (p2.x < minX)minX = p2.x;
		 if (p2.x > maxX)maxX = p2.x;

		 if (p2.z < minZ)minZ = p2.z;
		 if (p2.z > maxZ)maxZ = p2.z;

		 if (p3.x < minX)minX = p3.x;
		 if (p3.x > maxX)maxX = p3.x;

		 if (p3.z < minZ)minZ = p3.z;
		 if (p3.z > maxZ)maxZ = p3.z;

		 width = maxX - minX;
		 height = maxZ - minZ;
	}
	glm::vec3 p1;
	glm::vec3 p2;
	glm::vec3 p3;

	glm::vec3 centre;

	float width;
	float height;

	float minX;
	float minZ;
	float maxX;
	float maxZ;

	bool wasTooBig = false;

};