﻿#include "EngineWrapper.h"
#include "shader_setup.h"
#include "Game.h"
#include "DefaultShaders.h"
#include "TangentDrawingShaders.h"
#include <ft2build.h>
#include FT_FREETYPE_H
#include "Config.h"
#include <glad/glad.h>
#include <glfw/glfw3.h>
#include "ShaderManager.h"

using namespace CoreStructures;
using namespace std;

EngineWrapper::EngineWrapper()
{
	printf("Loading Horizons Engine Verision 0.6.5 PreRelease\n\n\n");


	width = 1600;
	height = 900;
}


EngineWrapper::~EngineWrapper()
{
}

void EngineWrapper::initializeGame(Scene * scene, bool useSplash)
{	
	gameInstanceWrapper = Game::singleton();

	gameInstanceWrapper->currentWindow = window;

	gameInstanceWrapper->setup(thisConfig, scene, useSplash);

	DisplayDetails * dd = gameInstanceWrapper->getDisplayDetails();

	dd->width = width;
	dd->height = height;
}


void EngineWrapper::startGame()
{
	gameInstanceWrapper->Start(); 


	while (!glfwWindowShouldClose(window))
	{
		//Clear the display
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		glEnable(GL_MULTISAMPLE);
		glEnable(GL_CLIP_DISTANCE0);
		//update the game	

		gameInstanceWrapper->update();
		gameInstanceWrapper->render();

		//flip the display
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	exit(0);
}

void EngineWrapper::sortConfig()
{
	shaderInit();
	setupFreeType();
}

void EngineWrapper::initGL(int argc, char* argv[], std::string windowName)
{
	//Setup the juicy GLFW stuff

	initGLFW(windowName);

	glEnable(GL_MULTISAMPLE);
	glEnable(GL_CLIP_DISTANCE0);
	glEnable(GL_CULL_FACE);

	//basic enables
	glViewport(0, 0, width, height);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

	// Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, cursor_pos_callback);
	//Print the version out
	const GLubyte * version = glGetString(GL_VERSION);
	std::cout << "Running OpenGL version " << version << "\n\n";

}

void EngineWrapper::initGLFW(std::string windowName)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

	window = glfwCreateWindow(width, height, windowName.c_str(), NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		exit(0);
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		exit(0);
	}

	glfwSetKeyCallback(window, keyEventWrapper); 
	glfwSetMouseButtonCallback(window, mouseDownWrapper);
}

#pragma region GameCalls

void mouseDownWrapper(GLFWwindow* window, int button, int action, int mods)
{
	gameInstanceWrapper->mouseDown(button, action, mods);
}

void mouseMoveWrapper(int x, int y)
{
	gameInstanceWrapper->mouseMove(x, y);
}

void cursor_pos_callback(GLFWwindow* window, double x, double y)
{
	gameInstanceWrapper->mousePassiveMouse(x, y);
}

void keyEventWrapper(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	gameInstanceWrapper->keyEvent(key, scancode, action, mods);

	
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
	
}
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

void specialKey(int Key, int x, int y)
{
}

void specialKeyUp(int Key, int x, int y)
{
}


#pragma endregion

void makeFrameWrapper(void)
{

}

void idle(void)
{
	
}

void EngineWrapper::addScene(Scene *x)
{
	gameInstanceWrapper->addScene(x);
}

void EngineWrapper::shaderInit()
{
	ShaderManager* sh = ShaderManager::singleton();

	DefaultShaders* defTemp = DefaultShaders::getInstance();

	WaterShaders* waterTemp = WaterShaders::getInstance();

	SkyboxShaders * skyTemp = SkyboxShaders::getInstance();

	GUIShader  * guiTemp = GUIShader::getInstance();

	TangentDrawingShaders * tanTemp = TangentDrawingShaders::getInstance();

	NormalDrawingShaders * normTemp = NormalDrawingShaders::getInstance();

	TextShaders* textTemp = TextShaders::getInstance();

	TerrainShaders* terrTemp = TerrainShaders::getInstance();

	PostProcessingShader* postTemp = PostProcessingShader::getInstance();

	InstancedShaders* instTemp = InstancedShaders::getInstance();

	thisConfig.instanceShaders = instTemp;
	sh->addShader(instTemp);

	thisConfig.postShaders = postTemp;
	sh->addShader(postTemp);

	thisConfig.terrShaders = terrTemp;
	sh->addShader(terrTemp);

	thisConfig.textShaders = textTemp;
	sh->addShader(textTemp);

	thisConfig.tanShaders = tanTemp;
	sh->addShader(tanTemp);

	thisConfig.guiShaders = guiTemp;
	sh->addShader(guiTemp);

	thisConfig.waterShaders = waterTemp;
	sh->addShader(waterTemp);

	thisConfig.mainShaders =defTemp;
	sh->addShader(defTemp);

	thisConfig.skyboxShaders = skyTemp;
	sh->addShader(skyTemp);

	thisConfig.normalShaders = normTemp;
	sh->addShader(normTemp);

	thisConfig.shaderManager = sh;
}

void EngineWrapper::setupFreeType()
{
	//Setup the arial font

	printf("Setting up arial....");

	FT_Face face;
	FT_Library ft;

	if (FT_Init_FreeType(&ft))
		std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;

	if (FT_New_Face(ft, "Fonts/arial.ttf", 0, &face))
		std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;

	FT_Set_Pixel_Sizes(face, 0, 48);


	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

	for (GLubyte c = 0; c < 128; c++)
	{
		// Load character glyph 
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		Character character = {
			texture,
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			face->glyph->advance.x
		};
		thisConfig.arialCharacters.insert(std::pair<GLchar, Character>(c, character));
	}

	FT_Done_Face(face);


	FT_Face face2;

	if (FT_New_Face(ft, "Fonts/regular.otf", 0, &face2))
		std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;

	FT_Set_Pixel_Sizes(face2, 0, 70);


	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

	for (GLubyte c = 0; c < 128; c++)
	{
		// Load character glyph 
		if (FT_Load_Char(face2, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face2->glyph->bitmap.width,
			face2->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face2->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		Character character = {
			texture,
			glm::ivec2(face2->glyph->bitmap.width, face2->glyph->bitmap.rows),
			glm::ivec2(face2->glyph->bitmap_left, face2->glyph->bitmap_top),
			face2->glyph->advance.x
		};
		thisConfig.codeFontCharacters.insert(std::pair<GLchar, Character>(c, character));
	}



	FT_Done_Face(face2);
	FT_Done_FreeType(ft);

	//glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
}