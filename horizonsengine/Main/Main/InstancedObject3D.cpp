#include "InstancedObject3D.h"



InstancedObject3D::InstancedObject3D(InstancedVAOData* d)
{
	data = d;
	indicesLength = data->getElementsCountByIndex(0);
}


InstancedObject3D::~InstancedObject3D()
{
}


void InstancedObject3D::update(double x)
{
	defaultShaders = thisConfig.instanceShaders;

	updateBehaviours(x);

}

void InstancedObject3D::setup()
{
	if (setupAlready) return;
	Object3D::setup();


	setupAlready = true;
}

void InstancedObject3D::draw(RenderSettings *rs)
{
	glUseProgram(defaultShaders->getProgramID());

	defaultShaders->setClipPlane(rs->clipPlane, rs->useClipPlane);

	defaultShaders->setCameraMat(rs->cameraMat);

	//Set all of the lighting uniforms
	defaultShaders->setLighting(shinyness, thisConfig.currLighting);

	//Tell the shader if a normal map is being used or not
	defaultShaders->setNormalBool((data->getNormalMapByIndex(0) != 0));

	//Bind the model's texture to the object we're drawing
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, data->getTextureByIndex(0));

	//and the specular map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, data->getSpecMapByIndex(0));


	//If the object uses surface normals....
	if (data->getNormalMapByIndex(0) != 0)
	{
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, data->getNormalMapByIndex(0));
	}

	GLuint VAO = data->getVAOByIndex(0);
	//bind the VAO
	glBindVertexArray(VAO);

	// Draw the triangles !
	glDrawElementsInstanced(GL_TRIANGLES, indicesLength, GL_UNSIGNED_INT, 0, data->getInstances());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (rs->renderTangents || thisConfig.debugSettings->drawTangents)
	{
		drawTangents(rs->cameraMat);
	}

	if (rs->renderNormals || thisConfig.debugSettings->drawNormals)
	{
		drawNormals(rs->cameraMat);
	}
}
