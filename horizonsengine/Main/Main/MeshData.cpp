#include "VAOData.h"


MeshData::MeshData(GLuint VAO, GLuint newTexture, GLuint newSpecTexture, GLuint newNormalMap, float *newPoints, float *newNormals, float* newTangents, float *newBitangents, float *newTextureCoords,GLuint * newIndices, int vertsCount, int indicesC)
{
	defaultVAO = VAO;
	texture = newTexture;
	specTexture = newSpecTexture;
	normalMap = newNormalMap;

	elementsCount = vertsCount;
	uniqueVerticiesCount = indicesC;

	points = new float[uniqueVerticiesCount * 3];
	normals = new float[uniqueVerticiesCount * 3];
	tangents = new float[uniqueVerticiesCount * 4];
	bitangents = new float[uniqueVerticiesCount * 4];
	textureCoords = new float[uniqueVerticiesCount * 2];
	indices = new GLuint[elementsCount];

	//Fill the dynamically allocated arrays with the numbers from the buffers MeshData keeps the raw data so that more efficient VAOs can be constructed when needed
	for (int i = 0; i < uniqueVerticiesCount * 3; i++)
	{
		points[i] = newPoints[i];
		normals[i] = newNormals[i];
	}

	//and the text coords
	for (int i = 0; i < uniqueVerticiesCount * 2; i++)
	{
		textureCoords[i] = newTextureCoords[i];
	}

	//once for each vertex
	for (int i = 0; i < uniqueVerticiesCount * 4; i++)
	{
		bitangents[i] = newBitangents[i];
		tangents[i] = newTangents[i];

	}
	//Move the indices across
	for (int i = 0; i < elementsCount; i++)
	{
		indices[i] = newIndices[i];
	}

}

MeshData::MeshData(GLuint VAO,  float* newPoints, float* newNormals, float* newTangents, float* newBitangents, float* newTextureCoords, GLuint* newIndices, int vertsCount, int indicesC)
{
	defaultVAO = VAO;

	elementsCount = vertsCount;
	uniqueVerticiesCount = indicesC;

	points = new float[uniqueVerticiesCount * 3];
	normals = new float[uniqueVerticiesCount * 3];
	tangents = new float[uniqueVerticiesCount * 4];
	bitangents = new float[uniqueVerticiesCount * 4];
	textureCoords = new float[uniqueVerticiesCount * 2];
	indices = new GLuint[elementsCount];

	//Fill the dynamically allocated arrays with the numbers from the buffers MeshData keeps the raw data so that more efficient VAOs can be constructed when needed
	for (int i = 0; i < uniqueVerticiesCount * 3; i++)
	{
		points[i] = newPoints[i];
		normals[i] = newNormals[i];
	}

	//and the text coords
	for (int i = 0; i < uniqueVerticiesCount * 2; i++)
	{
		textureCoords[i] = newTextureCoords[i];
	}

	//once for each vertex
	for (int i = 0; i < uniqueVerticiesCount * 4; i++)
	{
		bitangents[i] = newBitangents[i];
		tangents[i] = newTangents[i];

	}
	//Move the indices across
	for (int i = 0; i < elementsCount; i++)
	{
		indices[i] = newIndices[i];
	}

}

glm::vec3 MeshData::getVertexByIndex(int i)
{
	int index = indices[i];

	return glm::vec3(points[index * 3], points[(index * 3) + 1], points[(index * 3) + 2]);
}