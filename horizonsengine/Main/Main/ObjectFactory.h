#pragma once
#include "GameObject.h"
#include "VAOData.h"
#include "TerrainTextureData.h"
#include "Terrain.h"
#include "Triangle.h"
#include "Bounds.h"
#include "Extents.h"
#include <bullet/btBulletCollisionCommon.h>
#include "tiny_obj_loader.h"

class ObjectFactory
{
public:
	static ObjectFactory * getInstance();

	virtual Terrain * makeTerrain(Model * v, TerrainTextureData * t, float tile, float scale);
	virtual GameObject * makeInstancedObject(InstancedVAOData *);
	virtual GameObject * makeCustomObject(Model*, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot);
	virtual GameObject * makeCustomObject(Model*, float scale = 1.0f);

	virtual void addToTerrain(Model*);
	virtual btCollisionShape* createTerrainCollider();
	virtual btCollisionShape * makeBoxCollider(GameObject*);
	virtual btCollisionShape * makeBoxCollider(Extents volumeExtents);
	virtual btCollisionShape * makeCylinderCollider(GameObject*);
	virtual btCollisionShape * makeCylinderCollider(Extents volumeExtents);
	virtual btCollisionShape * makeComplexCollider(Model* data);
	virtual btCollisionShape * makeComplexCollider(MeshData* data);

	virtual bool handleCustomPrefix(tinyobj::shape_t shape) { return false; }

protected:

	ObjectFactory();
	~ObjectFactory();

	static ObjectFactory* thisPointer;

	
	btTriangleMesh* triangles = nullptr;



};

