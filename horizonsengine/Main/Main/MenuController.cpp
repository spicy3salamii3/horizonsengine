#include "MenuController.h"



MenuController::MenuController(Game* newRefr)
{
	refr = newRefr;
}


MenuController::~MenuController()
{
}

void MenuController::update(double dt)
{
	if (!choiceMade)
	{
		Keystate keys = refr->getKeys();

		if (keys & Keys::num1)
		{	
			printf("Loading scene...\n");
			refr->loadScene("Water");
			choiceMade = true;
		}
		if (keys & Keys::num2)
		{
			printf("Loading scene...\n");
			refr->loadScene("Night");
			choiceMade = true;
		}
		if (keys & Keys::num3)
		{
			printf("Loading scene...\n");
			refr->loadScene("Terrain");
			choiceMade = true;
		}
		if (keys & Keys::num4)
		{
			printf("Loading scene...\n");
			refr->loadScene("Physics");
			choiceMade = true;
		}
	}

}