#pragma once
#include "Object2D.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Config.h"
class Text :
	public GUIObject
{
public:
	Text(std::map<GLchar, Character> font);
	~Text();
	void update(double x);

	void draw(RenderSettings*);

	void sortVAO();

	void setText(std::string x) { text = x; name = x; }

	void setFont(std::map<GLchar, Character> f) { font = f; }

	void setColour(glm::vec3 c) { colour = c; }

	bool centred = false;

private:

	std::string text = "Hello World";

	GLuint VBO = 0;
	GLuint VAO = 0;

	glm::vec3 colour = glm::vec3(1.0f, 1.0f, 1.0f);

	float yOffset = 20.0f;

	std::map<GLchar, Character> font; 
};

