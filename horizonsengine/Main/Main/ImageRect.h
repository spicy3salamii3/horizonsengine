#pragma once
#include "Config.h"
#include "Object2D.h"
#include "Texture.h"
#include "Material.h"
#include "MaterialHandler.h"


class ImageRect : public virtual GUIObject
{
public:
	ImageRect(Texture*);
	ImageRect(glm::vec2);
	ImageRect();
	~ImageRect();

	void sortVAO();
	void draw(RenderSettings*);
	void setTexture(GLuint x) 
	{ 
		texture = x; 
		getComponent<RenderConditions*>()->setMaterial(
			MaterialHandler::singleton()->makeSingleMaterial((new Texture(x, glm::vec2(0), "")))
		); 
	}

	void applyScaling();

	GLuint getTextureID() { return texture; }



protected:
	const int totalPoints = 12;

	void setup();

	float points[12] =
	{
		-1.0f,1.0f,
		-1.0f,-1.0f,
		1.0f,1.0f,
		1.0f,1.0f, 
		-1.0f, -1.0f,
		1.0f, -1.0f
	};	

	float texts[12] =
	{
		0.0f,1.0f,
		0.0f,0.0f,
		1.0f,1.0f,
		1.0f,1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f
	};

	GLuint indicies[6] =
	{
		0,1,2,3,4,5
	};



	GLuint VAO = 0;

	GLuint pointsVBO = 0;
	GLuint textsVBO = 0;
};

