#pragma once
#include <glad\glad.h>
#include <glm-0.9.9.2\glm\glm.hpp>
#include <string>

using namespace std;

class Texture
{
public:
	Texture() {}
	Texture(GLuint i, glm::vec2 r, string f)
	{
		id = i;
		filename = f;
		res = r;
	}
	~Texture()
	{

	}
	GLuint id = 0;

	glm::vec2 res = glm::vec2(0);

	string filename = "";
};