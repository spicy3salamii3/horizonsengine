#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "FBO.h"
#include "Config.h"
#include "Camera.h"
class ReflectiveObject;

class WaterFBO : public FBOGroup
{
public:
	WaterFBO(ReflectiveObject * newHost);
	~WaterFBO();

	void bindReflection() {
		
		bindFrameBuffer(reflectionFBO, refWidth, refHeight);
	}
	void bindRefration() {
		
		bindFrameBuffer(refractionFBO, refraWidth, refraHeight);
		
	}

	void unBind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, initialWidth, initialHeight);
	}

	GLuint getReflectionTexture() { return reflectionTexture; }
	GLuint getRefractionTexture() { return refractionTexture; }
	GLuint getDepthBuffer() { return refractionDepth; }

	ReflectiveObject * getHost() { return host; }

	void update();

private:
	Config thisConfig;
	int createTextureAttachment(int, int);
	int createDepthTextureAttachment(int, int);
	int createDepthBufferAttachment(int, int);

	void createBuffers();

	ReflectiveObject * host;

	float heightFudge = 0.1f;


	int refWidth = 600;
	int refHeight = 600;

	int refraWidth = 850;
	int refraHeight = 480;

	int initialWidth ;
	int initialHeight;

	GLuint reflectionFBO;
	GLuint refractionFBO;

	GLuint reflectionTexture;
	GLuint refractionTexture;

	GLuint reflectionDepth;
	GLuint refractionDepth;
};

