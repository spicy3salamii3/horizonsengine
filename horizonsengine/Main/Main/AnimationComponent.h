#pragma once
#include "Component.h"
#include "Animation2D.h"
#include <vector>
#include "PrivateBehaviour.h"


class AnimationComponent : public Component
{
public:
	AnimationComponent(GameObject*x);
	~AnimationComponent() {}

	void stepAnimation(double delta);

	//add overload for 3d animations
	int addAnimation(Animation2D*);

	void setAnimation(int);

private:
	std::vector<Animation*> animations;
	Animation * currentAnimation = nullptr;

	bool loop  =false ;
};



class AnimationBehaviour : public PrivateBehaviour
{
public:
	AnimationBehaviour(AnimationComponent* an) { aniComp = an; }
	~AnimationBehaviour() {}

	void update(double delta)
	{
		aniComp->stepAnimation(delta);
	}
private:
	AnimationComponent * aniComp;
};
