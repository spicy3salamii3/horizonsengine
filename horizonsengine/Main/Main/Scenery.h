#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Object3D.h"


class Custom :
	public Object3D
{
public:
	Custom(Model* shape, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot);

	
	~Custom();

	void update(double deltaTime)
	{
		Object3D::update(deltaTime);

		Object3D::lateUpdate(deltaTime);
	}

	void setup();

protected:
	Model* tempModelStorage = nullptr;
};

