#include "Skybox.h"
#include "texture_loader.h"
#include <CoreStructures\CoreStructures.h>
#include <stdio.h>
#include "ShaderManager.h"
#include "Material.h"

Skybox::Skybox(int boxNo)
{
	//applyScaling();
	loadTextures(boxNo);
	sortVAO();
	setup();
}


Skybox::~Skybox()
{
}

void Skybox::update(double deltaTime)
{
	Object3D::update(deltaTime);

	Object3D::lateUpdate(deltaTime);

}

void Skybox::setup()
{
	RenderConditions* rc = getComponent<RenderConditions*>();

	rc->shaderInUse = ShaderManager::singleton()->getShader<SkyboxShaders*>(); 

	Texture* texture = new Texture(textureID, glm::vec2(0), "Skybox Texture");

	rc->setMaterial(new SkyboxMaterial(texture));

	Model* skyboxModel = new Model();

	MeshData* mesh = new MeshData(thisVAO, 36);

	skyboxModel->addMesh(mesh);

	rc->model = skyboxModel;
}

void Skybox::applyScaling()
{
	for (int i = 0; i< 36*3;i++)
	{
		skyboxVertices[i] *= scale;
	}
}

void Skybox::draw(RenderSettings*rs)
{
	//glDepthMask(GL_FALSE);
	glUseProgram(thisConfig.skyboxShaders->getProgramID());
	// ... set view and projection matrix

	thisConfig.skyboxShaders->setMats(T*R*S, rs->cameraMat, thisConfig.currLighting->persp);

	thisConfig.skyboxShaders->setClipPlane(rs->clipPlane, rs->useClipPlane);

	thisConfig.skyboxShaders->setScale(scale);


	glBindVertexArray(thisVAO);

	glActiveTexture(GL_TEXTURE0);

	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindVertexArray(0);
	//glDepthMask(GL_TRUE);
}

void Skybox::sortVAO()
{
	glGenVertexArrays(1, &thisVAO);
	glBindVertexArray(thisVAO);
	
	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*3*36, skyboxVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	GLuint* indicies = new GLuint[36];

	for (int i = 0; i < 36; i++)
	{

		indicies[i] = i;
	}

	//And the indicies buffer
	GLuint indiciesVBO = 0;
	glGenBuffers(1, &indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(GLuint), indicies, GL_STATIC_DRAW);

	glBindVertexArray(0);
}

void Skybox::loadTextures(int boxNo)
{
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);


	if (boxNo == 0)
	{
		for (unsigned int i = 0; i < facesPaths.size(); i++)
		{
			fiLoadTextureSkybox(facesPaths[i].c_str(), GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
		}
	}
	else if (boxNo == 1)
	{
		for (unsigned int i = 0; i < facesPaths2.size(); i++)
		{
			fiLoadTextureSkybox(facesPaths2[i].c_str(), GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
		}
	}


	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

}
