#pragma once
#include "Shader.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
class LightingState;

class WaterShaders : public Shader
{
public:
	~WaterShaders();

	static WaterShaders* getInstance();


	void setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S, glm::mat4 C);

	void setLightUniforms(LightingState*);
	void setTextures();
	void setOffset(float);
	void setTiling(glm::vec2);

	void prepareToRender(GameObject*);

	void prepareToRender(RenderSettings*);

protected:
	WaterShaders();

	void setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S);
	void setMats(glm::mat4 C);

	static WaterShaders* thisPointer;
	void setup();


	std::string vertexFileName = "Shaders\\waterVertex.shader";
	std::string fragmentFileName = "Shaders\\waterFrag.shader";

	GLuint locTw;
	GLuint locRw;
	GLuint locSw;
	GLuint locCw;

	GLuint locLightCountW;

	//water shader unique features
	GLuint locWaterDudu;
	GLuint locWaterNormal;
	GLuint locRefr;
	GLuint locRefra;
	GLuint locDepth;
	GLuint locTiling;

	GLuint waterOffset;
	GLuint locCameraPosW;



	GLuint lightingUniforms[MAX_LIGHTS * PARAMS_PER_LIGHT];
};

