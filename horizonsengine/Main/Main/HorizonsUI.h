#pragma once

#include "Game.h"

#include "Object2D.h"
#include "Button.h"
#include "ImageRect.h"

#include "InputListener.h"

#include "GUIShader.h"
#include "WorldGUIShader.h"

class SceneOnButtonAction : public ButtonAction
{
public:
	SceneOnButtonAction(Scene* theScene)
	{
		scene = theScene;
	}

	SceneOnButtonAction(std::string theSceneName)
	{
		sceneName = theSceneName;
	}

	void onClick(int type, float, float)
	{
		if (type != INPUT_LEFT_MOUSE_DOWN) return;

		if (scene != nullptr)
			Game::singleton()->loadScene(scene);
		else
			Game::singleton()->loadScene(sceneName);
	}
private:
	Scene* scene = nullptr;
	std::string sceneName = "";
};