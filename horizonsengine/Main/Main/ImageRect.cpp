#include "ImageRect.h"
#include "Game.h"
#include "Material.h"
#include "MaterialHandler.h"


ImageRect::ImageRect(Texture* text)
{
	setup();
	getComponent<RenderConditions*>()->setMaterial(MaterialHandler::singleton()->makeSingleMaterial(text));
	transform->res = text->res;
	texture = text->id;

	applyScaling();
}

ImageRect::ImageRect(glm::vec2 r)
{
	setup();
	transform->res = r;
	transform->setScale(glm::vec2(0.5f));

	applyScaling();
}

ImageRect::ImageRect()
{
	setup();
	transform->res = glm::vec2(250);
	GUIShader::getInstance()->setScreenPos(
		glm::translate(glm::mat4(1),
			glm::vec3((-transform->res/2.0f),0.0f)));
}

void ImageRect::setup()
{
	initTransform();

	sortVAO();

	RenderConditions* rc = getComponent<RenderConditions*>();

	Model* model = new Model();

	model->addMesh(new MeshData(VAO, 6));

	rc->model = model;
}

ImageRect::~ImageRect()
{
}

void ImageRect::applyScaling()
{

	for (int i = 0; i< totalPoints/2; i++)
	{
		points[i * 2] = points[i * 2] * (transform->res.x);
		points[(i * 2) + 1] =points[(i * 2) + 1]* (transform->res.y);
	}

	sortVAO();
}


void ImageRect::sortVAO()
{
	//Setup the VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	//Add the points to the VBO
	glGenBuffers(1, &pointsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, pointsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, texts, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	GLuint indiciesVBO = 0;

	glGenBuffers(1, &indiciesVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indiciesVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, totalPoints/2 * sizeof(GLuint), indicies, GL_STATIC_DRAW);

	glBindVertexArray(0);
}

void ImageRect::draw(RenderSettings*)
{
	//glFrontFace(GL_CW);

	glUseProgram(thisConfig.guiShaders->getProgramID());

	glm::mat4 p = transform->getPosMat();

	thisConfig.guiShaders->setScreenPos(p);
	thisConfig.guiShaders->setScaleMat(transform->getScaleMat());
	thisConfig.guiShaders->setProj(transform->getProj());
	

	glBindVertexArray(VAO);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	//Depeceated, now using tris
	glDrawElements(GL_TRIANGLES,6 , GL_UNSIGNED_INT, ( void*)0);

	//glFrontFace(GL_CCW);
}
