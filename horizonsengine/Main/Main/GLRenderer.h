#pragma once
#include "SceneRenderer.h"
#include "Material.h"
#include "RenderGroupClasses.h"

class Shader;

typedef RenderGroup<GameObject, Model> ModelData;

typedef RenderGroup<ModelData, Material> MaterialData;

typedef RenderGroup<MaterialData,Shader> ShaderData;

typedef RenderGroup<ShaderData, FBOGroup> PassData;

typedef RenderGroup<PassData,Scene> RenderData;



class GLRenderer : public SceneRenderer
{
public:

	static GLRenderer* singleton();

	void update(double);

	void render();

	void initialize();

	void addPostProcessor(PostProcessingShader*);

	void removePostProcessor(PostProcessingShader*);

	void onAddGameObject(GameObject* go);

	void setScene(Scene* scene);

	void onAddFBO(FBOGroup*, Scene * scene);

private:

	static GLRenderer* thisPointer;

	Shader* currentShader = nullptr;

	RenderSettings* rs = nullptr;

	void renderToFrameBuffer(PassData*, int i);

	void renderShaderData(ShaderData*);

	void renderMaterialData(MaterialData*);

	void renderModelData(ModelData*);

	//Render passes requested by the scene, containing references to all objects involved.
	RenderData renderPasses;

	//A group of postprocessing FBOs and screen buffers that are the same for each pass, regardless of scene
	FBOGroup* cyclePasses;

	GLRenderer();

	~GLRenderer();

	void addToPassData(PassData*, GameObject*, RenderConditions * );

	void addToShaderData(ShaderData*, GameObject*, RenderConditions*);

	void addToMaterialData(MaterialData*, GameObject*, RenderConditions*);

	void addToModelData(ModelData*, GameObject*);
	//Temp
	ScreenBuffer* screenBuffer = nullptr;

	void sortPasses();
};