#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "ShadowBox.h"

class ShadowMap
{
public:
	ShadowMap();
	~ShadowMap();

	GLuint getMap();

	void update(ShadowBox *);

};

