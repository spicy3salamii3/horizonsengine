#include "MaterialHandler.h"
#include <iostream>


MaterialHandler* MaterialHandler::thisPointer = nullptr;

Material* MaterialHandler::makeMaterial(Texture* diffuse, Texture* specular, Texture* normal)
{
	//Check to see if material already exists
	for each (Material * mat in materials)
	{
		if (mat->m_diff == diffuse)
			if (mat->m_norm == normal)
				if (mat->m_spec == specular)
				{
					std::cout << "Material exists \n";
					return mat;
				}
	}

	Material* mat = new Material(diffuse, specular, normal);
	materials.push_back(mat);
	return mat;
}

SingleMaterial* MaterialHandler::makeSingleMaterial(Texture* t)
{
	for each (SingleMaterial * sm in singleMaterials)
	{
		if (sm->getTexture() == t)
		{
			std::cout << "Material Exists\n";
			return sm;
		}
	}
	SingleMaterial* mat = new SingleMaterial(t);
	singleMaterials.push_back(mat);
	return mat;
}

SkyboxMaterial* MaterialHandler::makeSkyboxMaterial(std::string directory)
{
	return new SkyboxMaterial(new Texture(0, glm::vec2(0), ""));
}


MaterialHandler* MaterialHandler::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new MaterialHandler();
	}
	return thisPointer;
}