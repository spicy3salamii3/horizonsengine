#include "SplashScreen.h"
#include "GLRenderer.h"

#include "Game.h"

SplashScreen::SplashScreen(ImageRect * ig)
{
	img = ig;
}


SplashScreen::~SplashScreen()
{
}


void SplashScreen::load(Game * ref)
{
	gameRefr = ref;

	//gameRefr->setRenderEngine(GLRenderer::singleton());

	loadInit();

	Camera* camera = new Camera();

	gameRefr->setCamera(camera);

	addGameObject(img);


	fader = new Fade(FADE_SPLASH);
	addPublicBehaviour(fader);
}

void SplashScreen::run()
{
	gameRefr->setCaptureMouse(true);

	fader->startFade(2300.0f);
}