#include "PostProccessingFBO.h"



PostProccessingFBO::PostProccessingFBO(Config c, float a) : thisConfig(c), aaAmount(a)
{
	bufferCount = 1;

	setupFBO();

	FBOs[0] = renderFBO;
	

	dimensions[0] = thisConfig.displayDetails->width;
	dimensions[1] = thisConfig.displayDetails->height;

	usePlanes[0] = false;
	usePlanes[1] = false;
}

PostProccessingFBO::~PostProccessingFBO()
{

}

void PostProccessingFBO::bind()
{
	update();
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, renderFBO);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
}

void PostProccessingFBO::update()
{
	dimensions[0] = thisConfig.displayDetails->width;
	dimensions[1] = thisConfig.displayDetails->height;
}

void PostProccessingFBO::unBind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PostProccessingFBO::blitTo(PostProccessingFBO* x)
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, x->getRenderFBO());
	glBindFramebuffer(GL_READ_FRAMEBUFFER, renderFBO);
	glBlitFramebuffer(0, 0, dimensions[0], dimensions[1], 0, 0, dimensions[0], dimensions[1], GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);

	unBind();
}

void PostProccessingFBO::blitToScreen()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, renderFBO);
	glBlitFramebuffer(0, 0, dimensions[0], dimensions[1], 0, 0, dimensions[0], dimensions[1], GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);

	unBind();
}

void PostProccessingFBO::setupFBO()
{
	glGenFramebuffers(1, &renderFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, renderFBO);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

//set up colour texture


	if (aaAmount > 0)
	{
		glGenTextures(1, &colourBuffer);
		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, colourBuffer);
		glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, aaAmount, GL_RGB, thisConfig.displayDetails->width, thisConfig.displayDetails->height, GL_TRUE);
		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, colourBuffer, 0);

		glGenRenderbuffers(1, &rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, rbo);
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, aaAmount, GL_DEPTH24_STENCIL8, thisConfig.displayDetails->width, thisConfig.displayDetails->height );
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		//Colour texture
		glGenTextures(1, &texture);

		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, thisConfig.displayDetails->width, thisConfig.displayDetails->height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

		//Depth buffer
		glGenRenderbuffers(1, &depthBuffer);

		glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, thisConfig.displayDetails->width, thisConfig.displayDetails->height);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			std::cout << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!" << std::endl;
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}


	unBind();
}