#include "GenerateCube.h"
#include "GameObject.h"
#include "Scene.h"
#include "ForceOnKey.h"
#include "PhysicsComponent.h"
#include "ObjectFactory.h"
#include "Game.h"


GenerateCube::GenerateCube(Model * v, GameObject * p, Scene * s)
{
	scene = s;
	player = p;
	shape = v;
	debug = DebugCentre::singleton();

	noCubesWatchLoc = debug->addWatch(0, "Number Of Cubes ");
	
}


GenerateCube::~GenerateCube()
{
}


void GenerateCube::update(double)
{

}

void GenerateCube::OnClick(int key, float, float)
{
	if (key != INPUT_LEFT_MOUSE_DOWN && key!= INPUT_RIGHT_MOUSE_DOWN) return;

	glm::vec4 pos = player->getRotMat()* glm::vec4(0, 0, 20, 0);

	glm::vec3 pos3 = glm::vec3(pos.x, pos.y, pos.z);

	if (key == INPUT_LEFT_MOUSE_DOWN) {
		cube = new Custom(
			shape,
			player->getPos() + pos3,
			glm::vec3(0.2f),
			glm::vec3(0));
	}
	else
	{
		cube = new Custom(
			shape,
			player->getPos() + pos3/20.0f,
			glm::vec3(0.05f),
			glm::vec3(0));
	}
	cube->e = shape->getExtents(0);

	cube->addPrivateBehaviour(new ForceOnKey(Keys::x, glm::vec3(0, 800, 0)));

	cube->setVolume(ObjectFactory::getInstance()->makeBoxCollider(cube));

	cube->addComponent(new PhysicsComponent(cube, 50.0f));

	scene->addGameObject(cube);

	debug->addObject(cube, "Cube" + std::to_string(cubeCount));

	if (key == INPUT_RIGHT_MOUSE_DOWN)
	{
		cube->getComponent<PhysicsComponent*>()->applyImpulse(300.0f*pos3);
	}
	cubeCount++;

	debug->updateWatch(noCubesWatchLoc, cubeCount);
}
