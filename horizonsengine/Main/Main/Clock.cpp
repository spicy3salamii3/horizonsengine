#include "Clock.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>


void Clock::tick()
{
	ticks = glfwGetTime()*1000.0f;
	delta = ticks - lastTime;

	delay();
	lastTime = ticks;

	frames++;

	ticksCounter += delta;

	frameCounter++;

	avgFPS = frames / (ticks/1000.0f);

	if (ticksCounter > 10000.0f)
	{
		avgFPS10 = frameCounter / 10.0f;
		ticksCounter = 0.0f;
		frameCounter = 0.0f;
	}

	//every 60 frames, set the avgDelta
	frameTicker++;
	avgDeltaCounter += delta;

	if (frameTicker == 60)
	{
		frameTicker = 0;
		avgDelta = avgDeltaCounter / 60;
		avgDeltaCounter = 0;
	}

	if (delta > 40.0f)delta = 40.0f;
	if (avgDelta > 40.0f)avgDelta = 40.0f;
}

void Clock::end()
{
	std::cout << "Average FPS: " << avgFPS<<"\n" ;
	std::cout << "Average FPS per 10 second interval: " << avgFPS10<<"\n" ;
}

void Clock::delay()
{
	if (delta < target)
	{
		Sleep(target - delta);
	}
}
