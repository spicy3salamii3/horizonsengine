#include "Game.h"
#include <FreeImage\FreeImagePlus.h>
#include <iostream>
#include "Game.h"
#include "Scene.h"
#include "Scenery.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>
#include "GURiffModel.h"
#include <mmreg.h>
#include "RenderSettings.h"
#include "PostProcessRect.h"
#include "BulletPhysicsHandler.h"
#include "HideMouse.h"
#include "SplashScreen.h"
#include "ShaderManager.h"


using namespace std;

Game * Game::thisPointer = nullptr;

Game::Game()
{

}

void Game::setup(Config newConfig, Scene * firstScene, bool useSplash)
{
	thisConfig = newConfig;

	gameClock = new Clock();
	gameClock->tick();

	//Save the scene that has been passed in by the engine wrapper

	if (useSplash)
	{

		TextureExaminer::singleton()->setDirectory("Textures//");
		Texture* tex = TextureExaminer::singleton()->getTexture("HorizonsSplash.png");
		ImageRect * img = new ImageRect(tex);
		img->transform->setScale(glm::vec3(0.08f));
		img->transform->setPos(glm::vec3(0, -20, 0));
		img->transform->anchorType = GUI_TRANSFORM_ANCHOR_CEN;
		scenes.push_back(new SplashScreen(img));
		scenes.push_back(firstScene);
	}
	else
	{
		scenes.push_back(firstScene);
	}

	thisConfig.displayDetails = new DisplayDetails();

	//Set the anti-aliasing
	thisConfig.displayDetails->aaAmount = 4;



	physicsEngine = BulletPhysicsHandler::getInstance();

	renderEngine = new LogicalRenderer();

	debugCentre = DebugCentre::singleton();

	addGlobalBehaviour(new HideMouse());
}

Game* Game::singleton()
{

	if (thisPointer == nullptr)
	{
		thisPointer = new Game();
	}
	
	return thisPointer;
}

Game::~Game()
{
	//Start cleanup

	delete gameClock;

	for (int count = 0; count < globalBehaviours.size(); count++)
	{
		delete globalBehaviours[count];
	}
	delete renderEngine;

	delete physicsEngine;

	//delete[] scenes;

	exit(0);
}

void Game::Start()
{
	renderEngine->setConfig(thisConfig);
	renderEngine->initialize();

	renderEngine->setScene(scenes[currScene]);
	renderEngine->setConfig(thisConfig);

	currScene = 0;

	scenes[0]->load(this);

	setConfigAll();

	scenes[0]->run();

	if (gameCamera != nullptr)
	{
		gameCamera->update(deltaTime, 0, 0, false);
	}
}

void Game::render(void)
{

	thisConfig.shaderManager->beginFrame();
	renderEngine->update(deltaTime);


	renderEngine->render();

	debugCentre->draw();
}

void Game::setRenderEngine(SceneRenderer* render) 
{ 
	delete renderEngine; 
	renderEngine = render;
	render->setConfig(thisConfig);
	render->initialize();
	render->setScene(scenes[currScene]); 
	render->setCamera(gameCamera);
}

#pragma region Update Calls and Methods

void Game::update(void)
{

	thisConfig.displayDetails->update();


	if (thisConfig.debugSettings->statePrints) printf("Starting Update\n");
	
	//Tick the clock to count time etc
	gameClock->tick();

	if (thisConfig.debugSettings->statePrints) printf("updated clock\n");

	//Fill in the local value for the deltaTime
	deltaTime = gameClock->getDelta();

	if (thisConfig.debugSettings->statePrints) printf("got delta\n");

	updateBehaviours();

	if (thisConfig.debugSettings->statePrints) printf("updatedBehaviours\n");

	//Update all the gameobjects
	gameObjectUpdate();

	//if (thisConfig.debugSettings->statePrints) printf("gameobjects updated\n");

	////Get all the camera variables sorted
	//cameraUpdate();

	if (thisConfig.debugSettings->statePrints) printf("cameraupdated\n");

	//Update all the transparent objects
	transparentUpdate();

	if (thisConfig.debugSettings->statePrints) printf("updated transparant objects\n");

	//Reflective Update
	reflectiveUpdate();

	if (thisConfig.debugSettings->statePrints) printf("updated reflective obhects\n");

	//update the lighting
	lightingUpdate();

	if (thisConfig.debugSettings->statePrints) printf("updated lighting\n");

	//update the gui
	guiUpdate();

	if (thisConfig.debugSettings->statePrints) printf("updated gui\n");

	if (true) { physicsEngine->doPhysics(gameClock->getDelta(), scenes[currScene]); }
	//physicsFrame = !physicsFrame;

	//Get all the camera variables sorted
	cameraUpdate();


	//Reflective Update later so that the camera matrix is correct
	reflectiveUpdate();

	if (thisConfig.debugSettings->statePrints) printf("updated reflective obhects\n");



	if (thisConfig.debugSettings->statePrints) printf("cameraupdated\n");


	if (thisConfig.debugSettings->statePrints) printf("done late physics\n");


	if (thisConfig.debugSettings->statePrints) printf("glut redisplay posted\n");

	diffX = 0.0f;
	diffY = 0.0f;

	if (thisConfig.debugSettings->statePrints) printf("Finished update\n\n");

	scenes[currScene]->endOfFrame();

	if (thisConfig.debugSettings->statePrints) printf("Deleted any pending deletes`\n\n");


	debugCentre->update(deltaTime);

	renderEngine->update(deltaTime);
}

void Game::cameraUpdate()
{
	if (gameCamera != nullptr)
	{	//Update the camera with the mouse position, play bool
		gameCamera->update(deltaTime, diffX, diffY, playing);
		//Get a local copy of the camera matrix
		cameraMatrix = gameCamera->getCameraMat();

		//tell current lighting the cameraPosition
		thisConfig.currLighting->cameraPos = gameCamera->getCameraPos();

		thisConfig.currLighting->cameraWithOutPersp = gameCamera->getCameraMatNoPersp();

		thisConfig.currLighting->persp = gameCamera->getPersp();
	}
	else
	{
		if (thisConfig.debugSettings->statePrints) printf("camera was null");
	}

}

void Game::gameObjectUpdate()
{
	std::vector<GameObject*> objects = scenes[currScene]->getObjects();

	//loop through all the objects
	for (int count = 0; count < objects.size(); count++)
	{
		objects[count]->update(deltaTime);
	}
}

void Game::updateBehaviours()
{
	std::vector<PublicBehaviour*> behs  = scenes[currScene]->getPublicBehaviours();
	for (int count = 0; count < behs.size(); count++)
	{

		behs[count]->update(deltaTime);
		
	}

	for (int count = 0; count < globalBehaviours.size(); count++)
	{
		globalBehaviours[count]->update(deltaTime);
	}
}

void Game::transparentUpdate()
{
	//And then the transparent list
	std::vector<GameObject*> objects = scenes[currScene]->getTransObjects();

	for (int count = 0; count < objects.size(); count++)
	{
		objects[count]->update(deltaTime);
	}
}

void Game::reflectiveUpdate()
{
	//And then the transparent list
	std::vector<ReflectiveObject*> objects = scenes[currScene]->getReflectiveObjects();

	for (int count = 0; count < objects.size(); count++)
	{
		objects[count]->update(deltaTime);
	}
}

void Game::lightingUpdate()
{
	//Update the lightingstate (does nothing yet)
	//thisConfig.currLighting->updateShadows(new ShadowBox());

	LightingState * currLighting = thisConfig.currLighting;

	for (int count = 0; count < currLighting->numberOfLights; count++)
	{
		if (currLighting->lights[count] != nullptr) {
			currLighting->lights[count]->update(deltaTime);
		}
	}

}

void Game::guiUpdate()
{
	//And then the transparent list
	std::vector<GameObject*> objects = scenes[currScene]->getGUIObjects();

	for (int count = 0; count < objects.size(); count++)
	{
		objects[count]->update(deltaTime);
	}
}

#pragma endregion

#pragma region Utility Calls

void Game::addPostProccessor(PostProcessingShader * x)
{
	renderEngine->addPostProcessor(x);
}

void Game::removePostProcessor(PostProcessingShader * x)
{
	renderEngine->removePostProcessor(x);
}

void Game::addGlobalBehaviour(GlobalBehaviour* x)
{
	globalBehaviours.push_back(x);
	x->setTarget(this);
	x->setup();
}

void Game::setConfigAll()
{
	std::vector<GameObject*> objects = scenes[currScene]->getObjects();

	for (int count = 0; count < objects.size(); count++)
	{
		objects[count]->setConfig(thisConfig);
	}

	renderEngine->setConfig(thisConfig);

	objects = scenes[currScene]->getTransObjects();

	for (int count = 0; count <objects.size(); count++)
	{
		objects[count]->setConfig(thisConfig);
	}

	std::vector<ReflectiveObject*> rObjects = scenes[currScene]->getReflectiveObjects();

	for (int count = 0; count < rObjects.size(); count++)
	{
		rObjects[count]->setConfig(thisConfig);
	}

    objects = scenes[currScene]->getGUIObjects();
	

	for (int count = 0; count <objects.size(); count++)
	{
		objects[count]->setConfig(thisConfig);
	}

	for (int count = 0; count < thisConfig.currLighting->numberOfLights; count++)
	{
		if (thisConfig.currLighting->lights[count] != nullptr)
		{
			thisConfig.currLighting->lights[count]->setConfig(thisConfig);
		}
	}
}

void Game::addGameObject(GameObject * newObject)
{
	scenes[currScene]->addGameObject(newObject);
}

void Game::addTransparentGameObject(GameObject * newObject)
{
	scenes[currScene]->addTransparentGameObject(newObject);
}

void Game::addGUIObject(GameObject * newObject)
{
	scenes[currScene]->addGUIObject(newObject);
}

void Game::addReflectiveGameObject(ReflectiveObject * newObject)
{
	scenes[currScene]->addReflectiveGameObject(newObject);
}

#pragma endregion

#pragma region Event Handlers

void Game::mouseDown(int buttonID, int state, int mods)
{
	int type = 0;

	if (buttonID == GLFW_MOUSE_BUTTON_LEFT)
	{
		if (state == GLFW_PRESS)
		{
			mouseDownBool = true;
			type = INPUT_LEFT_MOUSE_DOWN;

		}
		else if (state == GLFW_RELEASE)
		{
			mouseDownBool = false;

			type = INPUT_LEFT_MOUSE_UP;
		
		}
	}

	if (buttonID == GLFW_MOUSE_BUTTON_RIGHT)
	{
		if (state == GLFW_PRESS)
		{
			type = INPUT_RIGHT_MOUSE_DOWN;
		}
		else if (state == GLFW_RELEASE)
		{
			type = INPUT_RIGHT_MOUSE_UP;
		}
	}

	std::vector<InputListener*> listeners= scenes[currScene]->getListeners();

	for (int count = 0; count < listeners.size(); count++)
	{
		listeners[count]->OnClick(type,mousePrevX, mousePrevY);
	}

}

void Game::mouseMove(int x, int y)
{


}

void Game::keyEvent(int key, int scancode, int action, int mods)
{

	if (action ==GLFW_PRESS)
	{
		switch (key)
		{
		case 'w':
			keys |= Keys::Up;

			break;
		case 'W':
			keys |= Keys::Up;
			break;
		case 's':
			keys |= Keys::Down;
			break;
		case 'S':
			keys |= Keys::Down;
			break;
		case 'a':
			keys |= Keys::Left;
			break;
		case 'A':
			keys |= Keys::Left;
			break;
		case 'd':
			keys |= Keys::Right;
			break;
		case 'D':
			keys |= Keys::Right;
			break;
		case 'r':
			keys |= Keys::R;
			break;
		case 'R':
			keys |= Keys::R;
			break;
		case 'f':
			keys |= Keys::F;
			break;
		case 'F':
			keys |= Keys::F;
			break;
		case '1':
			keys |= Keys::num1;
			break;
		case '2':
			keys |= Keys::num2;
			break;
		case '3':
			keys |= Keys::num3;
			break;
		case '4':
			keys |= Keys::num4;
			break;
		case '5':
			keys |= Keys::num5;
			break;
		case 'x':
			keys |= Keys::x;
			break;
		case 'X':
			keys |= Keys::x;
			break;
		case 'q':
		case 'Q':
			keys |= Keys::q;
			break;
		case 'e':
		case 'E':
			keys |= Keys::e;
			break;
		case 'z':
		case 'Z':
			keys |= Keys::z;
			break;

		case 32:
			keys |= Keys::Space;
			break;
		case GLFW_KEY_ESCAPE:
			gameClock->end();
			this->~Game();
		case GLFW_KEY_F4:
			keys |= Keys::F4;
			break;
		case GLFW_KEY_F3:
			keys |= Keys::F3;
			break;
		case GLFW_KEY_F5:
			keys |= Keys::F5;
			break;
		default:

			//

			break;
		}
	}
	//if this is a keyUp event
	else if (action == GLFW_RELEASE)
	{
		switch (key)
		{
		case 'w':
			keys &= (~Keys::Up);
			break;
		case 's':
			keys &= (~Keys::Down);
			break;
		case 'a':
			keys &= (~Keys::Left);
			break;
		case 'd':
			keys &= (~Keys::Right);
			break;
		case 'r':
			keys &= (~Keys::R);
			break;
		case 'f':
			keys &= (~Keys::F);
			break;
		case 'W':
			keys &= (~Keys::Up);
			break;
		case 'S':
			keys &= (~Keys::Down);
			break;
		case 'A':
			keys &= (~Keys::Left);
			break;
		case 'D':
			keys &= (~Keys::Right);
			break;
		case 'R':
			keys &= (~Keys::R);
			break;
		case 'F':
			keys &= (~Keys::F);
			break;
		case '1':
			keys &= (~Keys::num1);
			break;
		case '2':
			keys &= (~Keys::num2);
			break;
		case '3':
			keys &= (~Keys::num3);
			break;
		case '4':
			keys &= (~Keys::num4);
		break;		
		case '5':
			keys &= (~Keys::num5);
			break;
		case 'x':
			keys &= (~Keys::x);
			break;
		case 'X':
			keys &= (~Keys::x);
			break;
		case 'q':
		case 'Q':
			keys &= (~Keys::q);
			break;
		case 'e':
		case 'E':
			keys &= (~Keys::e);
			break;
		case 'z':
			keys &= (~Keys::z);
			break;
		case 32:
			keys &= (~Keys::Space);
			break;
		case GLFW_KEY_F4:
			keys &= (~Keys::F4);
			break;
		case GLFW_KEY_F3:
			keys &= (~Keys::F3);
			break;
		case GLFW_KEY_F5:
			keys &= (~Keys::F5);
			break;
		default:
			//

			break;
		}
	}
}

void Game::mousePassiveMouse(double x, double y )
{
	diffX = x - mousePrevX;
	diffY = y - mousePrevY;

	mousePrevX = x;
	mousePrevY = y;

	std::vector<InputListener*> listeners = scenes[currScene]->getListeners();

	for (int count = 0; count < listeners.size(); count++)
	{
		listeners[count]->mouseMovePos(x,y);
	}
}

Keystate Game::getKeys()
{
	return keys;
}

#pragma endregion

