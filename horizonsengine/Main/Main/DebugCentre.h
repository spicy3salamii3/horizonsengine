#pragma once
#include <vector>
#include "Texture.h"
#include "ImageRect.h"
#include "Text.h"

class DebugCentre
{
public:

	static DebugCentre* singleton();
	void addDebugTexture(Texture* t);
	void removeDebugTexture(GLuint);

	void update(double);
	void draw();

	void addObject(GameObject *, std::string);
	int addWatch(int initialWatch, string name);
	int addWatch(float initialWatch, string name);
	int addWatch(glm::vec3 initialWatch, string name);


	void updateWatch(int location , float value);
	void updateWatch(int location, int value);
	void updateWatch(int location, glm::vec3 value);
private:

	void addTextObject(Text*);
	static DebugCentre* thisPointer;
	DebugCentre();
	~DebugCentre();

	std::vector<ImageRect*> debugTextures;

	std::vector<Text *> textureTitles;

	bool open = false;
	bool fpsOpen = false;
	
	int readingOut = 0;

	Text* fps = nullptr;

	//This vector is a simple reference, not an ownership
	std::vector<std::pair<GameObject *, Text * >> trackedGameObjects;
	std::vector<Text * > printOuts;


	RenderSettings * rs;
	double inputTimer = 0;

	//This vector is reference, not ownership
	std::vector<std::pair<Text*, string>> watchList;

};

