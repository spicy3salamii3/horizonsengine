#include "TerrainShaders.h"

TerrainShaders * TerrainShaders::thisPointer = nullptr;

TerrainShaders::TerrainShaders()
{

	programID = setupShaders(vertFilename, fragFilename);
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &textureCount);
	printf("%d texture units available.\n\n", textureCount);

	setup();
	setTilingFactor(140);

}


TerrainShaders::~TerrainShaders()
{
}

TerrainShaders* TerrainShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new TerrainShaders();

	}
	return thisPointer;
}

void TerrainShaders::setup()
{
	DefaultShaders::setup();

	glUseProgram(programID);

	setTextureUnits();

	locTilingFactor = glGetUniformLocation(programID, "tilingFactor");
}


void TerrainShaders::setTextureUnits()
{
	for (int count = 0; count < 13; count++)
	{
		GLuint x = glGetUniformLocation(programID, textureNames[count].c_str());
		glUniform1i(x, count);
	}
}

void TerrainShaders::setTilingFactor(int x)
{
	glUniform1i(locTilingFactor, x);
}