#include "WaterShaders.h"
#include "LightingState.h"
#include "Light.h"
#include "WaterPlane.h"

WaterShaders* WaterShaders::thisPointer = nullptr;

WaterShaders::WaterShaders()
{
	programID = setupShaders(vertexFileName, fragmentFileName);
	setup();
}


WaterShaders::~WaterShaders()
{

	glDeleteProgram(programID);

	if (thisPointer != nullptr)
	{
		delete this;
		thisPointer = nullptr;
	}
}


WaterShaders* WaterShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new WaterShaders();
	}
	return thisPointer;
}

void WaterShaders::setTextures()
{
	//Set the texture units
	glUniform1i(locRefr, 0);
	glUniform1i(locRefra, 1);
	glUniform1i(locWaterDudu, 2);
	glUniform1i(locWaterNormal, 3);
	glUniform1i(locDepth, 4);
}

void WaterShaders::setOffset(float x)
{
	glUniform1f(waterOffset, x);
}


void WaterShaders::setTiling(glm::vec2 x)
{
	glUniform2f(locTiling, x.x, x.y);
}

void WaterShaders::setLightUniforms(LightingState * currLighting)
{
	//Tell the shader how many lights there are
	glUniform1i(locLightCountW, currLighting->numberOfLights);

	//Tell the shader where the camera is
	glm::vec3 cameraPos = currLighting->cameraPos;
	glUniform3f(locCameraPosW, cameraPos.x, cameraPos.y, cameraPos.z);

	//For each light, set the relevant factors in the shader
	for (int count = 0; count < currLighting->numberOfLights; count++)
	{
		//Work out the light pos based on the camera 
		glm::vec4 lightPos = currLighting->lights[count]->position;

		glUniform4f(lightingUniforms[(count*PARAMS_PER_LIGHT)], lightPos.x, lightPos.y, lightPos.z, lightPos.w);
		glUniform3f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 1], currLighting->lights[count]->intensities.x, currLighting->lights[count]->intensities.y, currLighting->lights[count]->intensities.z);
		glUniform1f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 2], currLighting->lights[count]->attenuation);
		glUniform1f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 3], currLighting->lights[count]->ambientCoefficient);
		glUniform1f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 4], currLighting->lights[count]->coneAngle);
		glUniform3f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 5], currLighting->lights[count]->coneDirection.x, currLighting->lights[count]->coneDirection.y, currLighting->lights[count]->coneDirection.z);
		glUniformMatrix4fv(lightingUniforms[(count* PARAMS_PER_LIGHT) + 6], 1, GL_FALSE, (GLfloat*)&currLighting->lights[count]->finalMatrix);
	}
}

void WaterShaders::setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S, glm::mat4 C)
{
	setMats(T, R, S);
	setMats(C);

}

void WaterShaders::setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S)
{
	glUniformMatrix4fv(locTw, 1, GL_FALSE, glm::value_ptr(T));
	glUniformMatrix4fv(locRw, 1, GL_FALSE, (GLfloat*)& R);
	glUniformMatrix4fv(locSw, 1, GL_FALSE, (GLfloat*)& S);
}

void WaterShaders::setMats(glm::mat4 C)
{
	glUniformMatrix4fv(locCw, 1, GL_FALSE, (GLfloat*)& C);
}

void WaterShaders::prepareToRender(GameObject* go)
{
	WaterRenderingComponent* wrc = go->getComponent<WaterRenderingComponent*>();
	if (wrc == nullptr) return;

	setTiling(wrc->dimensions);

	setOffset(wrc->offset);

	setMats(go->getTransMat(), go->getRotMat(), go->getScaleMat());
}

void WaterShaders::prepareToRender(RenderSettings* rs)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	setTextures();
}

void WaterShaders::setup()
{
	locTw = glGetUniformLocation(programID, "T");
	locSw = glGetUniformLocation(programID, "S");
	locRw = glGetUniformLocation(programID, "R");
	locCw = glGetUniformLocation(programID, "camera");

	locLightCountW = glGetUniformLocation(programID, "numLights");
	locCameraPosW = glGetUniformLocation(programID, "cameraPos");

	locWaterDudu = glGetUniformLocation(programID, "dudv");
	locWaterNormal = glGetUniformLocation(programID, "normal");
	waterOffset = glGetUniformLocation(programID, "offset");
	locRefr = glGetUniformLocation(programID, "reflectionTexture");
	locRefra = glGetUniformLocation(programID, "refractionTexture");
	locDepth = glGetUniformLocation(programID, "depthMap");
	locTiling = glGetUniformLocation(programID, "tiling");

	int index = 0;

	//Work out all uniform locations for the lighting engine. This is up to 16 lights, with 7 parameters each
	for (int count = 0; count < MAX_LIGHTS*PARAMS_PER_LIGHT; count++)
	{
		//if the number of params had been reached, increment the index by 1
		if (count == PARAMS_PER_LIGHT * (index + 1))
		{
			index++;
		}

		//fill the array of lighting uniforms using the method below
		lightingUniforms[count] = getLightUniformLocationName(count - (index*PARAMS_PER_LIGHT), index);
	}
}