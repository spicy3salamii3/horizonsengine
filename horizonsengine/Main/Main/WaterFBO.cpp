#include "WaterFBO.h"
#include "ReflectiveObject.h"
#include "LightingState.h"
#include "Game.h"


WaterFBO::WaterFBO(ReflectiveObject * newHost)
{
	host = newHost; 
	
	bufferCount = 2;

	createBuffers();

	bindFrameBuffer(reflectionFBO, refWidth, refHeight);

	reflectionTexture =createTextureAttachment(refWidth, refHeight);
	reflectionDepth = createDepthBufferAttachment(refWidth, refHeight);

	unBind();

	bindFrameBuffer(refractionFBO, refraWidth, refraHeight);
	refractionTexture = createTextureAttachment(refraWidth, refraHeight);
	refractionDepth = createDepthTextureAttachment(refraWidth, refraHeight);

	unBind();

	//set the fbos values for quick retrieval
	FBOs[0] = reflectionFBO;
	FBOs[1] = refractionFBO;

	dimensions[0] = refWidth;
	dimensions[1] = refHeight;
	dimensions[2] = refraWidth;
	dimensions[3] = refraHeight;

	clipPlanes[0] = glm::vec4(0, 1, 0, -host->getPos().y);
	clipPlanes[1] = glm::vec4(0, -1, 0, host->getPos().y);


	usePlanes[0] = true;
	usePlanes[1] = true;


	//Texture t1 = Texture (reflectionTexture, glm::vec2(dimensions[0], dimensions[1]), "Water Reflection");
	//Texture t2 = Texture (refractionTexture, glm::vec2(dimensions[2], dimensions[3]), "Water Refraction");
	//Game::singleton()->debugCentre->addDebugTexture(t1);
	//Game::singleton()->debugCentre->addDebugTexture(t2);
}

void WaterFBO::update()
{
	//Work out the positions of the camera for two texture
	thisConfig = host->getConfig();

	initialWidth = thisConfig.displayDetails->width;
	initialHeight = thisConfig.displayDetails->height;

	glm::vec3 newCameraPos = Game::singleton()->getCamera()->getCameraPos();

	float cameraHeight  =newCameraPos.y - host->getPos().y;
	
	newCameraPos.y -= 2 * cameraHeight;

	cameraAngles[1] = thisConfig.currLighting->currCamera->getCameraMat();

	cameraAngles[0] = thisConfig.currLighting->currCamera->getLookAtMatrix(true, newCameraPos);

	clipPlanes[0] = glm::vec4(0, 1, 0, -host->getPos().y-heightFudge);
	clipPlanes[1] = glm::vec4(0, -1, 0, host->getPos().y+heightFudge);

}



WaterFBO::~WaterFBO()
{
	glDeleteFramebuffers(1,&reflectionFBO);
	glDeleteTextures(1,&reflectionTexture);
	glDeleteRenderbuffers(1, &reflectionDepth);

	glDeleteFramebuffers(1, &refractionFBO);
	glDeleteTextures(1, &refractionTexture);
	glDeleteRenderbuffers(1, &refractionDepth);
}


void WaterFBO::createBuffers() 
{
	glGenFramebuffers(1, &reflectionFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, reflectionFBO);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	glGenFramebuffers(1, &refractionFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, refractionFBO);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
}


int WaterFBO::createTextureAttachment(int width, int height)
{
	GLuint texture;
	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, -720.0f);
	float min;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
	float amount = std::min(4.0f, min);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

	return texture;

}

int WaterFBO::createDepthTextureAttachment(int width, int height)
{
	GLuint texture;
	glGenTextures(1, &texture);

	glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, (GLvoid*)0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, -720.0f);
	float min;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
	float amount = std::min(4.0f, min);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);

	return texture;
}

int WaterFBO::createDepthBufferAttachment(int width, int height)
{
	GLuint depthBuffer;
	glGenRenderbuffers(1, &depthBuffer);

	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
	return depthBuffer;
}