#pragma once
#include "Component.h"
class Shader;
class Material;
class Model;

class RenderConditions : public Component
{
public:
	RenderConditions(GameObject* host);

	Shader* shaderInUse = nullptr;

	int renderPriority = 0;

	bool containsTransparency = false;
	bool castShadows = true;

	Model* model = nullptr;

	Material* getMaterial(int meshIndex);

	//Once material generation is central!!
	//Material * material;

	void setMaterial(Material* mat)
	{
		matSet = true;
		material = mat;
	}

	void setMaterial(Material* mat, int meshIndex)
	{
		if (MAX_MESH_MATERIALS <= meshIndex) meshIndex = MAX_MESH_MATERIALS - 1;
		materials[meshIndex] = mat;
	}

private:
	const static int MAX_MESH_MATERIALS = 24;

	bool matSet = false;
	Material* material = nullptr;

	Material* materials[MAX_MESH_MATERIALS] = { nullptr };
};

