#include "Shader.h"
#include <ostream>
#include <string>
#include <iostream>
#include <glm-0.9.9.2/glm/glm.hpp>

using namespace std;
Shader::Shader()
{
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxTexUnits);
}


Shader::~Shader()
{
}


GLuint Shader::getProgramID() { return programID; }

GLuint Shader::getLightUniformLocationName(int count, int index)
{
	//work out the name of the property based on index and the propertyname
	std::ostringstream ss;
	ss << "allLights[" << index << "]." << propertyNames[count];
	std::string s = ss.str();
	const char * uniformName = s.c_str();

	//return the unifrom location
	return glGetUniformLocation(programID, uniformName);
}

void Shader::setClipPlane(glm::vec4 cP, bool x)
{
	glUniform1i(locCPbool, x);
	glUniform4fv(locCP, 1, (GLfloat*)&cP);
}