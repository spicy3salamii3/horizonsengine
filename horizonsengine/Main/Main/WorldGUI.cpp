#include "WorldGUI.h"
#include "DefaultShaders.h"
#include "WorldGUIShader.h"

WorldGUI::WorldGUI(Texture * t) : ImageRect(t)
{
	setup();

	transform->res = t->res;
	applySizing();

}


WorldGUI::~WorldGUI()
{
}

void WorldGUI::update(double x)
{
	Object3D::update(x);
	Object3D::lateUpdate(x);
}

void WorldGUI::draw(RenderSettings* rs)
{
	glDisable(GL_CULL_FACE);
	defaultShaders = WorldGUIShader::getInstance();

	glUseProgram(defaultShaders->getProgramID());

	defaultShaders->setClipPlane(rs->clipPlane, rs->useClipPlane);
	defaultShaders->setMats(T, R, S, rs->cameraMat);

	defaultShaders->setParentLoc(glm::vec4(0.0f));

	defaultShaders->setLighting(1, thisConfig.currLighting);

	defaultShaders->setNormalBool(false);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glBindVertexArray(VAO);

	glDrawArrays(GL_QUADS, 0,totalPoints);
	glEnable(GL_CULL_FACE);
}

void WorldGUI::applySizing()
{
	for (int i = 0; i< totalPoints / 2; i++)
	{
		points[i * 2] = points[i * 2] * (transform->res.x/unitsPerPixel);
		points[(i * 2) + 1] = points[(i * 2) + 1] * (transform->res.y /unitsPerPixel);
	}

	sortVAO();
}
