#include "Path.h"
#include "Scene.h"
#include "Helpers.h"

void Path::generateFromSceneWithKeyword(Scene* scene, std::string keyword)
{	
	std::vector<Waypoint*> waypoints = scene->getWaypoints();

	//Find all the relevant waypoints
	for (int count = 0; count < waypoints.size(); count++)
	{
		if (waypoints[count]->getName().find(keyword) != string::npos)
		{
			//Only allowed if it has a number at the end
			std::string result = returnNumber(waypoints[count]->getName());
			if( result!= "")
				points.push_back(waypoints[count]);
		}
	}

	//sort those waypoints
	std::sort(points.begin(), points.end(), compareWaypoints);
}

Waypoint* Path::getWaypoint(int index)
{
	if (index >= points.size()) return getWaypoint(index - points.size());

	else return points[index];
}


