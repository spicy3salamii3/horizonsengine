#include "Scene.h"
#include <stdio.h>
#include "Game.h"
#include "Config.h"
#include "PhysicsComponent.h"

void Scene::loadInit()
{
	currLighting = new LightingState();

	gameRefr->thisConfig.currLighting = currLighting;

	debugSettings = new DebugSettings();

	gameRefr->thisConfig.debugSettings = debugSettings;

	gameRefr->getDisplayDetails()->shadowDistance = 200;
}

void Scene::addWaypoint(Waypoint* newWp)
{
	waypointsList.push_back(newWp);
}

void Scene::addHostlessCollider(btCollisionShape * x, glm::mat4 transform)
{
	hostlessColliders.push_back(x);

	PhysicsComponent *hostless = new PhysicsComponent(x, transform);

	Game::singleton()->getPhysicsEngine()->addToSimulation(hostless);
}

void Scene::addFBOGroup(FBOGroup * x)
{
	fboGroups.push_back(x);

	Game::singleton()->getRenderEngine()->onAddFBO(x, this);
}

Waypoint* Scene::findWaypoint(std::string name)
{
	for (int count = 0; count < waypointsList.size(); count++)
	{
		if (waypointsList[count] != nullptr)
		{
			if (waypointsList[count]->getName() == name|| waypointsList[count]->getName().substr(3) == name)
			{
				return waypointsList[count];
			}
		}
	}
}

void Scene::addGameObject(GameObject * newObject)
{

	renderableObjects.push_back(newObject);
	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);

	if (newObject->getComponent<PhysicsComponent*>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent*>());
}

void Scene::addGUIObject(GameObject * newObject)
{
	guiRenderableObjects.push_back(newObject);
	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);

	if (newObject->getComponent<PhysicsComponent*>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent*>());
}

void Scene::setGameObjectPointer(int type, GameObject* go)
{
	switch (type)
	{
	case SCENE_TERRAIN_PTR:
		terrain = go;
		break;
	default:
		std::cout << "oopsie";
		break;
	}
}

void Scene::removeGameObject(GameObject * object)
{
	removeEndOfFrame.push_back(object);
	object->kill();
	printf("added a pending delete object\n");
}

void Scene::endOfFrame()
{
	int deleted = 0;
	for (int eofCount = 0; eofCount < removeEndOfFrame.size(); eofCount++)
	{
		GameObject* object = removeEndOfFrame[eofCount];
		bool found = false;
		for (int count=0; count < renderableObjects.size(); count++)
		{
			if (renderableObjects[count] == object)
			{
				if (object->getComponent<PhysicsComponent*>() != nullptr) gameRefr->getPhysicsEngine()->removeFromSimulation(object->getComponent<PhysicsComponent*>());
				delete object;
				renderableObjects.erase(renderableObjects.begin() + count);
				deleted++;
				printf("Deleted\n");
				found = true;
				break;
			}
		}

		if (found) continue;

		for (int count = 0; count < transparentRenderableObjects.size(); count++)
		{
			if (transparentRenderableObjects[count] == object)
			{
				if (object->getComponent<PhysicsComponent*>() != nullptr) gameRefr->getPhysicsEngine()->removeFromSimulation(object->getComponent<PhysicsComponent*>());
				delete object;
				transparentRenderableObjects.erase(transparentRenderableObjects.begin() + count);
				deleted++;
				printf("Deleted\n");
				found = true;
				break;
			}
		}

		if (found) continue;

		for (int count = 0; count < reflectiveRenderableObjects.size(); count++)
		{
			if (reflectiveRenderableObjects[count] == object)
			{
				if (object->getComponent<PhysicsComponent*>() != nullptr) gameRefr->getPhysicsEngine()->removeFromSimulation(object->getComponent<PhysicsComponent*>());
				delete object;
				reflectiveRenderableObjects.erase(reflectiveRenderableObjects.begin() + count);
				deleted++;
				printf("Deleted\n");
				found = true;
				break;
			}
		}
		
	}
	
	removeEndOfFrame.clear();
}

void Scene::addTransparentGameObject(GameObject * newObject)
{
	transparentRenderableObjects.push_back(newObject);


	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);

	if (newObject->getComponent<PhysicsComponent*>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent*>());
}

void Scene::addReflectiveGameObject(ReflectiveObject* newObject)
{
	reflectiveRenderableObjects.push_back(newObject);
	newObject->setConfig(gameRefr->getConfig());

	gameRefr->getRenderEngine()->onAddGameObject(newObject);


	if (newObject->getComponent<PhysicsComponent*>() != nullptr)
		gameRefr->getPhysicsEngine()->addToSimulation(newObject->getComponent<PhysicsComponent*>());

	Game::singleton()->getRenderEngine()->onAddFBO(newObject->getFBOGroup(), this);
}

void Scene::addPublicBehaviour(PublicBehaviour * newBeh)
{

	publicBehavioursList.push_back(newBeh);
	newBeh->setTarget(this);
	newBeh->setup();
}

void Scene::addInputListener(InputListener * x)
{
	inputListenersList.push_back(x);
}