#include "ShadowFBO.h"
#include "Game.h"


ShadowFBO::ShadowFBO(int width, int height): w(width), h(height)
{
	bufferCount = 1;
	setupFBO();
	unBind();
	update();

	Game::singleton()->debugCentre->addDebugTexture(new Texture(texture, glm::vec2(w, h), ""));

	FBOs[0] = frameBuffer;

	dimensions[0] = w;
	dimensions[1] = h;

	usePlanes[0] = false;
}


ShadowFBO::~ShadowFBO()
{
}

void ShadowFBO::update()
{
	initialWidth = Game::singleton()->getConfig().displayDetails->width;
	initialHeight = Game::singleton()->getConfig().displayDetails->height;
}
void ShadowFBO::bind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer);
	glViewport(0, 0, w, h);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);
}

void ShadowFBO::unBind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, initialWidth, initialHeight);
}

void ShadowFBO::setupFBO()
{
	glGenFramebuffers(1,&frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	glGenTextures(1,&texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, (void*)0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);
}
