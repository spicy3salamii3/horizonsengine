#include "InstancedShaders.h"


InstancedShaders* InstancedShaders::thisPointer = nullptr;

InstancedShaders::InstancedShaders()
{
	programID = setupShaders(vertFilename, fragFilename);
	setup();
	glUseProgram(programID);
	setTextures();
	glUseProgram(0);
}


InstancedShaders::~InstancedShaders()
{
}


InstancedShaders* InstancedShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new InstancedShaders();
	}
	return thisPointer;
}

void InstancedShaders::setup()
{
	DefaultShaders::setup();
}

void InstancedShaders::setCameraMat(glm::mat4 c)
{
	glUniformMatrix4fv(locC, 1, GL_FALSE,(GLfloat*)&c);
}