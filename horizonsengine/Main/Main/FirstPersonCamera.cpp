#include "FirstPersonCamera.h"
#include <stdio.h>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>


FirstPersonCamera::FirstPersonCamera(GameObject * newPlayer)
{
	posTrackRelRotPlayer = glm::vec3(0.0f, 0.0f, 0.0f);
	posRelPlayer = glm::vec3(0.0f, 0.0f, -30.0f);
	target = newPlayer;
	
}


FirstPersonCamera::~FirstPersonCamera()
{
}

void FirstPersonCamera::update(double deltaTimed, int mouseX, int mouseY, bool playing)
{
	calcPersp();
	float deltaTime = deltaTimed;


	cameraAngle.x += (1.0f*(mouseY)) / mouseSens;
	cameraAngle.y += (-1.0f*(glm::min(mouseX,900))) / mouseSens;

	if (cameraAngle.y > M_PI * 2) cameraAngle.y -= M_PI * 2;
	if (cameraAngle.y < -M_PI * 2) cameraAngle.y += M_PI * 2;


	if (cameraAngle.x > maxX)
	{
		cameraAngle.x = maxX - 0.001f;
	}
	else if (cameraAngle.x <minX)
	{
		cameraAngle.x = minX + 0.001f;
	}

	///<Summary>
	///1.5. because this is a first person camera, rotate the target object by the y rotation of the camera
	///</Summary>
	

	if (target != nullptr)
	{
		target->setRot(glm::vec3(0, cameraAngle.y, 0));

		glm::vec3 totalOffset = (target->getRotMat() *  positionOffset);

		cameraPos = target->getPos() + totalOffset;
	}
	else
	{
		cameraPos = glm::vec3(0, 0, 0);
	}


	///<Summary>
	///2. The second step calculates the camera target
	///</Summary>

	float xAngle = cameraAngle.x + (M_PI / 2);
	glm::vec3 lookingAt = glm::rotateX(lookingAtConst, xAngle);
	lookingAt = glm::rotateY(lookingAt, cameraAngle.y);


	cameraTarget = cameraPos - lookingAt;


	//What is the camera looking at ?


	///<Summary>
	///3. The third step calculates the camera up vector. Although 0,1,0 would work in this
	///simple case, it assures that the camera stays upright whatever it is looking at
	///</Summary>

	//Get the vector the camera is looking down
	cameraDirection = glm::normalize(cameraTarget - cameraPos);

	//work out what camera right is
	cameraRight = glm::normalize(glm::cross(up, cameraDirection));

	//work out what  camera up is
	cameraUp = glm::cross(cameraDirection, cameraRight);


	///<Summary>
	///4. Calculate the camera matrix and mulitply it by the perspective matrix
	///</Summary>
	camera =
		persp
		* glm::lookAt(cameraPos, cameraTarget, cameraUp);

	cameraNoPersp = glm::lookAt(cameraPos, cameraTarget, cameraUp);

	reportPosition();
}


glm::mat4 FirstPersonCamera::getLookAtMatrix(bool invertY, glm::vec3 pos)
{
	if (invertY)
	{
		//Get the vector the camera is looking down
		cameraDirection = glm::normalize(cameraTarget - pos);

		//work out what camera right is
		cameraRight = glm::normalize(glm::cross(up, cameraDirection));

		//work out what  camera up is
		-cameraUp = glm::cross(cameraDirection, cameraRight);


		float xAngle = (-cameraAngle.x) + (M_PI / 2);

		
		glm::vec3 lookingAt = glm::rotateX(lookingAtConst,  xAngle);

		lookingAt = glm::rotateY(lookingAt, cameraAngle.y);

		cameraTarget = pos - lookingAt;



		return persp*glm::lookAt(pos, cameraTarget, cameraUp);
	}
	else
	{
		std::cout<<("THIS DOES NOT WORK YETz\n");
	}
	
}