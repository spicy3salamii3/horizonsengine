#pragma once
#include "Animation2D.h"
#include "Object2D.h"

class GrowAnimation : public Animation2D
{
public:
	GrowAnimation(double time, float startingScale, float finalSizeIncrease, GUIObject * t, bool backwards = false)
	{
		totalTime = time;
		target = t;
		size = finalSizeIncrease;
		startScale = startingScale;
		reverse = backwards;
	}

	void stepAnimation(double d)
	{
		float scale;
		if (reverse)
		{
			scale = startScale - ((timer / totalTime) * size);
		}
		else
		{
			scale = startScale + ((timer / totalTime) * size);
		}
	
		target->setLocalScale(glm::vec3(scale));

		timer += d;
		if (timer >= totalTime)
		{
			done = true;
		}
	}
	bool isDone()
	{
		return done;
	}

	void reset()
	{
		done = false;
		timer = 0.0f;
	}

private:
	double totalTime;
	float startScale;
	double timer =0.0f;
	float size;
	bool done = false;
	GUIObject * target;

	bool reverse;

};