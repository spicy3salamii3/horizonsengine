#include "CheckPlayerHeight.h"
#include "Game.h"
#include "GameObject.h"


CheckPlayerHeight::CheckPlayerHeight(Game * r, GameObject* p) 
{
	refr = r;
	player = p;

	tex = new Text(r->getConfig().codeFontCharacters);

	refr->addGUIObject(tex);

	tex->setPos(glm::vec3(500, 0, 0));
}


CheckPlayerHeight::~CheckPlayerHeight()
{
}


void CheckPlayerHeight::update(double x)
{
	glm::vec3 pos = player->getPos();
	
	float height =0;

	height = (pos - refr->getPhysicsEngine()->castRayDown(pos).hitPos).y;
	

	tex->setText("Player height "  + to_string(height)+ "\n");
}