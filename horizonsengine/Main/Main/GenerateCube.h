#pragma once
#include "VAOData.h"
#include "PrivateBehaviour.h"
#include "InputListener.h"
#include "Scenery.h"
#include "DebugCentre.h"

class GameObject;
class Scene;

class GenerateCube : public PrivateBehaviour, public InputListener
{
public:
	GenerateCube(Model * data, GameObject * host, Scene * scene);
	~GenerateCube();

	void update(double);

	void OnClick(int key, float, float);
private:
	Model * shape;
	DebugCentre* debug;
	Custom * cube =nullptr;
	GameObject * player;
	Scene * scene;
	bool deleteTime = false;
	int noCubesWatchLoc;

	int cubeCount = 0;
};

