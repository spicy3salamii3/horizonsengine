#include "ShadowBox.h"
#include "Light.h"
#include "Camera.h"
#include "Game.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/euler_angles.hpp>


ShadowBox::ShadowBox(Camera * camera)
{
	cam = camera;
	calculateWidthsAndHeights();
	xOffset = 50;

	SHADOW_DISTANCE = Game::singleton()->getDisplayDetails()->shadowDistance;
	
}


ShadowBox::~ShadowBox()
{
}


void ShadowBox::update(Light * light) 
{
	glm::vec3 cameraPos = cam->getCameraPos();

	lightViewMatrix = light->getLightViewMatrix();

	glm::mat4 rotation  = calculateCameraRotationMatrix();

	glm::vec3 forwardVector = rotation * FORWARD;

	glm::vec3 toFar = glm::vec3(forwardVector);

	toFar*=SHADOW_DISTANCE;




	glm::vec3 toNear = glm::vec3(forwardVector);

	toNear *= Game::singleton()->getDisplayDetails()->nearPlane;
	//toNear *= 0.00001f;

	glm::vec3 centerNear = toNear + cameraPos;

	glm::vec3 centerFar = toFar + cameraPos;

	std::vector<glm::vec4> points = calculateFrustumVertices(rotation, forwardVector, centerNear,
		centerFar);

	bool first = true;
	for each(glm::vec4 point in points) {
		if (first) {
			minX = point.x;
			maxX = point.x;
			minY = point.y;
			maxY = point.y;
			minZ = point.z;
			maxZ = point.z;
			first = false;
			continue;
		}
		if (point.x > maxX) {
			maxX = point.x;
		}
		else if (point.x < minX) {
			minX = point.x;
		}
		if (point.y > maxY) {
			maxY = point.y;
		}
		else if (point.y < minY) {
			minY = point.y;
		}
		if (point.z > maxZ) {
			maxZ = point.z;
		}
		else if (point.z < minZ) {
			minZ = point.z;
		}
	}
	maxZ += OFFSET;

	//minX -= xOffset;
	//maxX += xOffset;

}

	/**
	* Calculates the center of the "view cuboid" in light space first, and then
	* converts this to world space using the inverse light's view matrix.
	*
	* @return The center of the "view cuboid" in world space.
	*/
glm::vec3 ShadowBox::getCenter() {
	float x = (minX + maxX) / 2.0f;
	float y = (minY + maxY) / 2.0f;
	float z = (minZ + maxZ) / 2.0f;

	glm::vec4 cen = glm::vec4(x, y, z, 1);
	glm::mat4 invertedLight;
	invertedLight = glm::inverse(lightViewMatrix);

	return invertedLight * cen;
}

float ShadowBox::getWidth()
{
	return maxX - minX;
}


float ShadowBox::getHeight() 
{
	return maxY - minY;
}

float ShadowBox::getLength() 
{
	return maxZ - minZ;
}
	/**
	* Calculates the position of the vertex at each corner of the view frustum
	* in light space (8 vertices in total, so this returns 8 positions).
	*
	* @param rotation
	*            - camera's rotation.
	* @param forwardVector
	*            - the direction that the camera is aiming, and thus the
	*            direction of the frustum.
	* @param centerNear
	*            - the center point of the frustum's near plane.
	* @param centerFar
	*            - the center point of the frustum's (possibly adjusted) far
	*            plane.
	* @return The positions of the vertices of the frustum in light space.
	*/
std::vector<glm::vec4> ShadowBox::calculateFrustumVertices(glm::mat4 rotation, glm::vec3 forwardVector, glm::vec3 centerNear, glm::vec3 centerFar)
{
	glm::vec3 upVector = rotation * UP;

	glm::vec3 rightVector = glm::cross(forwardVector, upVector);

	glm::vec3 downVector = glm::vec3(-upVector.x, -upVector.y, -upVector.z);

	glm::vec3 leftVector = glm::vec3(-rightVector.x, -rightVector.y, -rightVector.z);

	glm::vec3 farTop = centerFar+ glm::vec3(upVector.x * farHeight, upVector.y * farHeight, upVector.z * farHeight);

	glm::vec3 farBottom =centerFar+ glm::vec3(downVector.x * farHeight,downVector.y * farHeight, downVector.z * farHeight);

	glm::vec3 nearTop = centerNear + glm::vec3 (upVector.x * nearHeight,upVector.y * nearHeight, upVector.z * nearHeight);

	glm::vec3 nearBottom = centerNear+glm::vec3(downVector.x * nearHeight, downVector.y * nearHeight, downVector.z * nearHeight);

	std::vector<glm::vec4> points;

	points.push_back( calculateLightSpaceFrustumCorner(farTop, rightVector, farWidth));

	points.push_back(calculateLightSpaceFrustumCorner(farTop, leftVector, farWidth));

	points.push_back( calculateLightSpaceFrustumCorner(farBottom, rightVector, farWidth));

	points.push_back(calculateLightSpaceFrustumCorner(farBottom, leftVector, farWidth));

	points.push_back(calculateLightSpaceFrustumCorner(nearTop, rightVector, nearWidth));

	points.push_back(calculateLightSpaceFrustumCorner(nearTop, leftVector, nearWidth));

	points.push_back(calculateLightSpaceFrustumCorner(nearBottom, rightVector, nearWidth));

	points.push_back(calculateLightSpaceFrustumCorner(nearBottom, leftVector, nearWidth));

	return points;
}

	/**
	* Calculates one of the corner vertices of the view frustum in world space
	* and converts it to light space.
	*
	* @param startPoint
	*            - the starting center point on the view frustum.
	* @param direction
	*            - the direction of the corner from the start point.
	* @param width
	*            - the distance of the corner from the start point.
	* @return - The relevant corner vertex of the view frustum in light space.
	*/
glm::vec4 ShadowBox::calculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction,float width) 
{
	glm::vec3 point = startPoint +glm::vec3(direction.x * width, direction.y * width, direction.z * width);

	glm::vec4 point4f =glm::vec4(point, 1.0f);

	return lightViewMatrix * point4f;
}

	/**
	* @return The rotation of the camera represented as a matrix.
	*/
glm::mat4 ShadowBox::calculateCameraRotationMatrix()
{
	glm::vec2 cameraAngle = cam->getCameraAngle();
	glm::mat4 rotation = glm::eulerAngleYXZ(cameraAngle.y, cameraAngle.x, 0.0f);
	return rotation;
}

	/**
	* Calculates the width and height of the near and far planes of the
	* camera's view frustum. However, this doesn't have to use the "actual" far
	* plane of the view frustum. It can use a shortened view frustum if desired
	* by bringing the far-plane closer, which would increase shadow resolution
	* but means that distant objects wouldn't cast shadows.
	*/
void ShadowBox::calculateWidthsAndHeights() {
	SHADOW_DISTANCE = Game::singleton()->getDisplayDetails()->shadowDistance;

	farWidth = (float)(SHADOW_DISTANCE * tan(glm::radians(Game::singleton()->getDisplayDetails()->fovDegs)));


	nearWidth = (float)(Game::singleton()->getDisplayDetails()->nearPlane* tan(glm::radians(Game::singleton()->getDisplayDetails()->fovDegs)));

	farHeight = farWidth / Game::singleton()->getDisplayDetails()->getAspectRatio();

	nearHeight = nearWidth / Game::singleton()->getDisplayDetails()->getAspectRatio();
}

 glm::mat4 ShadowBox::getOrthoProjectionMatrix() {
	 glm::mat4 projectionMatrix = glm::mat4(1);
	projectionMatrix[0][0] = 2.0f / getWidth();
	projectionMatrix[1][1] = 2.0f / getHeight();
	projectionMatrix[2][2] = -2.0f / getLength();
	projectionMatrix[3][3] = 1;
	return projectionMatrix;
	
}