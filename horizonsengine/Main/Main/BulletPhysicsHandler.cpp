#include "BulletPhysicsHandler.h"
#include "Scene.h"
#include "GameObject.h"
#include "PhysicsComponent.h"

BulletPhysicsHandler* BulletPhysicsHandler::thisPointer = nullptr;

BulletPhysicsHandler::BulletPhysicsHandler()
{

}



void BulletPhysicsHandler::setup()
{
	config = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(config);
	overlappingPairCache = new btDbvtBroadphase();
	solver = new btSequentialImpulseConstraintSolver();

	dynWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver,config);


	dynWorld->setGravity(btVector3(0, -10, 0));

	std::cout << "Bullet Physics setup finished\n";
}

BulletPhysicsHandler::~BulletPhysicsHandler()
{
	//cleanup in the reverse order of creation/initialization

	///-----cleanup_start----
	//remove the rigidbodies from the dynamics world and delete them

	for (int i = dynWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject* obj = dynWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);

		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}

		dynWorld->removeCollisionObject(obj);
		delete obj;
	}

	//delete collision shapes
	for (int j = 0; j < components.size(); j++)
	{
		PhysicsComponent* shape = components[j];
		components[j] = 0;
		delete shape;
	}

	//delete dynamics world
	delete dynWorld;

	//delete solver
	delete solver;

	//delete broadphase
	delete overlappingPairCache;

	//delete dispatcher
	delete dispatcher;

	delete config;

	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	components.clear();
}


BulletPhysicsHandler * BulletPhysicsHandler::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new BulletPhysicsHandler();
		thisPointer->setup();
	}
	return thisPointer;
}

void BulletPhysicsHandler::doPhysics(double delta, Scene *)
{
	double timeStep = delta / 1000.0f;

	for each (PhysicsComponent * comp in components)
	{
		comp->btUpdate(timeStep);
	}


	//actually step the simulation
	dynWorld->stepSimulation((float)timeStep, 5);

	// update positions of all objects
	for (int j = dynWorld->getNumCollisionObjects() - 1; j >= 0; j--)
	{
		btCollisionObject* obj = dynWorld->getCollisionObjectArray()[j];
		btRigidBody* body = btRigidBody::upcast(obj);
		btTransform trans;

		if (body && body->getMotionState())
		{
			void *userPointer = body->getUserPointer();
			if (userPointer)
			{
				body->getMotionState()->getWorldTransform(trans);
				btQuaternion orientation = trans.getRotation();
				PhysicsComponent *comp = static_cast<PhysicsComponent*>(userPointer);

				comp->setHostPosition(glm::vec3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
				comp->setHostOrientation(glm::quat(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
			}

		}
		else
		{
			trans = obj->getWorldTransform();
		}

	}
}

void BulletPhysicsHandler::addToSimulation(PhysicsComponent * x)
{
	for (int count = 0; count < components.size(); count++)
	{
		//found the object already!
		if (components[count] == x) return;
	}
	components.push_back(x);

	if (!x->setup) x->setupBullet();
	
	dynWorld->addRigidBody(x->body);
	
}

void BulletPhysicsHandler::removeFromSimulation(PhysicsComponent * x)
{
	btRigidBody *body = x->body;
	for (int count = 0; count < components.size(); count++)
	{
		//found the object already!
		components.erase(components.begin() + count);
		dynWorld->removeRigidBody(body);
		delete x;
	}
}

void BulletPhysicsHandler::checkScene(Scene * scene)
{
	//find each shape with a physics component
	std::vector<GameObject*> objects = scene->getObjects();

	for (int count = 0; count < objects.size(); count++)
	{
		if (objects[count] != nullptr)
		{
			if (objects[count]->getComponent<PhysicsComponent*>() != nullptr)
			{
				addToSimulation(objects[count]->getComponent<PhysicsComponent*>());
			}
		}
	}

	//Add the transparent objects with physics components too
	objects = scene->getTransObjects();

	for (int count = 0; count <objects.size(); count++)
	{
		if (objects[count] != nullptr)
		{
			if (objects[count]->getComponent<PhysicsComponent*>() != nullptr)
			{
				addToSimulation(objects[count]->getComponent<PhysicsComponent*>());
			}
		}
	}

	std::cout << "found " <<  components.size() << " with phyics components\n";
}

RayCastResult BulletPhysicsHandler::castRayDown(glm::vec3 position)
{
	btVector3 start = convert(position);

	btVector3 towards = start - btVector3(0, 10000, 0);

	btCollisionWorld::ClosestRayResultCallback closestResults(start, towards);

	dynWorld->rayTest(start, towards, closestResults);

	RayCastResult res;

	res.hit = closestResults.hasHit();
	
	res.hitPos = convert(closestResults.m_hitPointWorld);

	res.distanceTravelled = position.y - res.hitPos.y;

	return res;
}
