#include "Light.h"
#include "Game.h"

Light::Light()
{
	setScale(glm::vec3(0.2f));
}

void Light::update(double f)
{
	Object3D::update(f);



}

glm::mat4 Light::getLightViewMatrix()
{

	return lightView;
}

void Light::updateLightViewMatrix(glm::vec3 centre)
{
	if (position.w == 0.0) 
	{
		glm::vec3 direction = glm::normalize(-position);

		glm::vec3 sBoxCentre = -centre;

		lightView = glm::mat4(1);

		float pitch = acos(glm::length(glm::vec2(direction.x, direction.z)));

		lightView = glm::rotate(lightView, pitch, glm::vec3(1, 0, 0));

		float yaw = atan(direction.x / direction.z);

		yaw = direction.z > 0 ? yaw - 180 : yaw;

		lightView = glm::rotate(lightView, yaw, glm::vec3(0, 1, 0));

		lightView = glm::translate(lightView, sBoxCentre);

	}
	
	else
		lightView =  glm::lookAt(glm::vec3(position), glm::vec3(position) - glm::normalize(coneDirection), glm::vec3(0, 1, 0));

}


void Light::setupShadows()
{
	float size = Game::singleton()->getDisplayDetails()->shadowMapSize;
	shadowFBOGroup = new ShadowFBO(size,size);
	shadowsSetup = true;
}


