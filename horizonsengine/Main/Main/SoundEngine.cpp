#include "SoundEngine.h"
#include <iostream>

SoundEngine* SoundEngine::thisPointer = nullptr;


SoundEngine::SoundEngine()
{
	std::cout << "Initializing sound engine....\n";
}

void SoundEngine::initialize()
{
	ALCdevice * device;
	ALCcontext *context;
	 //Initialization
	device = alcOpenDevice(NULL); // select the "preferred device"

	if (device) {
		context = alcCreateContext(device, NULL);
		alcMakeContextCurrent(context);
	}
}

SoundEngine::~SoundEngine()
{
}

ALuint SoundEngine::generateBuffer(std::string filename)
{
	ALuint buffer;

	alGenBuffers(1, &buffer);

	auto mySoundData = new GURiffModel(filename.c_str());

	RiffChunk formatChunk = mySoundData->riffChunkForKey(' tmf');
	RiffChunk dataChunk = mySoundData->riffChunkForKey('atad');

	WAVEFORMATEXTENSIBLE wv;
	memcpy_s(&wv, sizeof(WAVEFORMATEXTENSIBLE), formatChunk.data, formatChunk.chunkSize);

	alBufferData(
		buffer,
		AL_FORMAT_MONO16,
		(ALvoid*)dataChunk.data,
		(ALsizei)dataChunk.chunkSize,
		(ALsizei)wv.Format.nSamplesPerSec
	);
	return buffer;
}

SoundEngine* SoundEngine::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new SoundEngine();
	}
	return thisPointer;
}

int SoundEngine::addSoundFile(std::string filename)
{
	//Make the sounds

	if (actSounds < MAX_SOUNDS)
	{
		for (int count = 0; count < actSounds; count++)
		{
			if (sounds[count] == -1)
			{
				sounds[count] = generateBuffer(filename);
				actSounds++;
				return count;
			}

		}
	}
	return -1;
}

void SoundEngine::loopSound(int,int )
{

}

void SoundEngine::stopSound(int, int)
{

}

void SoundEngine::playSound(int, int,glm::vec3 pos)
{

}
