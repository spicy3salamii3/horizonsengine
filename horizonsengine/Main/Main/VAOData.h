#pragma once
#include "GameObject.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include <stdio.h>
#include <vector>
#include "tiny_obj_loader.h"
#include <wincodec.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <CoreStructures\CoreStructures.h>
#include <FreeImage\FreeImagePlus.h>
#include "texture_loader.h"
#include <string>
#include "Volume.h"

class MeshData
{
public:

	MeshData(GLuint VAO, GLuint newTexture, GLuint newSpecTexture, GLuint newNormalMap, float *, float *, float*, float *, float *,GLuint * indices, int vertsCount, int indiciesCount);
	MeshData(GLuint VAO , int newIndiciesCount, int vertsCount = 0)
	{
		if (vertsCount == 0)
			elementsCount = newIndiciesCount;
		else
			elementsCount = vertsCount;

		defaultVAO = VAO;
		uniqueVerticiesCount = newIndiciesCount;
	}
	MeshData(GLuint VAO, float*, float*, float*, float*, float*, GLuint* indices, int vertsCount, int newIndiciesCount);

	~MeshData() 
	{
		delete[] points;
		delete[] normals;
		delete[] tangents;
		delete[] bitangents;
		delete[] textureCoords;
	}
	
	int getElementsCount() { return elementsCount; }
	int getTrianglesCount() { return elementsCount / 3; }

	GLuint getVAO() { return defaultVAO; }

	void setVAO(GLuint x) { defaultVAO = x; }

	GLuint getTexture() { return texture; }
	GLuint getSpecTexture() { return specTexture; }
	GLuint getNormalMap() { return normalMap; }
	
	float * getPoints() { return points; }
	float * getNormals() { return normals; }
	float * getTangents() { return tangents; }
	float * getBitangents() { return bitangents; }
	float * getTexts() { return textureCoords; }
	GLuint * getIndices() { return indices; }

	glm::vec3 getVertexByIndex(int i);
	void setExtents(Extents x) { e = x; }
	Extents getExtents() { return e; }

private:
	int elementsCount;
	int uniqueVerticiesCount;
	float * points;
	float * normals;
	float * tangents;
	float * bitangents;
	float * textureCoords;
	GLuint * indices;

	Extents e;
	GLuint defaultVAO;

	GLuint texture =0;
	GLuint specTexture=0;
	GLuint normalMap=0;

};




class Model
{
public:
	//New style constructor, assuming multiple meshdatas are going to be used using the new callback methods. 
	Model() {}
	~Model() {}

	void addMesh(MeshData* newMd) { meshes[totalMeshes] = newMd; totalMeshes++; }


	//Fetch the induvidual  VAO by index
	GLuint getVAOByIndex(int i) { return meshes[i]->getVAO(); }

	//fetch the induvidual verts count
	int getElementsCountByIndex(int i) { return meshes[i]->getElementsCount(); }

	GLuint getTextureByIndex(int i) { return meshes[i]->getTexture(); }
	GLuint getSpecMapByIndex(int i) { return meshes[i]->getSpecTexture(); }
	GLuint getNormalMapByIndex(int i) { return meshes[i]->getNormalMap(); }

	MeshData* getMeshByIndex(int i) { return meshes[i]; }

	int getMeshCount() { return totalMeshes; }

	Extents getExtents(int i) { return meshes[0]->getExtents(); }

protected:

	//The VAO to store things in
	GLuint shapeVAO = 0;

	int totalVerts = 0;

	int totalMeshes = 0;

	const static int MAX_MESHES = 1024;
	MeshData* meshes[MAX_MESHES] = { nullptr };
};




class InstancedVAOData : public Model
{
public:
	InstancedVAOData() {}
	~InstancedVAOData() {}
	int getInstances() { return instances; }
	void setInstances(int x) { instances = x; }
protected:
	int instances = 0;
};