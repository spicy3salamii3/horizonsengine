#include <glm-0.9.9.2/glm/glm.hpp>
#include "Helpers.h"
#include "Bounds.h"
#include "GameObject.h"

glm::vec2 makeVec3xzVec2(glm::vec3 x)
{
	returny.x = x.x;
	returny.y = x.z;
	return returny;
}

glm::vec2 makeVec3xyVec2(glm::vec3 x)
{
	returny.x = x.x;
	returny.y = x.y;
	return returny;
}

float heightAboveTriangleGivenXZ(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) {
	float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
	float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
	float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
	float l3 = 1.0f - l1 - l2;
	return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}

float heightAboveTriangleGivenXZ(Triangle tri, glm::vec2 pos) {
	float det = (tri.p2.z - tri.p3.z) * (tri.p1.x - tri.p3.x) + (tri.p3.x - tri.p2.x) * (tri.p1.z - tri.p3.z);
	float l1 = ((tri.p2.z - tri.p3.z) * (pos.x - tri.p3.x) + (tri.p3.x - tri.p2.x) * (pos.y - tri.p3.z)) / det;
	float l2 = ((tri.p3.z - tri.p1.z) * (pos.x - tri.p3.x) + (tri.p1.x - tri.p3.x) * (pos.y - tri.p3.z)) / det;
	float l3 = 1.0f - l1 - l2;
	return l1 * tri.p1.y + l2 * tri.p2.y + l3 * tri.p3.y;
}

glm::vec3 getAverageOf3PointsXZ(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	return (a + b + c) / 3.0f;
}

bool isWithinBounds(glm::vec3 p, Bounds2DXZ b)
{
	if (p.x > b.topRight.x || p.x < b.bottomLeft.x) return false;
	if (p.z > b.topRight.y || p.z < b.bottomLeft.y) return false;

	return true;
}
bool isWithinBounds(glm::vec3 p, Bounds2DXY b)
{
	if (p.x > b.topRight.x || p.x < b.bottomLeft.x) return false;
	if (p.y > b.topRight.y || p.y < b.bottomLeft.y) return false;

	return true;
}
bool isWithinBounds(glm::vec2 p, Bounds2DXZ b)
{
	if (p.x > b.topRight.x || p.x < b.bottomLeft.x) return false;
	if (p.y > b.topRight.y || p.y < b.bottomLeft.y) return false;

	return true;
}
bool isWithinBounds(glm::vec2 p, Bounds2DXY b)
{
	if (p.x > b.topRight.x || p.x < b.bottomLeft.x) return false;
	if (p.y > b.topRight.y || p.y < b.bottomLeft.y) return false;

	return true;
}

bool triExceedsBounds(Triangle t, Bounds2DXZ b)
{
	bool a = isWithinBounds(t.p1, b);
	bool d = isWithinBounds(t.p2, b);
	bool c = isWithinBounds(t.p3, b);

	if (a && d && c)
	{
		return false;
	}

	return true;
}

bool extentsExceedsBounds(Extents e, Bounds2DXZ b)
{
	if  (isWithinBounds(e.minExtent, b) &&
		isWithinBounds(e.maxExtent, b))
		return false;

	else return true;
}


glm::vec3 makeVec3Vec4IgnoreW(glm::vec4 x)
{
	returny3.x = x.x;
	returny3.y = x.y;
	returny3.z = x.z;
	return returny3;
}

bool isPointInTriangle(glm::vec2 pt, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3)
{
	float d1, d2, d3;
	bool has_neg, has_pos;

	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	return !(has_neg && has_pos);
}

bool isPointInTriangle(glm::vec2 pt, Triangle tri)
{
	float d1, d2, d3;
	bool has_neg, has_pos;

	v1.x = tri.p1.x;
	v1.y = tri.p1.z;
	v2.x = tri.p2.x;
	v2.y = tri.p2.z;
	v3.x = tri.p3.x;
	v3.y = tri.p3.z;

	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	return !(has_neg && has_pos);
}


float sign(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

void gameobjectLookAt(GameObject * host, GameObject * target, float maxTurn)
{
	glm::vec3 diff = target->getPos() - host->getPos();

	float targetRot = atan(diff.x / diff.z);

	float currRot = host->getRot().y;

	host->setRot(glm::vec3(0, -targetRot, 0));
}

btVector3 convert(glm::vec3 x)
{
	return btVector3(x.x, x.y, x.z);
}

glm::vec3 convert(btVector3 x)
{
	return glm::vec3(x.x(), x.y(), x.z());
}

bool compareWaypoints(Waypoint* i1, Waypoint* i2)
{
	std::string name1 = i1->getName();
	std::string name2 = i2->getName();

	int firstNumber = std::stoi(returnNumber(name1).c_str());
	int secondNumber = std::stoi(returnNumber(name2).c_str());

	return (firstNumber < secondNumber);
}

std::string returnNumber(std::string x)
{
	char b;
	int indexOfFirstNumber = x.length();
	for (int count = 0; count < x.length(); count++)
	{
		b = x[count];
		if (isdigit(b))
		{
			//it is a number
			indexOfFirstNumber = count;
			break;
		}
	}
	return x.substr(indexOfFirstNumber, x.length());
}