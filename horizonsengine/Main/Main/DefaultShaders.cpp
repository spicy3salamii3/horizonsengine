#include "DefaultShaders.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "LightingState.h"
#include "Light.h"
#include "Game.h"
#include "RenderSettings.h"

DefaultShaders* DefaultShaders::thisPointer = nullptr;

DefaultShaders::DefaultShaders(int )
{
	programID = setupShaders(vertexFileName, fragmentFileName);
	setup();
	glUseProgram(programID);
	setTextures();
	glUseProgram(0);


}

DefaultShaders::DefaultShaders()
{

}

DefaultShaders::~DefaultShaders()
{
}

DefaultShaders* DefaultShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new DefaultShaders(0);

	}
	return thisPointer;
}

void DefaultShaders::setNormalBool(int x)
{

	glUniform1i(locUseNormalMap, x);
}

void DefaultShaders::setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S, glm::mat4 C)
{
	setMats(T, R, S);
	setMats(C);

}
void DefaultShaders::setMats(glm::mat4 C)
{
	glUniformMatrix4fv(locC, 1, GL_FALSE, (GLfloat*)& C);

}

void DefaultShaders::setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S)
{
	glUniformMatrix4fv(locT, 1, GL_FALSE, glm::value_ptr(T));
	glUniformMatrix4fv(locR, 1, GL_FALSE, (GLfloat*)& R);
	glUniformMatrix4fv(locS, 1, GL_FALSE, (GLfloat*)& S);
}

void DefaultShaders::setInvertY(bool x)
{
	if (x)
	{
		invertY = -1;
	}
	else
	{
		invertY = 1;
	}

}

void DefaultShaders::prepareToRender(GameObject* go)
{
	setMats(go->getTransMat(), go->getRotMat(), go->getScaleMat());

	setNormalBool(true);

	setInvertY(false);

	if(go->getParent()!= nullptr)
		setParentLoc(glm::vec4(go->getParent()->getPos(),0));
	else
	{
		setParentLoc(glm::vec4(0));
	}
}

void DefaultShaders::prepareToRender(RenderSettings* rs)
{
	glDisable(GL_BLEND);
	//Set the camera angle
	setMats(rs->cameraMat);

	setClipPlane(rs->clipPlane, rs->useClipPlane);
}

void DefaultShaders::setParentLoc(glm::vec4 pos)
{
	glUniform4f(locPosRelP, pos.x, pos.y, pos.z, 0.0f);
}

void DefaultShaders::setLighting(LightingState* currLighting)
{

	DisplayDetails* dd = Game::singleton()->getDisplayDetails();
	int lightsWithShadowsCount = 0;

	glUniform3fv(locCameraPos, 1, (GLfloat*)& currLighting->cameraPos);

	glUniform1i(locInvertY, invertY);



	//Tell the shader how many lights there are
	glUniform1i(locLightCount, currLighting->numberOfLights);

	//Tell the shader the shadow map details
	glUniform1f(locShadowDistance, dd->shadowDistance);
	glUniform1f(locShadowMapSize, dd->shadowMapSize);

	if (!newFrameBool) return;

	//For each light, set the relevant factors in the shader
	for (int count = 0; count < currLighting->numberOfLights; count++)
	{
		//Work out the light pos based on the camera 
		Light* light = currLighting->lights[count];

		glm::vec4 lightPos = light->position;

		glUniform4fv(lightingUniforms[(count * PARAMS_PER_LIGHT)], 1, glm::value_ptr(lightPos));
		glUniform3fv(lightingUniforms[(count * PARAMS_PER_LIGHT) + 1], 1, glm::value_ptr(light->intensities));
		glUniform1f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 2], light->attenuation);
		glUniform1f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 3], light->ambientCoefficient);
		glUniform1f(lightingUniforms[(count * PARAMS_PER_LIGHT) + 4], light->coneAngle);
		glUniform3fv(lightingUniforms[(count * PARAMS_PER_LIGHT) + 5], 1, glm::value_ptr(light->coneDirection));

		glUniformMatrix4fv(lightingUniforms[(count * PARAMS_PER_LIGHT) + 6], 1, GL_FALSE, (GLfloat*)& currLighting->lights[count]->finalMatrix);

		if (currLighting->lights[count]->castShadows)
		{
			glUniform1i(lightingUniforms[(count * PARAMS_PER_LIGHT) + 7], maxTexUnits - lightsWithShadowsCount);
			glActiveTexture(GL_TEXTURE0 + (maxTexUnits - lightsWithShadowsCount));
			glBindTexture(GL_TEXTURE_2D, light->shadowMap);
			lightsWithShadowsCount++;

			glUniform1i(lightingUniforms[(count * PARAMS_PER_LIGHT) + 8], 1);

			glUniformMatrix4fv(lightingUniforms[(count * PARAMS_PER_LIGHT) + 9], 1, GL_FALSE, glm::value_ptr(offsetMatrix * light->projMatrix * light->getLightViewMatrix()));

		}
		else
		{
			glUniform1i(lightingUniforms[(count * PARAMS_PER_LIGHT) + 8], 0);
		}
	}
	glActiveTexture(GL_TEXTURE0);

	newFrameBool = false;
}

void DefaultShaders::setLighting(float shinyness, LightingState * currLighting)
{
	//Tell the shader how shiny this object is
	//(DEPRECATED)
	glUniform1f(locShiny, shinyness);

	setLighting(currLighting);

}

void DefaultShaders::setTextures()
{
	//Set the texture units
	glUniform1i(locTex0, 0);
	glUniform1i(locTex1, 1);
	glUniform1i(locTex2, 2);
}

void DefaultShaders::setup()
{
	locT = glGetUniformLocation(programID, "T");
	locS = glGetUniformLocation(programID, "S");
	locR = glGetUniformLocation(programID, "R");
	locC = glGetUniformLocation(programID, "camera");

	locTex0 = glGetUniformLocation(programID, "textureImage");
	locTex1 = glGetUniformLocation(programID, "specularImage");
	locTex2 = glGetUniformLocation(programID, "normalMap");

	locPosRelP = glGetUniformLocation(programID, "posRelParent");

	locShiny = glGetUniformLocation(programID, "materialShininess");
	locLightCount = glGetUniformLocation(programID, "numLights");
	
	locInvertY = glGetUniformLocation(programID, "invertY");

	locCameraPos = glGetUniformLocation(programID, "cameraPos_World");

	locCP = glGetUniformLocation(programID, "clipPlane");
	locCPbool = glGetUniformLocation(programID, "usePlane");

	locUseNormalMap = glGetUniformLocation(programID, "useNormalMap");

	locShadowMapSize = glGetUniformLocation(programID, "mapSize");
	locShadowDistance = glGetUniformLocation(programID, "shadowDistance");

	offsetMatrix = glm::mat4(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);

	int index = 0;

	//Work out all uniform locations for the lighting engine. This is up to 20 lights, with 10 parameters each
	for (int count = 0; count < MAX_LIGHTS*PARAMS_PER_LIGHT; count++)
	{
		//if the number of params had been reached, increment the index by 1
		if (count == PARAMS_PER_LIGHT*(index + 1))
		{
			index++;
		}

		//fill the array of lighting uniforms using the method below
		lightingUniforms[count] = getLightUniformLocationName(count - (index*PARAMS_PER_LIGHT), index);
	}

	
}
