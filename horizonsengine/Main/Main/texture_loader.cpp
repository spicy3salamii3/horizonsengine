
#include "texture_loader.h"
#include "..\\..\\Common\\Libs\\FreeImage\\FreeImagePlus.h"
#include <iostream>
#include <math.h>
#include <algorithm>
#include <glad/glad.h>
#include <glfw/glfw3.h>
using namespace std;

unsigned int h1 = 0;
unsigned int w1 = 0;

bool fiOkay = false;


#pragma region FreeImagePlus texture loader
int getH() { return h1; }
int getW() { return w1; }

bool getOkay() { return fiOkay; }

GLuint fiLoadTexture(const char *filename) {

	fiOkay = false;
	GLuint				newTexture = 0;
	fipImage			I;

	fiOkay = I.load(filename);

	if (!fiOkay) {

		cout << "FreeImagePlus: Cannot open  " << filename << "\n";
		return 0;
	}

	fiOkay = I.convertTo32Bits();

	if (!fiOkay) {

		cout << "FreeImagePlus: Conversion to 24 bits unsuccessful.\n";
		return 0;
	}

	w1 = I.getWidth();
	h1 = I.getHeight();

	BYTE *buffer = I.accessPixels();

	if (!buffer) {

		cout << "FreeImagePlus: Cannot access bitmap data.\n";
		return 0;
	}


	glGenTextures(1, &newTexture);
	glBindTexture(GL_TEXTURE_2D, newTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB_ALPHA, w1, h1, 0, GL_BGRA, GL_UNSIGNED_BYTE, buffer);

	// Setup default texture properties
	if (newTexture) {

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.5f);
		float min;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
		float amount =std::min(16.0f,min);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
	}

	return newTexture;
}

void fiLoadTextureSkybox(const char *filename, int type)
{
	fiOkay = FALSE;
	GLuint				newTexture = 0;
	fipImage			I;

	fiOkay = I.load(filename);

	if (!fiOkay) {

		cout << "FreeImagePlus: Cannot open image file.\n";
		return;
	}

	fiOkay = I.flipVertical();
	fiOkay = I.convertTo24Bits();

	if (!fiOkay) {

		cout << "FreeImagePlus: Conversion to 24 bits successful.\n";
		return;
	}

	w1 = I.getWidth();
	h1 = I.getHeight();

	BYTE *buffer = I.accessPixels();


	if (!buffer) {

		cout << "FreeImagePlus: Cannot access bitmap data.\n";
		return;
	}


	glTexImage2D(type, 0, GL_RGBA, w1, h1, 0, GL_BGR, GL_UNSIGNED_BYTE, buffer);

	// Setup default texture properties
	if (newTexture) {

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		float min;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
		float amount = std::min(16.0f, min);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
	}

}

GLuint fiLoadTextureNormal(const char *filename) {

	fiOkay = FALSE;
	GLuint				newTexture = 0;
	fipImage			I;

	fiOkay = I.load(filename);

	if (!fiOkay) {

		cout << "FreeImagePlus: Cannot open image file.\n";
		return 0;
	}

	fiOkay = I.convertTo24Bits();

	if (!fiOkay) {

		cout << "FreeImagePlus: Conversion to 24 bits unsuccessful.\n";
		return 0;
	}

	w1 = I.getWidth();
	h1 = I.getHeight();

	BYTE *buffer = I.accessPixels();

	if (!buffer) {

		cout << "FreeImagePlus: Cannot access bitmap data.\n";
		return 0;
	}


	glGenTextures(1, &newTexture);
	glBindTexture(GL_TEXTURE_2D, newTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w1, h1, 0, GL_BGR, GL_UNSIGNED_BYTE, buffer);

	// Setup default texture properties
	if (newTexture) {

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -1.3f);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, -720.0f);
		float min;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &min);
		float amount = std::min(4.0f, min);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, amount);
	}

	return newTexture;
}

#pragma endregion

