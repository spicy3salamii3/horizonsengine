#include "PlayerController.h"
#include "keyState.h"
#include "Game.h"
#include "GameObject.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>

PlayerController::PlayerController(Game * newRefr)
{
	refr = newRefr;
}


PlayerController::~PlayerController()
{
}

void PlayerController::update(double deltaTime)
{
	Keystate currKeys = refr->getKeys();

	glm::vec3 currPos = target->getPos();


	glm::vec3 additive = glm::vec3(0,0,0);

	if (currKeys & Keys::R)
	{
		additive += glm::vec3(0, deltaTime*speed, 0);
	}
	if (currKeys & Keys::F)
	{
		additive += glm::vec3(0, -deltaTime*speed, 0);
	}	
	if (currKeys & Keys::Left)
	{
		additive += glm::vec3( deltaTime*speed,0, 0);
	}
	if (currKeys & Keys::Right)
	{
		additive += glm::vec3(-deltaTime * speed, 0, 0);
	}
	if (currKeys & Keys::Up)
	{
		additive += glm::vec3(0, 0, deltaTime * speed);
	}
	if (currKeys & Keys::Down)
	{
		additive+= glm::vec3(0, 0, -deltaTime * speed);
	}

	additive = target->getRotMat() * glm::vec4( additive,0);

	target->setPos(currPos + additive );
}

void PlayerController::draw(RenderSettings* rs)
{
}