#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <vector>

/*
This class very closely based off the "Shadow Box" code by Thin Matrix.
I followed his tutorials on shadows, and this code is translated from Java and refactored slightly.
It's purpose is to create an orthographic matrix around the main camera in which to render a shadow map

https://www.youtube.com/user/ThinMatrix
*/

class Camera;
class Light;

class ShadowBox
{
public:
	ShadowBox(Camera * camera);

	~ShadowBox();

	void setCamera(Camera*x) { cam = x; }

	void update(Light * );

	glm::mat4 getOrthoProjectionMatrix();

	glm::vec3 getCenter();



private:
	float SHADOW_DISTANCE = 200;
	float OFFSET = 60;

	float xOffset = 15;
	glm::vec4 UP = glm::vec4(0, 1, 0, 0);
	glm::vec4 FORWARD = glm::vec4 (0, 0, 1, 1);


	float minX, maxX;
	float minY, maxY;
	float minZ, maxZ;
	glm::mat4 lightViewMatrix;
	Camera* cam;

	float farHeight, farWidth, nearHeight, nearWidth;

	float getWidth();

	float getHeight();
	
	float getLength();

	std::vector<glm::vec4> calculateFrustumVertices(glm::mat4 rotation, glm::vec3 forwardVector, glm::vec3 centerNear, glm::vec3 centerFar);

	glm::mat4 calculateCameraRotationMatrix();

	glm::vec4 calculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction, float width);

	void calculateWidthsAndHeights();

};

