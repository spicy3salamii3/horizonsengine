#pragma once
#include "Object3D.h"
#include "ImageRect.h"
#include "ShaderManager.h"
#include "WorldGUIShader.h"

class WorldGUI :
	public Object3D, public ImageRect
{
public:
	WorldGUI(Texture* texture);
	~WorldGUI();
	void update(double);
	void draw(RenderSettings*);

	static const int unitsPerPixel = 10;

	void setup()
	{
		Object3D::setup();
		ImageRect::setup();

		getComponent<RenderConditions*>()->shaderInUse = ShaderManager::singleton()->getShader<WorldGUIShader*>();
	}
protected:
	void applySizing();

	
};

