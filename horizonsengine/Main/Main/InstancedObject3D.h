#pragma once
#include "Object3D.h"
#include "VAOData.h"
#include "InstancedShaders.h"
class InstancedObject3D :
	public Object3D
{
public:


	InstancedObject3D(InstancedVAOData*);
	~InstancedObject3D();

	void update(double);
	void draw(RenderSettings*);

	void setup();
protected:

	InstancedVAOData* data=nullptr;

	int indicesLength;

	InstancedShaders * defaultShaders= nullptr;
};

