#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
#include "Object3D.h"
#include "RenderSettings.h"
#include "ShadowFBO.h"

class Light : public Object3D {
public:
	Light();

	int id = 0;

	void update(double);

	void draw(RenderSettings *rs) { if (thisConfig.debugSettings->lightSphere) { Object3D::draw(rs); } }

	void setRot(glm::vec3 newRot)
	{
		GameObject::setRot(newRot);
		coneDirection = glm::normalize(GameObject::getRotMat() * glm::vec4(coneDirection,0));
	}
	void updateLightViewMatrix(glm::vec3);
	glm::mat4 getLightViewMatrix();
	glm::mat4 projMatrix;
	glm::mat4 lastUsedSboxMatrix;

	glm::mat4 lightView = glm::mat4(1);

	bool shadowsSetup = false;

	void setupShadows();
	glm::vec4 position;
	glm::vec3 intensities;
	float attenuation  = 0.1f;
	float ambientCoefficient = 0.0f;
	float coneAngle  =0.15f; 
	glm::vec3 coneDirection = glm::vec3(0,1,0);

	Volume * getVolume() { return nullptr; }

	bool castShadows = false;

	GLuint shadowMap = 0;

	glm::mat4 finalMatrix = glm::mat4(1);

	ShadowFBO * shadowFBOGroup;

};