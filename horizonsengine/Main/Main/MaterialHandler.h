#pragma once
#include "Material.h"
#include <vector>


class MaterialHandler
{
public:
	static MaterialHandler* singleton();

	Material* makeMaterial(Texture* diffuse, Texture* specular, Texture* normal);

	SingleMaterial* makeSingleMaterial(Texture* t);

	//Look for all the textures needed for a skybox ina specific directory
	SkyboxMaterial* makeSkyboxMaterial(std::string directory);

private:
	std::vector<Material*> materials;
	std::vector<SingleMaterial*> singleMaterials;

	static MaterialHandler* thisPointer;

	MaterialHandler() {}
};

