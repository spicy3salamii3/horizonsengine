#include "Object3D.h"
#include "Game.h"
#include "texture_loader.h"
#include "tiny_obj_loader.h"
#include <FreeImage\FreeImagePlus.h>
#include <iostream>
#include <stdio.h>
#include "VAOData.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "Light.h"
#include "DefaultShaders.h"

///<Summary>
///An Object3D is defined as an object rendered in 3d space, even if itself is only 2 dimensional
///every object3d must have an update call that calls Object3D::update and Object3D::lateupdate
///</Summary>

Object3D::Object3D()
{
	setup();
}

void Object3D::update(double newDeltaTime)
{
	if (this == nullptr)return;

	if (renderConditions == nullptr)
	{
		renderConditions = getComponent<RenderConditions*>();
	}

	//set deltaTime
	deltaTime = newDeltaTime;
}



void Object3D::lateUpdate(double deltaTime)
{
	if (this == nullptr)return;

	updateBehaviours(deltaTime);
	

	GameObject::update(deltaTime);
}

void Object3D::draw(RenderSettings*rs)
{
	//The following pipeline is only used in the older LogicalRenderer pipeline. This will be depreceated soon
	if (dead) {
		std::cout << "Tried to render a dead object\n"; return;
	}

	if (defaultShaders == nullptr) defaultShaders = thisConfig.mainShaders;

	glUseProgram(defaultShaders->getProgramID());

	defaultShaders->setMats(T, R, S, rs->cameraMat);
	defaultShaders->setClipPlane(rs->clipPlane, rs->useClipPlane);

	//Parent Relative Transform stuff
	if (parent == nullptr) { defaultShaders->setParentLoc(glm::vec4(0.0f)); }
	else { defaultShaders->setParentLoc(glm::vec4(0, 0, 0, 0.0f)); }

	//Set all of the lighting uniforms
	defaultShaders->setLighting(shinyness, thisConfig.currLighting);
	
	//for each mesh in object
	for (int count = 0; count < renderConditions->model->getMeshCount(); count++)
	{	
		//Tell the shader if a normal map is being used or not
		defaultShaders->setNormalBool((renderConditions->model->getNormalMapByIndex(count) != 0));

		//Bind the model's texture to the object we're drawing
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, renderConditions->model->getTextureByIndex(count));

		//and the specular map
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, renderConditions->model->getSpecMapByIndex(count));


		//If the object uses surface normals....
		if (renderConditions->model->getNormalMapByIndex(count) != 0)
		{
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, renderConditions->model->getNormalMapByIndex(count));
		}

		GLuint VAO = renderConditions->model->getVAOByIndex(count);
		//bind the VAO
		glBindVertexArray(VAO);

		int indicesLength = renderConditions->model->getElementsCountByIndex(count);

		// Draw the triangles !
		glDrawElements(GL_TRIANGLES, indicesLength, GL_UNSIGNED_INT, (void*)0);

	}

	if (rs->renderTangents || thisConfig.debugSettings->drawTangents)
	{
		drawTangents(rs->cameraMat);
	}

	if (rs->renderNormals || thisConfig.debugSettings->drawNormals)
	{
		drawNormals(rs->cameraMat);
	}
}

void Object3D::drawNormals(glm::mat4 camera)
{
	glUseProgram(thisConfig.normalShaders->getProgramID());

	thisConfig.normalShaders->setProj(thisConfig.currLighting->persp);

	thisConfig.normalShaders->setMats(T,R,S,thisConfig.currLighting->cameraWithOutPersp);

	for (int count = 0; count < renderConditions->model->getMeshCount(); count++)
	{
		glBindVertexArray(renderConditions->model->getVAOByIndex(count));
		glDrawArrays(GL_TRIANGLES, 0, renderConditions->model->getElementsCountByIndex(count));
	}
}

void Object3D::drawTangents(glm::mat4 camera)
{
	glUseProgram(thisConfig.tanShaders->getProgramID());

	thisConfig.tanShaders->setProj(thisConfig.currLighting->persp);

	thisConfig.tanShaders->setMats(T, R, S, thisConfig.currLighting->cameraWithOutPersp);

	for (int count = 0; count < renderConditions->model->getMeshCount(); count++)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, renderConditions->model->getNormalMapByIndex(count));

		glBindVertexArray(renderConditions->model->getVAOByIndex(count));
		glDrawArrays(GL_TRIANGLES, 0, renderConditions->model->getElementsCountByIndex(count));
	}

}


