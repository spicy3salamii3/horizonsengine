#include "Object2D.h"
#include "Game.h"
#include "Helpers.h"



GUITransform::GUITransform(GUIObject * h) : Component(h)
{
	guiObject = h; 
}

glm::mat4 GUITransform::getProj()
{
	DisplayDetails * dd = Game::singleton()->getDisplayDetails();
	return glm::ortho(-dd->width / 2.0f, dd->width / 2.0f, -dd->height / 2.0f, dd->height / 2.0f, 100.0f, -100.0f);
}


glm::mat4 GUITransform::getScaleMat()
{
	return glm::scale(glm::mat4(1), glm::vec3(getRelativeScale(), 1.0f));
}

glm::mat4 GUITransform::getPosMat()
{
	return glm::translate(glm::mat4(1), getPos());
}

glm::vec2 GUITransform::getScreenRes()
{
	return res * scale;
}

glm::vec2 GUITransform::getScale()
{
	if (guiObject->getParent() == nullptr)
		return scale;
	else
	{
		return scale * guiObject->getParent()->transform->getScale();
	}
}

glm::vec2 GUITransform::getRelativeScale()
{
	if (guiObject->getParent() == nullptr)
		return relativeScale;
	else
	{
		return relativeScale * guiObject->getParent()->transform->getRelativeScale();
	}
}

glm::vec3 GUITransform::getPos()
{
	float width = 0.0f;
	float height = 0.0f;
	glm::vec3 parentPos = glm::vec3(0);

	//GUI transform ensures that a BL anchored button at (0,0) will have it's bottom left corner 
	//touching the bottom left corner of the screen or it's parent
	if (guiObject->getParent() == nullptr)
	{
		DisplayDetails* dd = Game::singleton()->getDisplayDetails();
		width = dd->width;
		height = dd->height;
	}
	else
	{
		glm::vec2 p = guiObject->getParent()->transform->getScreenRes();
		width = p.x;
		height = p.y;
		parentPos = guiObject->getParent()->transform->getPos();
	}

	switch (anchorType)
	{
	case GUI_TRANSFORM_ANCHOR_BL:
		return parentPos + glm::vec3(pos.x, pos.y, pos.z) - glm::vec3(width / 2.0f - (res.x* getScale().x), (height / 2.0f - (res.y* getScale().y)), 0.0f);
		break;
	case GUI_TRANSFORM_ANCHOR_BR:
		return  parentPos + glm::vec3(-pos.x, pos.y, pos.z) - glm::vec3(-(width / 2.0f - (res.x* getScale().x)), (height / 2.0f - (res.y* getScale().y)), 0.0f);
		break;
	case GUI_TRANSFORM_ANCHOR_TL:
		return  parentPos + glm::vec3(pos.x, -pos.y, pos.z) - glm::vec3(width / 2.0f - (res.x* getScale().x), -(height / 2.0f - (res.y* getScale().y)), 0.0f);
		break;
	case GUI_TRANSFORM_ANCHOR_TR:
		return  parentPos + glm::vec3(-pos.x, -pos.y, pos.z) + glm::vec3(width / 2.0f - (res.x* getScale().x), (height / 2.0f - (res.y* getScale().y)), 0.0f);
		break;

	case GUI_TRANSFORM_ANCHOR_CEN:
		return   parentPos + pos;
		break;
	}
}


Bounds2DXY GUITransform::getScreenBounds()
{
	glm::vec2 bl;
	glm::vec2 tr;
	
	glm::vec3 pos = getPos();
	
	bl.x = pos.x - (res.x *scale.x);
	tr.x = pos.x + (res.x *scale.x);

	bl.y = pos.y - (res.y *scale.y);
	tr.y = pos.y + (res.y *scale.y);

	return Bounds2DXY(bl, tr);
}

bool GUITransform::isMouseOver(float x, float y)
{
	DisplayDetails * dd = Game::singleton()->getDisplayDetails();
	float screenX = x - (dd->width/2.0f);
	float screenY = -y + (dd->height/2.0f);

	return isWithinBounds(glm::vec2(screenX, screenY), getScreenBounds());
}