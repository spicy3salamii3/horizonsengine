#pragma once
#include "Waypoint.h"
#include <vector>
class Scene;

class Path
{
public:
	Path() {}
	~Path() {}
	void addPoint(Waypoint*x)
	{
		points.push_back(x);
	}
	std::vector<Waypoint*> getPoints()
	{
		return points;
	}

	void generateFromSceneWithKeyword(Scene* scene, std::string keyword);

	Waypoint* getWaypoint(int index);
private:
	std::vector<Waypoint*> points;
};