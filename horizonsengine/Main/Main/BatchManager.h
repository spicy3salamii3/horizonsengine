#pragma once
#include "Batch.h"
#include "RenderSettings.h"
#include "Scene.h"
#include "Config.h"
#include "Object3D.h"

class BatchManager
{
public:
	BatchManager(Config g);
	~BatchManager();

	void drawBatches(RenderSettings *);

private:
	const static int MAX_BATCHES = 16;
	Batch * allBatches[MAX_BATCHES] = { nullptr };

	void drawBatch(RenderSettings*);
	void makeNewBatchSet(Scene *);

	bool tryMeshFitInBatch(Batch*, MeshData *, glm::mat4);

	const static int MAX_GAMEOBJECTS = 2048;
	GameObject * notBatched[MAX_GAMEOBJECTS] = { nullptr };
	GameObject * notBatchable[MAX_GAMEOBJECTS] = { nullptr };

	MeshData *  meshesToBatch[MAX_GAMEOBJECTS * 10] = { nullptr };

	//The max size in bytes (4mb)
	int maxSize = 4000000;

	Config thisConfig;
};

