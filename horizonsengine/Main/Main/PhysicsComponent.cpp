#include "PhysicsComponent.h"
#include "GameObject.h"
#include "Volume.h"
#include "Helpers.h"
#include <bullet/btBulletCollisionCommon.h>
#include <bullet/btBulletDynamicsCommon.h>

PhysicsComponent::PhysicsComponent(GameObject * h, float m) :Component(h)
{
	mass = m;
}

PhysicsComponent::PhysicsComponent(btCollisionShape * vol, glm::mat4 position) : Component(nullptr)
{
	noHostComp = true;
	bulletHostless = vol;
	hostlessTransform = position;
}

PhysicsComponent::~PhysicsComponent()
{

}

void PhysicsComponent::setMass(float m)
{
	mass = m;
}

void PhysicsComponent::applyForce(glm::vec3 f)
{
	body->activate(true);
	body->applyForce(convert(f), convert(glm::vec3(0)));
}

void PhysicsComponent::applyForce(Force f)
{
	body->activate(true);
	body->applyForce(convert(f.magnitude), convert(f.posRelCG));
}

void PhysicsComponent::setHostPosition(glm::vec3 p)
{
	if (host == nullptr)return;
	host->setPos(p);
}

void PhysicsComponent::setHostOrientation(glm::quat q)
{
	if (host == nullptr)return;
	host->setRot(q);
}

void PhysicsComponent::applyAcceleration(glm::vec3 a)
{
	glm::vec3 force = a * mass;
	applyForce(force);
}

void PhysicsComponent::applyImpulse(glm::vec3 impulse)
{
	body->activate(true);
	body->applyCentralImpulse(convert(impulse));
}

btCollisionShape * PhysicsComponent::getBtShape()
{
	if (host != nullptr)
	{
		return host->getBtVolume();
	}
	else if (noHostComp)
	{
		return bulletHostless;
	}
	else
	{
		std::cout << "physics component doesn't have host, but isn't hostless\n";
		return nullptr;
	}
}

void PhysicsComponent::setupBullet()
{
	bool isDynamic = (mass != 0.f);

	btCollisionShape * shape = getBtShape();

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
	{
		getBtShape()->calculateLocalInertia(mass, localInertia);
	}
	transform.setIdentity();

	if (host != nullptr) transform.setFromOpenGLMatrix((const btScalar*)glm::value_ptr(host->getTransMat()));
	else  transform.setFromOpenGLMatrix((const btScalar*)glm::value_ptr(hostlessTransform));

	
	motionState = new btDefaultMotionState(transform);

	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, getBtShape(), localInertia);

	body = new btRigidBody(rbInfo);

	body->setUserPointer((void *)this);

	isBullet = true;


	setup = true;
}

void PhysicsComponent::btUpdate(double timeStep)
{
	if (host == nullptr)return;

	if (bulletIndependant)
	{
		btTransform trans = body->getWorldTransform();

		glm::vec3 rot = host->getRot();

		trans.setRotation(btQuaternion(rot.y, rot.x, rot.z));

		body->setWorldTransform(trans);

		//Calulate an appropriate force to apply to the box in order for it to be moved correctly
		btVector3 newPos = convert(host->getPos());

		btVector3 oldPos = body->getWorldTransform().getOrigin();

		btVector3 dist = newPos - oldPos;

		btVector3 currVel = body->getLinearVelocity();

		btVector3 acc = 2 * ((newPos - oldPos - (currVel * timeStep)) / (pow(timeStep, 2)));

		body->applyCentralForce(mass * acc / glm::max(50.0f, responsiveness));

		//Anti-Gravity
		body->applyCentralForce(btVector3(0,mass * 10.0f,0));
	}

	//btTransform trans;
	//trans.setFromOpenGLMatrix(glm::value_ptr(host->getTransMat() * host->getRotMat()));

	//motionState->setWorldTransform(trans);
	//body->setWorldTransform(trans);

	body->activate(true);
}

void PhysicsComponent::btLateUpdate(double timeStep)
{
	if (host == nullptr)return;


}