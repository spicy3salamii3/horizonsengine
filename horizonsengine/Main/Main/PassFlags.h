#pragma once
#include <cstdint>

typedef  uint32_t PassFlags;

enum PassFlagTypes
{
	AllObjects = 0b1,
	OpaqueObjects = 0b01,
	Transparent = 0b001,
	Reflective = 0b0001,
	GUI1 = 0b00001,
	GUI2 = 0b000001
};
