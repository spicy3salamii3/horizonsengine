#pragma once
#include "Scene.h"
#include "Config.h"
#include "Camera.h"
#include <glm-0.9.9.2\glm\glm.hpp>

class PostProcessingShader;
///<Summary>
///A scene renderer can take in a scene and render it to the screen using a variety of shaders,
///techniques and optomizations
///</Summary>

class SceneRenderer
{
public:
	SceneRenderer();
	virtual ~SceneRenderer();

	virtual void setScene(Scene * newScene)
	{
		currScene = newScene;
	}

	virtual void addPartialScene(Scene* scene) {}

	virtual void render()=0;
	virtual void update(double)=0;

	void setConfig(Config c) { thisConfig = c; }

	void setCamera(Camera * c) { gameCamera = c; }
	virtual void initialize()= 0;

	virtual void addPostProcessor(PostProcessingShader *) = 0;
	virtual void removePostProcessor(PostProcessingShader *) = 0;

	virtual void onAddGameObject(GameObject* go) {}
	virtual void onAddFBO(FBOGroup* fbo, Scene * ) {}

protected:
	Scene * currScene;
	Config thisConfig;

	glm::mat4 cameraMatrix = glm::mat4(1);

	double deltaTime;

	Camera * gameCamera = nullptr;
};

