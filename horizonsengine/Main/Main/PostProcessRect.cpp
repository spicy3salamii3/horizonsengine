#include "PostProcessRect.h"



PostProcessRect::PostProcessRect(GLuint x)
{
	texture = x;

	sortVAO();
}


PostProcessRect::~PostProcessRect()
{
}

void PostProcessRect::draw(RenderSettings*)
{
	//glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glUseProgram(postProcessingShader->getProgramID());

	glBindVertexArray(VAO);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glDrawArrays(GL_QUADS, 0, 4);

	//glEnable(GL_DEPTH_TEST); 
	glEnable(GL_CULL_FACE);
}