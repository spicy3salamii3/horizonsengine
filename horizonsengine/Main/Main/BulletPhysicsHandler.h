#pragma once
#include "PhysicsHandler.h"
#include <bullet/btBulletCollisionCommon.h>
#include <bullet/btBulletDynamicsCommon.h>

class BulletPhysicsHandler :
	public PhysicsHandler
{
public:
	static BulletPhysicsHandler* getInstance();

	void doPhysics(double, Scene*);

	void checkScene(Scene*);

	RayCastResult castRayDown(glm::vec3 position);

	bool useDefaultColliderTypes() { return false; }

	void addToSimulation(PhysicsComponent*);

	void removeFromSimulation(PhysicsComponent *);

private:
	void setup();
	static BulletPhysicsHandler * thisPointer;
	BulletPhysicsHandler();
	~BulletPhysicsHandler();

	std::vector<PhysicsComponent*> components;

	btDiscreteDynamicsWorld * dynWorld;
	btDefaultCollisionConfiguration * config;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver * solver;
	btDbvtBroadphase * overlappingPairCache;
};

