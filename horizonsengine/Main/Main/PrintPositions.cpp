#include "PrintPositions.h"
#include "Game.h"

PrintPositions::PrintPositions(Game * x) : refr(x)
{

}
PrintPositions::~PrintPositions()
{

}

void PrintPositions::addObject(GameObject * g, std::string c)
{
	if (readingOut < MAX_TRACKED_OBJECTS)
	{
		//Generate Name, x, y and z readouts at the relavant places on screen
		Text * name = new Text(Game::singleton()->getConfig().codeFontCharacters);
		name->setConfig(refr->getConfig());
		name->setText(c);
		name->setScale(glm::vec3(0.2f));
		name->setPos(glm::vec3(140 + (readingOut * xoffset), 20, 0));
		refr->addGUIObject(name);

		Text * x = new Text(Game::singleton()->getConfig().codeFontCharacters);
		x->setConfig(refr->getConfig());
		x->setPos(glm::vec3(140 + (readingOut * xoffset), 45, 0));
		refr->addGUIObject(x);
		x->setScale(glm::vec3(0.2f));

		Text * y = new Text(Game::singleton()->getConfig().codeFontCharacters);
		y->setConfig(refr->getConfig());
		y->setPos(glm::vec3(140 + (readingOut * xoffset), 70, 0));
		refr->addGUIObject(y);
		y->setScale(glm::vec3(0.2f));

		Text * z = new Text(Game::singleton()->getConfig().codeFontCharacters);
		z->setConfig(refr->getConfig());
		z->setPos(glm::vec3(140 + (readingOut * xoffset), 95, 0));
		refr->addGUIObject(z);
		z->setScale(glm::vec3(0.2f));


		readOuts[readingOut * 4] = name;
		readOuts[(readingOut * 4) + 1] = x;
		readOuts[(readingOut * 4) + 2] = y;
		readOuts[(readingOut * 4) + 3] = z;
		tracked[readingOut] = g;

		readingOut++;
	}
}


void PrintPositions::update(double)
{
	for (int count = 0; count < readingOut; count++)
	{
		readOuts[(count * 4) + 1]->setText("X: " + to_string(tracked[count]->getPos().x));
		readOuts[(count * 4) + 2]->setText("Y: " + to_string(tracked[count]->getPos().y));
		readOuts[(count * 4) + 3]->setText("Z: " + to_string(tracked[count]->getPos().z));
	}
}