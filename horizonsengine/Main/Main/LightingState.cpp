#include "LightingState.h"
#include "Light.h"
#include "ObjExaminer.h"
#include "VAOData.h"


LightingState::LightingState()
{
	ObjExaminer * examiner = ObjExaminer::getInstance();

	examiner->setDirectoryObject("Objects\\");

	examiner->setFile("LightSphere.obj");

	sphereData = new Model();

	examiner->indexShapes(sphereData);


}
void LightingState::addLight(Light* newLight) 
{
	for (int count = 0; count < MAX_LIGHTS; count++)
	{
		if (lights[count] == nullptr)
		{
			lights[count] = newLight;
			newLight->addComponent(new RenderConditions(newLight));
			lights[count]->getComponent<RenderConditions*>()->model = sphereData;
			numberOfLights++;
			return;
		}
	}
}


