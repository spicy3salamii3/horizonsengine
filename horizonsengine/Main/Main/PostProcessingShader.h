#pragma once
#include "Shader.h"
#include <string>

class PostProcessingShader : public Shader
{
public:

	static PostProcessingShader* getInstance();

	virtual void update() {}

	void destroy() { delete this; }


protected:
	PostProcessingShader();
	PostProcessingShader(int x);
	~PostProcessingShader();

	void setup();

	static PostProcessingShader * thisPointer;

	std::string vertexFilename = "Shaders/PP/vertex_pp.shader";
	std::string fragFilename = "Shaders/PP/fragment_pp.shader";
};

