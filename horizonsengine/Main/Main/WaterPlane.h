#pragma once
#include "Object3D.h"
#include "ReflectiveObject.h"
#include "WaterFBO.h"
#include "RenderSettings.h"



class WaterRenderingComponent : public Component
{
public: 
	WaterRenderingComponent(GameObject* host) : Component(host) {}

	~WaterRenderingComponent() {}

	float offset = 0.0f;
	glm::vec2 dimensions = glm::vec2(0);
};

class WaterPlane : public ReflectiveObject
{
public:
	WaterPlane(glm::vec2 dimensions);
	~WaterPlane();

	void update(double f);
	void draw(RenderSettings*);
	void sortVAO();
	void applyScaling();


	void setDudv(GLuint x) { dudv = x; }
	void setNormal(GLuint x) { normal= x; }

	void setup();

private:
	WaterRenderingComponent* wrc = nullptr;

	float offset = 0;
	float waterSpeed = 0.00005f;

	float vertices[18] = {
		// positions          
		-1.0f, 0.0f, -1.0f,
		-1.0f, 0.0f,  1.0f,
		1.0f, 0.0f, -1.0f,
		1.0f, 0.0f, -1.0f,
		-1.0f, 0.0f,  1.0f,
		1.0f, 0.0f,  1.0f
	};

	GLuint dudv;
	GLuint normal;
	GLuint ref = 0;
	GLuint refra = 0;
	GLuint depthMap = 0;

	GLuint thisVAO = 0;

	GLuint vertsVBO = 0;
	float scale;
	
	WaterFBO * waterFBO = nullptr;

	void intializeFBOGroup();

	glm::vec2 dimensions;
};

