#include "TangentDrawingShaders.h"


TangentDrawingShaders* TangentDrawingShaders::thisPointer = nullptr;

TangentDrawingShaders::TangentDrawingShaders()
{
	programID = setupShadersGeometry(vertFilename, geometryFilename, fragFilename);
	setup();
}


TangentDrawingShaders::~TangentDrawingShaders()
{
}

TangentDrawingShaders* TangentDrawingShaders::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new TangentDrawingShaders();
	}
	return thisPointer;
}

void TangentDrawingShaders::setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S, glm::mat4 C)
{
	glUniformMatrix4fv(locTt, 1, GL_FALSE, glm::value_ptr(T));
	glUniformMatrix4fv(locRt, 1, GL_FALSE, (GLfloat*)&R);
	glUniformMatrix4fv(locSt, 1, GL_FALSE, (GLfloat*)&S);
	glUniformMatrix4fv(locCt, 1, GL_FALSE, (GLfloat*)&C);

}

void TangentDrawingShaders::setup()
{
	locTt = glGetUniformLocation(programID, "T");
	locSt = glGetUniformLocation(programID, "S");
	locRt = glGetUniformLocation(programID, "R");
	locCt = glGetUniformLocation(programID, "camera");

	tLocProj = glGetUniformLocation(programID, "projection");
}


void TangentDrawingShaders::setProj(glm::mat4 p)
{
	glUniformMatrix4fv(tLocProj, 1, GL_FALSE, (GLfloat*)&p);
}