#pragma once
#include "Camera.h"
class ThirdPersonCamera :
	public Camera
{
public:
	ThirdPersonCamera();
	~ThirdPersonCamera();
	void update(double deltaTimed, int, int, bool playing);
	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);

protected:
	float maxX = 2.2f;

	float minX = 1.6f;

	float mouseSens = 140;

	float radius = 6.2f;

	float maxDistPerSec = 10.0f;


	float minHeightAtPoint = 0.0f;
};

