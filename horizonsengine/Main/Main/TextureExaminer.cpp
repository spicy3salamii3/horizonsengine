#include "TextureExaminer.h"
#include "texture_loader.h"
#include <iostream>
#include "Texture.h"


TextureExaminer* TextureExaminer::thisPointer = nullptr;

TextureExaminer::TextureExaminer()
{
	printf("Initalizing texture examiner\n\n");
}


TextureExaminer::~TextureExaminer()
{
}

TextureExaminer * TextureExaminer::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new TextureExaminer();
	}
	return thisPointer;
}

Texture* TextureExaminer::getTexture(string newFileName)
{
	if (newFileName == "")
	{
		return new Texture();
	}
	string f = removeLongFileName(newFileName);

	for (int count = 0; count < texturesDone; count++)
	{
		if (f == textures[count]->filename)
		{
			//Found same texture!
			//std::cout << "Reusing texture " << f << " found at " << newFileName << "\n";

			return textures[count];
		}
	}


	//When splitting into multi thread, gen texture here then pass the 
	//GLuint ID to the import texture thread, where the actual pixels can be added

	readInNewTexture(texturesDone, f);

	if (getOkay())
	{

		texturesDone++;

		return textures[texturesDone - 1];
	}
	else
	{
		std::cout << "Tried to add new texture " << f << " but couldn't find it\n";

		return textures[texturesDone ];
	}
}

Texture* TextureExaminer::getNormalTexture(string newFileName)
{
	if (newFileName == "")
	{
		return new Texture();
	}
	string f = removeLongFileName(newFileName);

	for (int count = 0; count < texturesDone; count++)
	{
		if (f == textures[count]->filename)
		{
			//Found same texture!
			return textures[count];
		}
	}


	//When splitting into multi thread, gen texture here then pass the 
	//GLuint ID to the import texture thread, where the actual pixels can be added

	readInNewNormalTexture(texturesDone, f);

	texturesDone++;

	return textures[texturesDone - 1];
}

void TextureExaminer::readInNewTexture(int index, string filename)
{
	const char * f = filename.c_str();

	GLuint i = fiLoadTexture(f);

	float w = 0.0f;
	float h = 0.0f;

	if (!getOkay())
	{
	}
	else
	{
		std::cout << "Found id " << i << "\n";
		w = getW();
		h = getH();
	}

	textures[index] = new Texture(i, glm::vec2(w, h), filename);


}

void TextureExaminer::readInNewNormalTexture(int index, string filename)
{
	const char * f = filename.c_str();


	GLuint i = fiLoadTextureNormal(f);

	textures[index] = new Texture(i, glm::vec2(getW(), getH()), filename);

}


string TextureExaminer::removeLongFileName(string filePath)
{
	int index = filePath.find("Textures");
	if (index < 0)
	{
		index = filePath.find("textures");
		if (index<0) return directory + filePath;
	}
	string f  = filePath.substr(index, filePath.length());

	return  directory + f;
}